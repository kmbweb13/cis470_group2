/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.bootstrap;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.group2.wsc.catalog.converters.Utility;
import com.group2.wsc.catalog.domain.Category;
import com.group2.wsc.catalog.domain.Product;
import com.group2.wsc.catalog.domain.ProductStyle;
import com.group2.wsc.catalog.domain.Role;
import com.group2.wsc.catalog.repositories.CategoryRepository;
import com.group2.wsc.catalog.repositories.ProductRepository;
import com.group2.wsc.catalog.repositories.ProductStyleRepository;
import com.group2.wsc.catalog.services.CategoryService;
import com.group2.wsc.catalog.services.ProductStyleService;
import com.group2.wsc.catalog.services.RoleService;

import lombok.Data;

/**
 * This Class is only initialized and executed during local development with application mock enabled. The purpose is to load data into the local H2 in memory database. In prod situations the external
 * DB will contain all data. This class will not be used in production.
 * 
 * Due to this is only used for local development - Unit testing will not be included for this file.
 */

@Data
@Component
public class SpringJpaBootstrap implements ApplicationListener<ContextRefreshedEvent> {

	/** The category repository. */
	@Autowired
	private CategoryRepository categoryRepository;

	/** The category service. */
	@Autowired
	private CategoryService categoryService;

	/** The product style repository. */
	@Autowired
	private ProductStyleRepository productStyleRepository;

	/** The product style service. */
	@Autowired
	private ProductStyleService productStyleService;

	/** The product repository. */
	@Autowired
	private ProductRepository productRepository;

	/** The role service. */
	@Autowired
	private RoleService roleService;

	/** The user utility. */
	@Autowired
	private UserUtility userUtility;

	/** The apperal size list. */
	private final List<String> APPERAL_SIZE_LIST = Arrays.asList("X-Small", "Small", "Medium", "Large", "X-Large",
			"XX-Large");

	/** The apperal color list. */
	private final List<String> APPERAL_COLOR_LIST = Arrays.asList("Red", "Blue", "Green", "White", "Black", "Orange",
			"Yellow");

	/** The engrable size list. */
	private final List<String> ENGRABLE_SIZE_LIST = Arrays.asList("Small", "Meduim", "Large");

	/** The log. */
	private Logger log = Logger.getLogger(SpringJpaBootstrap.class);

	/** The use mock. */
	@Value("${application.enable.mock}")
	private boolean useMock;

	/*
	 * (non-Javadoc)
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
	 */
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (useMock) {
			loadCategories();
			loadProductStyles();
			loadProducts();
			loadRoles();
			loadUsers();
		}
	}

	/**
	 * Load categories.
	 */
	private void loadCategories() {
		createCategory("apparel", "Apparel");
		createCategory("plaque", "Plaque");
		createCategory("trophy", "Trophy");
		createCategory("mug", "Mug");
	}

	/**
	 * Creates the category.
	 *
	 * @param code the code
	 * @param label the label
	 */
	private void createCategory(String code, String label) {
		Category category = new Category();
		category.setCatagoryCode(code);
		category.setLabel(label);
		categoryRepository.save(category);
	}

	/**
	 * Load product styles.
	 */
	private void loadProductStyles() {
		@SuppressWarnings("unchecked")
		List<Category> categories = (List<Category>) categoryService.listAll();
		Category category = categories.get(0);
		createProductStyle("tshirt", "T-Shirt", true, false, category);
		createProductStyle("buttonShirt", "Button Down Shirt", true, false, category);
		createProductStyle("hoodie", "Hoodie", true, false, category);
		createProductStyle("polo", "Polo Shirt", true, false, category);
		category = categories.get(1);
		createProductStyle("smallPlague", "Small Plaque", false, true, category);
		category = categories.get(2);
		createProductStyle("smallTrophy", "Small Trophy", false, true, category);
		category = categories.get(3);
		createProductStyle("ceramicMug", "Ceramic Mug", true, false, category);
		createProductStyle("travelMug", "Tall Travel Mug", true, true, category);
	}

	/**
	 * Creates the product style.
	 *
	 * @param code the code
	 * @param label the label
	 * @param print the print
	 * @param engrave the engrave
	 * @param category the category
	 */
	private void createProductStyle(String code, String label, boolean print, boolean engrave, Category category) {
		ProductStyle style = new ProductStyle();
		style.setCode(code);
		style.setLabel(label);
		style.setCategory(category);
		style.setPrint(print);
		style.setEngrave(engrave);
		productStyleRepository.save(style);
	}

	/**
	 * Load products.
	 */
	private void loadProducts() {

		createProduct(68551, APPERAL_SIZE_LIST, APPERAL_COLOR_LIST,
				"http://garffshirts.com/images/products/detail/5170_ASH_5170_Hanes_Comfortblend_Tee_Shirts_5_2_oz_50_preshrunk_cotton_50_polyester_t_shirts_aaron_richman.jpg",
				"Cotton T-Shirt", "19.95", 0);
		createProduct(63851, APPERAL_SIZE_LIST, APPERAL_COLOR_LIST, "https://assets.academy.com/mgen/95/10787095.jpg",
				"Short sleve cotton button down shirt", "23.95", 1);
		createProduct(846852, APPERAL_SIZE_LIST, APPERAL_COLOR_LIST,
				"https://www.customink.com/mms/images/catalog/styles/193900/catalog_detail_image_large.jpg",
				"Cotton Hoodie", "24.95", 2);
		createProduct(78946, APPERAL_SIZE_LIST, APPERAL_COLOR_LIST,
				"http://promo.vistaprint.com/render/undecorated/product/PVAG-273306Q654Q2/RS-8Q417Q8Q6396/jpeg?compression=95&width=700",
				"Cotton Polo Shirt", "22.95", 3);
		createProduct(6845341, ENGRABLE_SIZE_LIST,
				"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrxg9bBqkUu4QZNZFuv32BQVMkUAIVWAs4rAnLENSh7F83Bhgt",
				"Small Plaque with gold plate", "9.95", 4);
		createProduct(6845341, ENGRABLE_SIZE_LIST,
				"https://img1.cgtrader.com/items/597379/a98db7ce27/large/golden-trophy-3d-model-max-obj-3ds.jpg",
				"Small Trophy with gold plate", "13.95", 5);
		createProduct(6845341, ENGRABLE_SIZE_LIST,
				"https://images-na.ssl-images-amazon.com/images/I/41pqpiVAG8L._SL500_AC_SS350_.jpg",
				"White Ceramic mug", "9.95", 6);
		createProduct(93934953, ENGRABLE_SIZE_LIST,
				"https://photos3.walmart.com/prism/themes/wmfullphoto-1.themepack/custom_14oz_travel.mug/_hd_product_01.jpg",
				"Travel Mug With Handle", "19.95", 7);

	}

	/**
	 * Creates the product.
	 *
	 * @param mediaCatalogNumber the media catalog number
	 * @param sizes the sizes
	 * @param imageUrl the image url
	 * @param description the description
	 * @param price the price
	 * @param index the index
	 */
	private void createProduct(int mediaCatalogNumber, List<String> sizes, String imageUrl, String description,
			String price, int index) {
		createProduct(mediaCatalogNumber, sizes, null, imageUrl, description, price, index);
	}

	/**
	 * Creates the product.
	 *
	 * @param mediaCatalogNumber the media catalog number
	 * @param sizes the sizes
	 * @param colors the colors
	 * @param imageUrl the image url
	 * @param description the description
	 * @param price the price
	 * @param index the index
	 */
	private void createProduct(int mediaCatalogNumber, List<String> sizes, List<String> colors, String imageUrl,
			String description, String price, int index) {
		Product product = new Product();
		@SuppressWarnings("unchecked")
		ProductStyle style = ((List<ProductStyle>) productStyleService.listAll()).get(index);
		product.setProductStyle(style);
		product.setSizes(sizes);
		product.setColors(colors);
		product.setImageUrl(imageUrl);
		product.setMediaCatalogNumber(new Integer(mediaCatalogNumber).toString());
		product.setDescription(description);
		product.setPrice(Utility.createCurrency(price));
		style.addProduct(product);
		productRepository.save(product);
		productStyleService.saveOrUpdate(style);

		log.info("Saved " + style.getLabel() + " - id: " + product.getId());
	}

	/**
	 * Load users.
	 */
	private void loadUsers() {

		userUtility.generateUser("admin", "admin", "ADMIN", true, true);
		userUtility.generateUser("manager", "manager", "MANAGER", true, true, false);
		userUtility.generateUser("employee", "employee", "EMPLOYEE", true, false);
		for (int i = 0; i < 10; i++) {
			userUtility.generateUser("customer" + i, "customer" + i, "CUSTOMER", false, true, i % 3 == 0);
		}
		for (int i = 0; i < 4; i++) {
			userUtility.generateUser("guest" + i, "guest" + i, "GUEST", false, false);
		}
	}

	/**
	 * Load roles.
	 */
	private void loadRoles() {
		createRole("GUEST", 1);
		createRole("CUSTOMER", 2);
		createRole("EMPLOYEE", 3);
		createRole("MANAGER", 4);
		createRole("ADMIN", 10);
	}

	/**
	 * Creates the role.
	 *
	 * @param roleTxt the role txt
	 * @param prioritylevel the prioritylevel
	 */
	private void createRole(String roleTxt, int prioritylevel) {
		Role role = new Role();
		role.setRole(roleTxt);
		role.setPrioritylevel(prioritylevel);
		roleService.saveOrUpdate(role);
		log.info("Saved role " + role.getRole());
	}
}
