/*
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.bootstrap;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import com.group2.wsc.catalog.converters.Utility;
import com.group2.wsc.catalog.domain.Address;
import com.group2.wsc.catalog.domain.CustomerDetail;
import com.group2.wsc.catalog.domain.EmployeeDetail;
import com.group2.wsc.catalog.domain.OrderDetail;
import com.group2.wsc.catalog.domain.Payment;
import com.group2.wsc.catalog.domain.PhoneNumber;
import com.group2.wsc.catalog.domain.Product;
import com.group2.wsc.catalog.domain.ProductOrderDetail;
import com.group2.wsc.catalog.domain.Role;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.services.AddressService;
import com.group2.wsc.catalog.services.CustomerDetailService;
import com.group2.wsc.catalog.services.EmployeeDetailService;
import com.group2.wsc.catalog.services.OrderDetailService;
import com.group2.wsc.catalog.services.PhoneNumberService;
import com.group2.wsc.catalog.services.ProductService;
import com.group2.wsc.catalog.services.RoleService;
import com.group2.wsc.catalog.services.UserInformationService;
import com.group2.wsc.catalog.services.UserService;

/**
 * This Class is only initialized and executed during local development with application mock enabled. The purpose is to load data into the local H2 in memory database. In prod situations the external
 * DB will contain all data. This class will not be used in production.
 * 
 * Due to this is only used for local development - Unit testing will not be included for this file.
 */

@Component
public class UserUtility {

	/** The user information service. */
	@Autowired
	private UserInformationService userInformationService;

	/** The address service. */
	@Autowired
	private AddressService addressService;

	/** The phone number service. */
	@Autowired
	private PhoneNumberService phoneNumberService;

	/** The employee detail service. */
	@Autowired
	private EmployeeDetailService employeeDetailService;

	/** The customer detail service. */
	@Autowired
	private CustomerDetailService customerDetailService;

	/** The order detail service. */
	@Autowired
	private OrderDetailService orderDetailService;

	/** The product service. */
	@Autowired
	private ProductService productService;

	/** The user service. */
	@Autowired
	private UserService userService;

	/** The role service. */
	@Autowired
	private RoleService roleService;

	/** The utility. */
	@Autowired
	private Utility utility;

	/**
	 * Generate user.
	 *
	 * @param userName the user name
	 * @param pass the pass
	 * @param mainRole the main role
	 * @param generateEmployee the generate employee
	 * @param generateCustomer the generate customer
	 */
	public void generateUser(String userName, String pass, String mainRole, boolean generateEmployee,
			boolean generateCustomer) {
		generateUser(userName, pass, mainRole, generateEmployee, generateCustomer, true);
	}

	/**
	 * Generate user.
	 *
	 * @param userName the user name
	 * @param pass the pass
	 * @param mainRole the main role
	 * @param generateEmployee the generate employee
	 * @param generateCustomer the generate customer
	 * @param sameAddress the same address
	 */
	public void generateUser(String userName, String pass, String mainRole, boolean generateEmployee,
			boolean generateCustomer, boolean sameAddress) {
		User user = new User();
		user.setUsername(userName);
		user.setPassword(pass);
		user.setMainRoleName(mainRole);
		Role role = roleService.getByRoleName(mainRole);
		user.setMainRole(role);
		user.setRoles(roleService.getAdditionalRoles(role));
		userService.saveOrUpdate(user);
		generateUserInformation(user, generateEmployee, generateCustomer, sameAddress);
		userService.saveOrUpdate(user);
	}

	/**
	 * Generate user information.
	 *
	 * @param user the user
	 * @param generateEmployee the generate employee
	 * @param generateCustomer the generate customer
	 * @param sameAddress the same address
	 */
	private void generateUserInformation(User user, boolean generateEmployee, boolean generateCustomer,
			boolean sameAddress) {
		UserInformation userInformation = new UserInformation();
		Faker faker = new Faker();
		com.github.javafaker.Address address = faker.address();
		userInformation.setFirstname(address.firstName());
		userInformation.setLastname(address.lastName());
		userInformation.setMiddlename(address.firstName());
		userInformation.setEmailAddress(
				userInformation.getFirstname() + "." + userInformation.getLastname() + "@junkmail.com");
		userInformation.setBirthDt(faker.date().birthday(20, 50));
		userInformation.addPhoneNumber(generatePhoneNumber(faker, "cell"));
		if (generateEmployee) {
			generateEmployeeDetails(userInformation, user.getUsername(), faker);
			userInformation.addAddress(generateAddress(address, "home"));
			userInformation.addPhoneNumber(generatePhoneNumber(faker, "home"));
		}
		if (generateCustomer) {
			userInformation.addAddress(generateAddress(address, "billing"));
			if (!sameAddress) {
				userInformation.addAddress(generateAddress(address, "shipping"));
			}
			generateCustomerDetails(userInformation, user.getUsername(), faker);
		}

		userInformationService.saveOrUpdate(userInformation);
		user.setUserInformation(userInformation);

	}

	/**
	 * Generate phone number.
	 *
	 * @param faker the faker
	 * @param type the type
	 * @return the phone number
	 */
	private PhoneNumber generatePhoneNumber(Faker faker, String type) {
		String number = faker.phoneNumber().cellPhone();
		PhoneNumber phoneNumber = new PhoneNumber();
		phoneNumber.setType(type);
		phoneNumber.setCountryCode("1");
		Pattern p = Pattern.compile("[0-9]+");
		Matcher m = p.matcher(number);

		while (m.find()) {
			String group = m.group();
			if (group.length() > 1) {
				if (phoneNumber.getAreacode() == null) {
					phoneNumber.setAreacode(m.group());
				} else if (phoneNumber.getPrefix() == null) {
					phoneNumber.setPrefix(m.group());
				} else if (phoneNumber.getLinenumber() == null) {
					phoneNumber.setLinenumber(m.group());
				}
			}
		}
		phoneNumberService.saveOrUpdate(phoneNumber);
		return phoneNumber;
	}

	/**
	 * Generate address.
	 *
	 * @param fakerAddress the faker address
	 * @param type the type
	 * @return the address
	 */
	private Address generateAddress(com.github.javafaker.Address fakerAddress, String type) {
		Address address = new Address();
		address.setType(type);
		address.setStreet(fakerAddress.streetAddress());
		address.setCity(fakerAddress.cityName());
		address.setState(fakerAddress.stateAbbr());
		address.setCountry("USA");
		address.setPostalcode(fakerAddress.zipCode());
		addressService.saveOrUpdate(address);
		return address;
	}

	/**
	 * Generate customer details.
	 *
	 * @param userInformation the user information
	 * @param username the username
	 * @param faker the faker
	 */
	private void generateCustomerDetails(UserInformation userInformation, String username, Faker faker) {
		CustomerDetail customerDetail = utility.generateCustomerDetails(username, new Faker().numerify("########"));
		customerDetail.setOrderDetails(generateOrders(customerDetail, userInformation, faker));
		customerDetailService.saveOrUpdate(customerDetail);
		userInformation.setCustomerDetail(customerDetail);
	}

	/**
	 * Generate orders.
	 *
	 * @param customerDetail the customer detail
	 * @param userInformation the user information
	 * @param faker the faker
	 * @return the list
	 */
	private List<OrderDetail> generateOrders(CustomerDetail customerDetail, UserInformation userInformation,
			Faker faker) {
		List<OrderDetail> orderDetails = new ArrayList<>();
		int numberOfOrders = faker.number().numberBetween(1, 5);
		for (int i = 0; i < numberOfOrders; i++) {
			orderDetails.add(generateOrderDetail(customerDetail, userInformation, faker, i + 1 != numberOfOrders));
		}
		return orderDetails;
	}

	/**
	 * Generate order detail.
	 *
	 * @param customerDetail the customer detail
	 * @param userInfomation the user infomation
	 * @param faker the faker
	 * @param paidInFull the paid in full
	 * @return the order detail
	 */
	private OrderDetail generateOrderDetail(CustomerDetail customerDetail, UserInformation userInfomation, Faker faker,
			boolean paidInFull) {
		OrderDetail orderDetail = new OrderDetail();
		orderDetail.setUsername(customerDetail.getUsername());
		orderDetail.setCustomerDetail(customerDetail);
		Date orderDate = faker.date().birthday(0, 2);
		orderDetail.setOrderDt(orderDate);
		orderDetail.setEmployeeId(employeeDetailService.getById(1).getEmployeeId());
		orderDetail.setTaxRate(.075);

		orderDetail.setBillingAddress(userInfomation.getAddressByType("billing"));
		orderDetail.setShippingAddress(userInfomation.getAddressByType("shipping"));
		orderDetail.setPhoneNumber(userInfomation.getPhoneNumberByType("cell").toString());

		int numberOfProducts = faker.number().numberBetween(1, 5);
		BigDecimal subTotal = Utility.createCurrency(0.0);

		for (int i = 0; i < numberOfProducts; i++) {
			ProductOrderDetail pod = new ProductOrderDetail();
			Product product = productService.getProductById(faker.number().numberBetween(1, 8));
			if (product.getProductStyle().isEngrave()) {
				pod.setEngrave(true);
			} else {
				pod.setPrint(true);
			}
			Random r = new Random();
			List<String> colors = product.getColors();
			if (colors != null) {
				pod.setColor(colors.get(r.nextInt(colors.size())));
			}
			List<String> sizes = product.getSizes();
			if (sizes != null) {
				pod.setSize(sizes.get(r.nextInt(sizes.size())));
			}
			pod.setProduct(product);
			pod.setFinalPrice(product.getPrice());
			pod.setQuantity(faker.number().numberBetween(1, 50));
			pod.setDescription(faker.chuckNorris().fact());
			subTotal = subTotal.add(pod.subTotal());
			orderDetail.addProductOrderDetail(pod);
		}

		orderDetail.setOrderSubtotal(subTotal);
		orderDetail.setTax(Utility.createCurrency(subTotal.multiply(Utility.createCurrency(orderDetail.getTaxRate()))));
		Payment payment = new Payment();
		payment.setDate(orderDetail.getOrderDt());
		BigDecimal orderTotal = Utility.createCurrency(subTotal.add(orderDetail.getTax()));
		orderDetail.setOrderTotal(orderTotal);
		orderDetail.setOrderFinalized(true);
		orderDetail.setOrderFinalizedDt(orderDate);

		if (paidInFull) {
			payment.setAmount(orderTotal);
			orderDetail.setOrderCompleteDt(orderDate);
			orderDetail.setOrderComplete(true);
			orderDetail.setOrderShippedDt(orderDate);
			orderDetail.setOrderShipped(true);
			orderDetail.setOrderReceivedDt(orderDate);
			orderDetail.setOrderReceived(true);
			payment.setType("Visa");
		} else {
			payment.setAmount(Utility.createCurrency(orderTotal.multiply(Utility.createCurrency(0.10))));
			payment.setType("Cash");
		}
		orderDetail.addPayment(payment);
		orderDetailService.saveOrUpdate(orderDetail);
		return orderDetail;
	}

	/**
	 * Generate employee details.
	 *
	 * @param userInformation the user information
	 * @param username the username
	 * @param faker the faker
	 */
	private void generateEmployeeDetails(UserInformation userInformation, String username, Faker faker) {

		EmployeeDetail employeeDetail = utility.generateEmployeeDetails(username,
				faker.letterify("?") + faker.numerify("######"), faker.date().birthday(4, 6));
		employeeDetailService.saveOrUpdate(employeeDetail);
		userInformation.setEmployeeDetail(employeeDetail);

	}
}
