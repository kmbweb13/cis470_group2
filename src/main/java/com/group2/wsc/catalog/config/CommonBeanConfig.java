/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.config;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The Class CommonBeanConfig. 
 * Creates reusable encryptor
 * 
 */
@Configuration
public class CommonBeanConfig {

	/**
	 * Strong encryptor.
	 *
	 * @return the strong password encryptor
	 */
	@Bean
	public StrongPasswordEncryptor strongEncryptor() {
		StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
		return encryptor;
	}
}
