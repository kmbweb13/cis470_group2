/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;

import com.group2.wsc.catalog.domain.Address;
import com.group2.wsc.catalog.domain.EmployeeDetail;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.services.UserService;

/**
 * The Class AbstractController. Base class for all controllers
 */
public abstract class AbstractController {

	/** The user service. */

	private UserService userService;

	/**
	 * Sets the user service.
	 *
	 * @param userService the new user service
	 */
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * Inits the profile.
	 *
	 * @param model the model
	 * @param success the success
	 * @param fail the fail
	 * @param field the field
	 * @param message the message
	 * @return the model
	 */
	protected Model initProfile(Model model, boolean success, boolean fail, String field, String message) {
		User user = getUser();
		if (user != null) {
			UserInformation userInfo = user.getUserInformation();
			model.addAttribute("currentUser", user);
			model.addAttribute("preferredName", userInfo.getPreferredname());
			model.addAttribute("showAlert", false);
			Address address = userInfo.getAddressByType("billing");
			if (address == null) {
				address = new Address();
				address.setType("billing");
				model.addAttribute("missingAddress", true);
			}
			model.addAttribute("billingAddress", address);

			address = userInfo.getAddressByType("shipping");
			if (address == null) {
				address = new Address();
				address.setType("shipping");
			}
			model.addAttribute("shippingAddress", address);
			if (userInfo.getEmployeeDetail() != null) {
				address = userInfo.getAddressByType("home");
				if (address == null) {
					address = new Address();
					address.setType("home");
				}
				model.addAttribute("homeAddress", address);
			}

			if (success) {
				model.addAttribute(field, true);
				model.addAttribute("alertClass", "alert-success");
				model.addAttribute("updateMessageTitle", "Success");
				model.addAttribute("updateMessage", message);
			}
		}
		return model;
	}

	/**
	 * Inits the profile.
	 *
	 * @param model the model
	 * @return the model
	 */
	protected Model initProfile(Model model) {
		return initProfile(model, false, false, null, null);
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	protected User getUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			Object obj = auth.getPrincipal();
			if (obj instanceof UserDetails) {
				UserDetails prin = (UserDetails) obj;
				User user = userService.findByUsername(prin.getUsername());
				return user;
			}
		}
		return null;
	}

	/**
	 * Load customer.
	 *
	 * @param model the model
	 * @param userName the user name
	 */
	protected void loadCustomer(Model model, String userName) {
		User user = userService.findByUsername(userName);
		if (user != null) {
			UserInformation userInfo = user.getUserInformation();
			model.addAttribute("customer", user);
			model.addAttribute("customerName", userInfo.getFullName());
			model.addAttribute("customerId", userInfo.getCustomerDetail().getCustomerId());
			Address address = userInfo.getAddressByType("billing");
			if (address == null) {
				address = new Address();
				address.setType("billing");
			}
			model.addAttribute("customerBillingAddress", address);
			address = userInfo.getAddressByType("shipping");
			if (address == null) {
				address = new Address();
				address.setType("shipping");
			}
			model.addAttribute("customerShippingAddress", address);
		}
	}

	/**
	 * Load employee.
	 *
	 * @param model the model
	 * @param userName the user name
	 */
	protected void loadEmployee(Model model, String userName) {
		User user = userService.findByUsername(userName);
		if (user != null) {
			String mainRoleName = user.getMainRoleName();
			user.setNewRoleName(("GUEST".equals(mainRoleName) || "CUSTOMER".equals(mainRoleName)) ? "" : mainRoleName);

			UserInformation userInfo = user.getUserInformation();
			model.addAttribute("selectedUser", user);
			model.addAttribute("employeeName", userInfo.getFullName());
			EmployeeDetail employeeDetail = userInfo.getEmployeeDetail();
			if (employeeDetail != null) {
				model.addAttribute("employeeId", userInfo.getEmployeeDetail().getEmployeeId());
				model.addAttribute("hireDt", userInfo.getEmployeeDetail().getHireDt());
			}
			Address address = userInfo.getAddressByType("home");

			if (address == null) {
				address = new Address();
				address.setType("home");
			}
			model.addAttribute("homeAddress", address);

		}
	}
}
