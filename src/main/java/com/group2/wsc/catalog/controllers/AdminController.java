/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.controllers;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.javafaker.Faker;
import com.group2.wsc.catalog.converters.Utility;
import com.group2.wsc.catalog.domain.CustomerDetail;
import com.group2.wsc.catalog.domain.EmployeeDetail;
import com.group2.wsc.catalog.domain.Product;
import com.group2.wsc.catalog.domain.ProductStyle;
import com.group2.wsc.catalog.domain.Role;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.services.CategoryService;
import com.group2.wsc.catalog.services.EmployeeDetailService;
import com.group2.wsc.catalog.services.ProductService;
import com.group2.wsc.catalog.services.ProductStyleService;
import com.group2.wsc.catalog.services.RoleService;
import com.group2.wsc.catalog.services.UserInformationService;
import com.group2.wsc.catalog.services.UserService;

/**
 * The Class AdminController.
 * This class provides routing for admin functions
 */
@Controller
public class AdminController extends AbstractController {

	/** The product style service. */
	@Autowired
	private ProductStyleService productStyleService;

	/** The product service. */
	@Autowired
	private ProductService productService;

	/** The category service. */
	@Autowired
	private CategoryService categoryService;

	/** The employee detail service. */
	@Autowired
	private EmployeeDetailService employeeDetailService;

	/** The user service. */
	@Autowired
	private UserService userService;

	/** The user information service. */
	@Autowired
	private UserInformationService userInformationService;

	/** The role service. */
	@Autowired
	private RoleService roleService;

	/** The utility. */
	@Autowired
	private Utility utility;

	/**
	 * Instantiates a new admin controller.
	 *
	 * @param productStyleService the product style service
	 * @param productService the product service
	 * @param categoryService the category service
	 * @param employeeDetailService the employee detail service
	 * @param userService the user service
	 * @param userInformationService the user information service
	 * @param roleService the role service
	 * @param utility the utility
	 */
	@Autowired
	public AdminController(ProductStyleService productStyleService, ProductService productService,
			CategoryService categoryService, EmployeeDetailService employeeDetailService, UserService userService,
			UserInformationService userInformationService, RoleService roleService, Utility utility) {
		super();
		this.productStyleService = productStyleService;
		this.productService = productService;
		this.categoryService = categoryService;
		this.employeeDetailService = employeeDetailService;
		this.userService = userService;
		this.userInformationService = userInformationService;
		this.roleService = roleService;
		this.utility = utility;
	}

	/** The size list. */
	private final List<String> SIZE_LIST = Arrays.asList("X-Small", "Small", "Medium", "Large", "X-Large", "XX-Large");

	/** The color list. */
	private final List<String> COLOR_LIST = Arrays.asList("Red", "Blue", "Green", "White", "Black", "Orange", "Yellow");

	/**
	 * Index.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("/adminUserMaint")
	String index(Model model) {
		model = initProfile(model);
		return "adminUser";
	}

	/**
	 * Search customers.
	 *
	 * @param userName the user name
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("/searchUser/{userName}")
	String searchCustomers(@PathVariable String userName, Model model) {
		model = initProfile(model);
		model.addAttribute("users",
				userService.findByUsernameContainsIgnoreCase(userName).stream().limit(10).collect(Collectors.toList()));
		return "fragments/userSearchResults :: userSearchResults";
	}

	/**
	 * Load customer.
	 *
	 * @param userName the user name
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("/loadUser/{userName}")
	String loadUser(@PathVariable String userName, Model model) {
		model = initProfile(model);
		loadEmployee(model, userName);
		return "fragments/selectedUser :: selectedUser";
	}

	/**
	 * Creates the user.
	 *
	 * @param user the user
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/updateEmployeeStatus", method = RequestMethod.POST)
	public String updateEmployeeStatus(User user, Model model) {
		String newRoleName = user.getNewRoleName();
		String mainRoleName = user.getMainRoleName();
		if (!mainRoleName.equals(newRoleName)
				&& (newRoleName.length() > 0 || !("GUEST".equals(mainRoleName) || "CUSTOMER".equals(mainRoleName)))) {
			User currentUser = userService.getById(user.getId());
			UserInformation currentUserInfo = currentUser.getUserInformation();
			if (newRoleName.length() == 0) {
				CustomerDetail customerDetail = currentUserInfo.getCustomerDetail();
				newRoleName = (customerDetail == null || customerDetail.getOrderDetails().size() == 0) ? "GUEST"
						: "CUSTOMER";

				if (currentUserInfo.getEmployeeDetail() != null) {
					currentUserInfo.setEmployeeDetail(null);
					userInformationService.saveOrUpdate(currentUserInfo);
				}
			} else if (currentUserInfo.getEmployeeDetail() == null) {
				Faker faker = new Faker();
				EmployeeDetail employeeDetail = utility.generateEmployeeDetails(currentUser.getUsername(),
						faker.letterify("?") + faker.numerify("######"), new Date());
				employeeDetailService.saveOrUpdate(employeeDetail);
				currentUserInfo.setEmployeeDetail(employeeDetail);
				userInformationService.saveOrUpdate(currentUserInfo);
			}

			Role role = roleService.getByRoleName(newRoleName);
			currentUser.setMainRole(role);
			currentUser.setMainRoleName(newRoleName);
			currentUser.setRoles(roleService.getAdditionalRoles(role));
			userService.saveOrUpdate(currentUser);
		}
		return "redirect:/adminUserMaint";

	}

	/**
	 * Adminproductstyle.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/adminproductstyle", method = RequestMethod.GET)
	public String adminproductstyle(Model model) {
		model = initProfile(model);
		model.addAttribute("productStyle", new ProductStyle());
		model.addAttribute("productStyles", productStyleService.listAll());
		model.addAttribute("categories", categoryService.listAll());
		return "adminproductstyle";
	}

	/**
	 * Save product style.
	 *
	 * @param productStyle the product style
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "adminproductstyle/save", method = RequestMethod.POST)
	public String saveProductStyle(ProductStyle productStyle, Model model) {
		model = initProfile(model);
		if (productStyle.getId() != null) {
			productStyle.setProducts(productStyleService.getById(productStyle.getId()).getProducts());
		}
		productStyleService.saveOrUpdate(productStyle);
		return "redirect:/adminproductstyle";
	}

	/**
	 * Edits the product style.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("adminproductstyle/edit/{id}")
	public String editProductStyle(@PathVariable Integer id, Model model) {
		model.addAttribute("productStyles", productStyleService.listAll());
		model.addAttribute("categories", categoryService.listAll());
		model.addAttribute("productStyle", productStyleService.getById(id));
		return "adminproductstyle";
	}

	/**
	 * Delete product style.
	 *
	 * @param id the id
	 * @return the string
	 */
	@RequestMapping("adminproductstyle/delete/{id}")
	public String deleteProductStyle(@PathVariable Integer id) {
		productStyleService.delete(id);
		return "redirect:/adminproductstyle";
	}

	/**
	 * Adminproduct.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/adminproduct", method = RequestMethod.GET)
	public String adminproduct(Model model) {
		model = initProfile(model);
		Product product = new Product();
		product.setColors(COLOR_LIST);
		product.setSizes(SIZE_LIST);
		model.addAttribute("currentProduct", product);
		model.addAttribute("allProductStyles", productStyleService.listAll());
		model.addAttribute("products", productService.listAllProducts());
		return "adminproduct";
	}

	/**
	 * Save product.
	 *
	 * @param product the product
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "adminproduct/save", method = RequestMethod.POST)
	public String saveProduct(Product product, Model model) {
		//model = initProfile(model);
		ProductStyle productStyle = product.getProductStyle();
		productStyle.addProduct(product);

		productService.saveProduct(product);
		productStyleService.saveOrUpdate(productStyle);
		return "redirect:/adminproduct";
	}

	/**
	 * Edits the product.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("adminproduct/edit/{id}")
	public String editProduct(@PathVariable Integer id, Model model) {
		model = initProfile(model);
		model.addAttribute("currentProduct", productService.getProductById(id));
		model.addAttribute("allProductStyles", productStyleService.listAll());
		model.addAttribute("products", productService.listAllProducts());
		model.addAttribute("colors", COLOR_LIST);
		model.addAttribute("sizes", SIZE_LIST);
		return "adminproduct";
	}

	/**
	 * Delete product.
	 *
	 * @param id the id
	 * @return the string
	 */
	@RequestMapping("adminproduct/delete/{id}")
	public String deleteProduct(@PathVariable Integer id) {
		Product product = productService.getProductById(id);
		ProductStyle productStyle = product.getProductStyle();
		productStyle.removeProduct(product);
		productStyleService.saveOrUpdate(productStyle);

		productService.deleteProduct(id);
		return "redirect:/adminproduct";
	}
}
