/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.controllers;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.group2.wsc.catalog.services.CustomerDetailService;

/**
 * The Class CustomersController. This class provides ui access for customer maintenence
 */
@Controller
public class CustomersController extends AbstractController {

	/** The customer detail service. */

	private CustomerDetailService customerDetailService;

	/**
	 * Sets the customer detail service.
	 *
	 * @param customerDetailService the new customer detail service
	 */
	@Autowired
	public void setCustomerDetailService(CustomerDetailService customerDetailService) {
		this.customerDetailService = customerDetailService;
	}

	/**
	 * Index.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("/customers")
	String index(Model model) {
		model = initProfile(model);
		return "customers";
	}

	/**
	 * Search customers.
	 *
	 * @param userName the user name
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("/searchCustomer/{userName}")
	String searchCustomers(@PathVariable String userName, Model model) {
		model = initProfile(model);
		model.addAttribute("customers", customerDetailService.findByUsernameContainsIgnoreCase(userName).stream()
				.limit(10).collect(Collectors.toList()));
		return "fragments/customerSearchResults :: customerSearchResults";
	}

	/**
	 * Load customer.
	 *
	 * @param userName the user name
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("/loadCustomer/{userName}")
	String loadCustomer(@PathVariable String userName, Model model) {
		model = initProfile(model);
		loadCustomer(model, userName);
		return "fragments/selectedCustomer :: selectedCustomer";
	}

}
