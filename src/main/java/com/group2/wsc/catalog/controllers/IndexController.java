/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.group2.wsc.catalog.domain.PhoneNumber;
import com.group2.wsc.catalog.domain.Role;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.services.PhoneNumberService;
import com.group2.wsc.catalog.services.RoleService;
import com.group2.wsc.catalog.services.UserInformationService;
import com.group2.wsc.catalog.services.UserService;

/**
 * The Class IndexController. This class
 */
@Controller
public class IndexController extends AbstractController {

	/** The user service. */
	private UserService userService;

	/** The role service. */
	private RoleService roleService;

	/** The phone number service. */
	private PhoneNumberService phoneNumberService;

	/** The user information service. */
	private UserInformationService userInformationService;

	/** The use mock. Enable functions during local dev - mock */
	@Value("${application.enable.mock}")
	private boolean useMock;

	public void setUseMock(boolean useMock) {
		this.useMock = useMock;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.controllers.AbstractController#setUserService(com.group2.wsc.catalog.services.UserService)
	 */
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * Sets the role service.
	 *
	 * @param roleService the new role service
	 */
	@Autowired
	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	/**
	 * Sets the phone number service.
	 *
	 * @param phoneNumberService the new phone number service
	 */
	@Autowired
	public void setPhoneNumberService(PhoneNumberService phoneNumberService) {
		this.phoneNumberService = phoneNumberService;
	}

	/**
	 * Sets the user information service.
	 *
	 * @param userInformationService the new user information service
	 */
	@Autowired
	public void setUserInformationService(UserInformationService userInformationService) {
		this.userInformationService = userInformationService;
	}

	/**
	 * Index.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("/")
	String index(Model model) {
		model = initProfile(model);
		return "index";
	}

	/**
	 * Login.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		model = initProfile(model);
		return "login";
	}

	/**
	 * Signup.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Model model) {

		User user = new User();
		UserInformation userInfo = new UserInformation();
		userInfo.addPhoneNumber(new PhoneNumber());
		user.setUserInformation(userInfo);

		model.addAttribute("currentUser", user);
		return "signup";
	}

	/**
	 * Creates the user.
	 *
	 * @param user the user
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "createGuest", method = RequestMethod.POST)
	public String createUser(User user, Model model) {

		UserInformation userInfo = user.getUserInformation();

		phoneNumberService.saveOrUpdate(userInfo.getPhonenumbers().get(0));
		String tempDate = userInfo.getTempBirthDt();
		if (tempDate != null && tempDate.length() > 5) {
			try {
				userInfo.setBirthDt((new SimpleDateFormat("yyyy-MM-dd")).parse(tempDate));
			} catch (ParseException e) {
				// do nothing bad date not critical for operation.
			}
		}
		userInformationService.saveOrUpdate(userInfo);
		Role role = roleService.getByRoleName("GUEST");
		user.setMainRoleName("GUEST");
		user.setMainRole(role);
		user.setRoles(roleService.getAdditionalRoles(role));

		userService.saveOrUpdate(user);

		return login(model);
	}

	/**
	 * Login 2.short cut for logging in during mock situations.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/login2", method = RequestMethod.GET)
	public String login2(Model model) {
		model = initProfile(model);
		return useMock ? "login2" : "login";

	}

}
