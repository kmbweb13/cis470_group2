/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.group2.wsc.catalog.converters.Utility;
import com.group2.wsc.catalog.domain.OrderDetail;
import com.group2.wsc.catalog.domain.Payment;
import com.group2.wsc.catalog.services.OrderDetailService;

/**
 * The Class OrdersController. Class for generate and maintain orders
 */
@Controller
public class OrdersController extends AbstractController {

	/** The order detail service. */

	private OrderDetailService orderDetailService;

	/**
	 * Sets the order detail service.
	 *
	 * @param orderDetailService the new order detail service
	 */
	@Autowired
	public void setOrderDetailService(OrderDetailService orderDetailService) {
		this.orderDetailService = orderDetailService;
	}

	/**
	 * Load order.
	 *
	 * @param id the id
	 * @param customerName the customer name
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("/loadOrder/{id}/{customer}")
	String loadOrder(@PathVariable("id") String id, @PathVariable("customer") String customerName, Model model) {
		model = initProfile(model);
		model.addAttribute("disableEditOrder", !getUser().getUsername().equals(customerName));
		model.addAttribute("customerName", customerName);
		model.addAttribute("selectedCustomerOrder", orderDetailService.getById(Integer.parseInt(id)));
		return "fragments/selectedOrder :: selectedOrder";
	}

	/**
	 * Adds the payment.
	 *
	 * @param id the id
	 * @param paymentAmount the payment amount
	 * @param type the type
	 * @param customerName the customer name
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("/addPayment/{orderId}/{paymentAmount}/{type}/{customer}")
	public String addPayment(@PathVariable("orderId") int id, @PathVariable("paymentAmount") String paymentAmount,
			@PathVariable("type") String type, @PathVariable("customer") String customerName, Model model) {

		try {
			Payment payment = new Payment();
			payment.setDate(new Date());
			payment.setType(type);
			payment.setAmount(Utility.createCurrency((Double.parseDouble(paymentAmount)) / 100));
			OrderDetail orderDetail = orderDetailService.getById(id);
			orderDetail.addPayment(payment);
			orderDetailService.saveOrUpdate(orderDetail);
		} catch (Exception e) {
			// dont add payment
		}

		model = initProfile(model);
		model.addAttribute("disableEditOrder", !getUser().getUsername().equals(customerName));
		model.addAttribute("customerName", customerName);
		model.addAttribute("selectedCustomerOrder", orderDetailService.getById(id));

		return "fragments/selectedOrderModalDetail :: selectedOrderModalDetail";
	}

	/**
	 * Update order.
	 *
	 * @param id the id
	 * @param shipped the shipped
	 * @param received the received
	 * @param approved the approved
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("/updateOrder/{orderId}/{shipped}/{received}/{approved}")
	public String updateOrder(@PathVariable("orderId") int id, @PathVariable("shipped") boolean shipped,
			@PathVariable("received") boolean received, @PathVariable("approved") boolean approved, Model model) {
		Date today = new Date();
		OrderDetail orderDetail = orderDetailService.getById(id);
		String userName = orderDetail.getUsername();
		orderDetail.setOrderFinalized(approved);
		if (approved && orderDetail.getOrderFinalizedDt() == null) {
			orderDetail.setOrderFinalizedDt(today);
		} else if (!approved) {
			orderDetail.setOrderFinalizedDt(null);
		}
		orderDetail.setOrderShipped(shipped);
		if (shipped && orderDetail.getOrderShippedDt() == null) {
			orderDetail.setOrderShippedDt(today);
		} else if (!shipped) {
			orderDetail.setOrderShippedDt(null);
		}
		orderDetail.setOrderReceived(received);
		if (received && orderDetail.getOrderReceivedDt() == null) {
			orderDetail.setOrderReceivedDt(today);
		} else if (!received) {
			orderDetail.setOrderReceivedDt(null);
		}
		boolean orderComplete = received && shipped && approved;
		orderDetail.setOrderComplete(orderComplete);
		if (orderComplete) {
			orderDetail.setOrderCompleteDt(today);
		}

		orderDetailService.saveOrUpdate(orderDetail);
		return "redirect:/loadCustomer/" + userName;
	}

	/**
	 * Update order.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("/cancelOrderEmployee/{orderId}")
	public String cancelOrderEmployee(@PathVariable("orderId") int id, Model model) {

		OrderDetail orderDetail = orderDetailService.getById(id);
		String userName = orderDetail.getUsername();
		orderDetail.setOrderCanceled(true);
		orderDetailService.saveOrUpdate(orderDetail);
		return "redirect:/loadCustomer/" + userName;
	}

	/**
	 * Update order.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("/cancelOrderCustomer/{orderId}")
	public String cancelOrderCustomer(@PathVariable("orderId") int id, Model model) {

		OrderDetail orderDetail = orderDetailService.getById(id);
		orderDetail.setOrderCanceled(true);
		orderDetailService.saveOrUpdate(orderDetail);
		model = initProfile(model);
		model.addAttribute("customer", getUser());

		return "fragments/orderHistory :: orderHistory";
	}
}
