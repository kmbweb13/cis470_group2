/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.group2.wsc.catalog.domain.Product;
import com.group2.wsc.catalog.domain.ProductOrderDetail;
import com.group2.wsc.catalog.domain.ProductStyle;
import com.group2.wsc.catalog.services.ProductService;
import com.group2.wsc.catalog.services.ProductStyleService;

/**
 * The Class ProductController.
 * Controller for maintaining and viewing products in catalog
 */
@Controller
public class ProductController extends AbstractController {

	/** The product service. */

	private ProductService productService;

	/** The product style service. */
	private ProductStyleService productStyleService;

	/**
	 * Sets the product service.
	 *
	 * @param productService the new product service
	 */
	@Autowired
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	/**
	 * Sets the product style service.
	 *
	 * @param productStyleService the new product style service
	 */
	@Autowired
	public void setProductStyleService(ProductStyleService productStyleService) {
		this.productStyleService = productStyleService;
	}

	/**
	 * List.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public String list(Model model) {
		model = initProfile(model);
		model.addAttribute("products", productService.listAllProducts());
		return "products";
	}

	/**
	 * Show product.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("product/show/{id}")
	public String showProduct(@PathVariable Integer id, Model model) {
		model = initProfile(model);
		Product product = productService.getProductById(id);
		ProductOrderDetail pod = new ProductOrderDetail();
		pod.setProduct(product);
		pod.setProductId(product.getId());
		pod.setFinalPrice(product.getPrice());
		pod.setQuantity(1);
		boolean print = product.getProductStyle().isPrint();
		if (print ^ product.getProductStyle().isEngrave()) {
			pod.setMarkStyle(print ? "print" : "engrave");
		}
		model.addAttribute("product", product);
		model.addAttribute("productOrderDetail", pod);
		return "productshow";
	}

	/**
	 * Edits the.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("product/edit/{id}")
	public String edit(@PathVariable Integer id, Model model) {
		model = initProfile(model);
		model.addAttribute("product", productService.getProductById(id));
		return "productform";
	}

	/**
	 * New product.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("product/new")
	public String newProduct(Model model) {
		model = initProfile(model);
		model.addAttribute("product", new Product());
		model.addAttribute("productStyles", productStyleService.listAll());
		return "productform";
	}

	/**
	 * Save product.
	 *
	 * @param product the product
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "product", method = RequestMethod.POST)
	public String saveProduct(Product product, Model model) {
		productService.saveProduct(product);
		return "redirect:/product/show/" + product.getId();
	}

	/**
	 * Delete.
	 *
	 * @param id the id
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping("product/delete/{id}")
	public String delete(@PathVariable Integer id, Model model) {
		Product product = productService.getProductById(id);
		ProductStyle style = productStyleService.getById(product.getProductStyle().getId());
		style.removeProduct(product);
		productStyleService.saveOrUpdate(style);
		productService.deleteProduct(id);
		return "redirect:/products";
	}
}
