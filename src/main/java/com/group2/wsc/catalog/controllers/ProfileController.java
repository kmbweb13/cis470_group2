/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.group2.wsc.catalog.domain.Address;
import com.group2.wsc.catalog.domain.PhoneNumber;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.services.AddressService;
import com.group2.wsc.catalog.services.PhoneNumberService;
import com.group2.wsc.catalog.services.UserInformationService;
import com.group2.wsc.catalog.services.UserService;

/**
 * The Class ProfileController. Controller for maintaining and viewing users profile
 */
@Controller
public class ProfileController extends AbstractController {

	/** The address service. */
	@Autowired
	private AddressService addressService;

	/** The user service. */
	@Autowired
	private UserService userService;

	/** The user information service. */
	@Autowired
	private UserInformationService userInformationService;

	/** The phone number service. */
	@Autowired
	private PhoneNumberService phoneNumberService;

	/**
	 * Instantiates a new profile controller.
	 *
	 * @param addressService the address service
	 * @param userService the user service
	 * @param userInformationService the user information service
	 * @param phoneNumberService the phone number service
	 */
	@Autowired
	public ProfileController(AddressService addressService, UserService userService,
			UserInformationService userInformationService, PhoneNumberService phoneNumberService) {
		this.addressService = addressService;
		this.userService = userService;
		this.userInformationService = userInformationService;
		this.phoneNumberService = phoneNumberService;
	}

	/**
	 * Userprofile.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/userprofile", method = RequestMethod.GET)
	public String userprofile(Model model) {
		model = initProfile(model);
		model.addAttribute("customer", getUser());
		return "userprofile";
	}

	/**
	 * Userprofile success.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/userprofileSuccess", method = RequestMethod.GET)
	public String userprofileSuccess(Model model) {

		model = initProfile(model, true, false, "showProfileAlert", "Profile Updated");
		model.addAttribute("customer", getUser());
		return "userprofile";
	}

	/**
	 * Billing address success.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/userBillingSuccess", method = RequestMethod.GET)
	public String billingAddressSuccess(Model model) {

		model = initProfile(model, true, false, "showBillingAlert", "Address Updated");
		model.addAttribute("customer", getUser());
		model.addAttribute("displayTab", "addressTab");
		return "userprofile";
	}

	/**
	 * Shipping address success.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/userShippingSuccess", method = RequestMethod.GET)
	public String shippingAddressSuccess(Model model) {

		model = initProfile(model, true, false, "showShippingAlert", "Address Updated");
		model.addAttribute("customer", getUser());
		model.addAttribute("displayTab", "addressTab");
		return "userprofile";
	}

	/**
	 * Shipping home success.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "/userHomeSuccess", method = RequestMethod.GET)
	public String shippingHomeSuccess(Model model) {

		model = initProfile(model, true, false, "showHomeAlert", "Address Updated");
		model.addAttribute("customer", getUser());
		model.addAttribute("displayTab", "employeeTab");
		return "userprofile";
	}

	/**
	 * Edituserprofile.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/edituserprofile")
	public String edituserprofile(Model model) {
		model = initProfile(model);
		model.addAttribute("editibleProfile", true);
		return "fragments/userProfileContent :: userProfileContent";
	}

	/**
	 * Canceledituserprofile.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/canceledituserprofile")
	public String canceledituserprofile(Model model) {
		model = initProfile(model);
		return "fragments/userProfileContent :: userProfileContent";
	}

	/**
	 * Update user.
	 *
	 * @param user the user
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "updateUserProfile", method = RequestMethod.POST)
	public String updateUser(User user, Model model) {
		UserInformation userInfo = user.getUserInformation();
		User storedUser = userService.getById(user.getId());
		UserInformation storedUserInfo = storedUser.getUserInformation();

		storedUserInfo.setPreferredname(userInfo.getPreferredname());
		storedUserInfo.setFirstname(userInfo.getFirstname());
		storedUserInfo.setLastname(userInfo.getLastname());
		storedUserInfo.setEmailAddress(userInfo.getEmailAddress());
		List<PhoneNumber> phoneNumbers = userInfo.getPhonenumbers();
		if (phoneNumbers.size() > 0) {
			for (PhoneNumber phoneNumber : phoneNumbers) {
				updatePhoneNumber(phoneNumber);
			}
		}
		String tempDate = userInfo.getTempBirthDt();
		if (tempDate != null && tempDate.length() > 5) {
			try {
				storedUserInfo.setBirthDt((new SimpleDateFormat("yyyy-MM-dd")).parse(tempDate));
			} catch (ParseException e) {
				// do nothing bad date not critical for operation.
			}
		}

		userInformationService.saveOrUpdate(storedUserInfo);
		return "redirect:/userprofileSuccess";
	}

	/**
	 * Update phone number.
	 *
	 * @param phoneNumber the phone number
	 */
	private void updatePhoneNumber(PhoneNumber phoneNumber) {
		PhoneNumber storedPhoneNumber = phoneNumberService.getById(phoneNumber.getId());
		storedPhoneNumber.setType(phoneNumber.getType());
		storedPhoneNumber.setCountryCode(phoneNumber.getCountryCode());
		storedPhoneNumber.setAreacode(phoneNumber.getAreacode());
		storedPhoneNumber.setPrefix(phoneNumber.getPrefix());
		storedPhoneNumber.setLinenumber(phoneNumber.getLinenumber());
		phoneNumberService.saveOrUpdate(storedPhoneNumber);
	}

	/**
	 * Edits the shipping address.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/editShippingAddress")
	public String editShippingAddress(Model model) {
		model = initProfile(model);
		model.addAttribute("editibleShippingAddress", true);
		return "fragments/userProfileAddress :: userProfileAddress";
	}

	/**
	 * Edits the billing address.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/editBillingAddress")
	public String editBillingAddress(Model model) {
		model = initProfile(model);
		model.addAttribute("editibleBillingAddress", true);
		return "fragments/userProfileAddress :: userProfileAddress";
	}

	/**
	 * Edits the home address.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/editHomeAddress")
	public String editHomeAddress(Model model) {
		model = initProfile(model);
		model.addAttribute("editibleHomeAddress", true);
		return "fragments/userProfileEmployee :: userProfileEmployee";
	}

	/**
	 * Cancel edit shipping address.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/cancelEditAddress")
	public String cancelEditShippingAddress(Model model) {
		model = initProfile(model);
		return "fragments/userProfileAddress :: userProfileAddress";
	}

	/**
	 * Cancel edit home address.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping(value = "/cancelEditHomeAddress")
	public String cancelEditHomeAddress(Model model) {
		model = initProfile(model);
		return "fragments/userProfileEmployee :: userProfileEmployee";
	}

	/**
	 * Update address.
	 *
	 * @param address the address
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "updateAddress", method = RequestMethod.POST)
	public String updateAddress(Address address, Model model) {
		if (address.getId() == null) {
			address = addressService.saveOrUpdate(address);
			User user = getUser();
			user.getUserInformation().addAddress(address);
			userService.saveOrUpdate(user);

		} else {
			Address storredAddress = addressService.getById(address.getId());
			storredAddress.setStreet(address.getStreet());
			storredAddress.setStreet2(address.getStreet2());
			storredAddress.setCity(address.getCity());
			storredAddress.setState(address.getState());
			storredAddress.setPostalcode(address.getPostalcode());
			storredAddress.setCountry(address.getCountry());
			addressService.saveOrUpdate(storredAddress);
		}
		if ("billing".equals(address.getType())) {

			return "redirect:/userBillingSuccess";
		}
		if ("home".equals(address.getType())) {

			return "redirect:/userHomeSuccess";
		}
		return "redirect:/userShippingSuccess";
	}
}
