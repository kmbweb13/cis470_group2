/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.group2.wsc.catalog.domain.Payment;
import com.group2.wsc.catalog.domain.Product;
import com.group2.wsc.catalog.domain.ProductOrderDetail;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.services.ProductService;
import com.group2.wsc.catalog.services.ShoppingCartService;

/**
 * The Class ShoppingCartController. Controller for access to the shoppign cart
 */
@Controller
public class ShoppingCartController extends AbstractController {

	/** The shopping cart service. */
	@Autowired
	private ShoppingCartService shoppingCartService;

	/** The product service. */
	@Autowired
	private ProductService productService;

	/**
	 * Instantiates a new shopping cart controller.
	 *
	 * @param shoppingCartService the shopping cart service
	 * @param productService the product service
	 */
	@Autowired
	public ShoppingCartController(ShoppingCartService shoppingCartService, ProductService productService) {
		this.shoppingCartService = shoppingCartService;
		this.productService = productService;
	}

	/**
	 * Shopping cart.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping("/shoppingCart")
	public String shoppingCart(Model model) {
		model = initProfile(model);
		model.addAttribute("productOrderDetails", shoppingCartService.getProductOrderDetailsInCart());
		model.addAttribute("subtotal", shoppingCartService.getSubTotal());
		model.addAttribute("tax", shoppingCartService.getTax());
		model.addAttribute("total", shoppingCartService.getTotal());
		model.addAttribute("minPayment", shoppingCartService.getMinPayment());
		model.addAttribute("existingPayments", shoppingCartService.getExistingPaymentsTotal());
		Integer orderId = shoppingCartService.getExistingOrderid();
		Payment payment = new Payment();
		if (orderId != null) {
			model.addAttribute("updateOrder", true);
			payment.setOrderId(orderId);
		}
		model.addAttribute("payment", payment);
		return "shoppingCart";
	}

	/**
	 * Clear cart.
	 *
	 * @param model the model
	 * @return the string
	 */
	@GetMapping("/shoppingCart/clear")
	public String clearCart(Model model) {
		shoppingCartService.clearCart();
		return "redirect:/products";
	}

	/**
	 * Adds the product to cart.
	 *
	 * @param productOrderDetail the product order detail
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "addProductOrder", method = RequestMethod.POST)
	public String addProductToCart(ProductOrderDetail productOrderDetail, Model model) {
		productOrderDetail.setProduct(productService.getProductById(productOrderDetail.getProductId()));
		User user = getUser();
		productOrderDetail.setUserId(user.getUsername());
		shoppingCartService.addProductOrderDetail(productOrderDetail);
		return "redirect:/shoppingCart";
	}

	/**
	 * Removes the product from cart.
	 *
	 * @param productOrderDetailId the product order detail id
	 * @param model the model
	 * @return the string
	 */
	@GetMapping("/shoppingCart/removeProduct/{productOrderDetailId}")
	public String removeProductFromCart(@PathVariable("productOrderDetailId") String productOrderDetailId,
			Model model) {
		shoppingCartService.removeProductOrderDetail(Integer.parseInt(productOrderDetailId));
		return shoppingCart(model);
	}

	/**
	 * Edits the product in cart.
	 *
	 * @param productOrderDetailId the product order detail id
	 * @param model the model
	 * @return the string
	 */
	@GetMapping("/shoppingCart/editProduct/{productOrderDetailId}")
	public String editProductInCart(@PathVariable("productOrderDetailId") String productOrderDetailId, Model model) {
		model = initProfile(model);
		ProductOrderDetail pod = shoppingCartService
				.getProductOrderDetailFromCart(Integer.parseInt(productOrderDetailId));
		Product product = productService.getProductById(pod.getProductId());

		model.addAttribute("product", product);
		model.addAttribute("productOrderDetail", pod);
		return "productshow";
	}

	/**
	 * Edits the existing product.
	 *
	 * @param orderDetailId the order detail id
	 * @param model the model
	 * @return the string
	 */
	@GetMapping("/shoppingCart/editExistingOrder/{orderDetailId}")
	public String editExistingProduct(@PathVariable("orderDetailId") String orderDetailId, Model model) {
		shoppingCartService.loadPreviousOpenOrder(Integer.parseInt(orderDetailId));
		return shoppingCart(model);
	}

	/**
	 * Checkout.
	 *
	 * @param payment the payment
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "shoppingCart/checkout", method = RequestMethod.POST)
	public String checkout(Payment payment, Model model) {
		payment.setDate(new Date());
		shoppingCartService.checkout(payment, getUser());
		return shoppingCart(model);
	}
}
