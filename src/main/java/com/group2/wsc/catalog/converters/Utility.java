/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.converters;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.group2.wsc.catalog.domain.CustomerDetail;
import com.group2.wsc.catalog.domain.EmployeeDetail;
import com.group2.wsc.catalog.services.CustomerDetailService;
import com.group2.wsc.catalog.services.EmployeeDetailService;

/**
 * The Class Utility. Utility functions generate employee and customer details, Create re-usable currency
 */
@Component
public class Utility {

	/** The customer detail service. */

	private CustomerDetailService customerDetailService;

	/** The employee detail service. */

	private EmployeeDetailService employeeDetailService;
	/** The log. */
	private static Logger log = Logger.getLogger(Utility.class);

	/**
	 * Sets the customer detail service.
	 *
	 * @param customerDetailService the new customer detail service
	 */
	@Autowired
	public void setCustomerDetailService(CustomerDetailService customerDetailService) {
		this.customerDetailService = customerDetailService;
	}

	/**
	 * Sets the employee detail service.
	 *
	 * @param employeeDetailService the new employee detail service
	 */
	@Autowired
	public void setEmployeeDetailService(EmployeeDetailService employeeDetailService) {
		this.employeeDetailService = employeeDetailService;
	}

	/**
	 * Creates the currency.
	 *
	 * @param amount the amount
	 * @return the big decimal
	 */
	public static BigDecimal createCurrency(double amount) {
		return new BigDecimal(amount).setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * Creates the currency.
	 *
	 * @param amount the amount
	 * @return the big decimal
	 */
	public static BigDecimal createCurrency(String amount) {
		try {
			return new BigDecimal(amount).setScale(2, BigDecimal.ROUND_HALF_UP);
		} catch (NumberFormatException nfe) {
			log.warn("Invalid amount");
		}
		return new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * Creates the currency.
	 *
	 * @param bigDecimal the big decimal
	 * @return the big decimal
	 */
	public static BigDecimal createCurrency(BigDecimal bigDecimal) {
		return createCurrency(bigDecimal.toString());
	}

	/**
	 * Generate customer details.
	 *
	 * @param username the username
	 * @param id the id
	 * @return the customer detail
	 */
	public CustomerDetail generateCustomerDetails(String username, String id) {
		CustomerDetail customerDetail = new CustomerDetail();
		customerDetail.setCustomerId(id);

		customerDetail.setUsername(username);
		customerDetail = customerDetailService.saveOrUpdate(customerDetail);
		return customerDetail;
	}

	/**
	 * Generate employee details.
	 *
	 * @param username the username
	 * @param id the id
	 * @param hireDate the hire date
	 * @return the employee detail
	 */
	public EmployeeDetail generateEmployeeDetails(String username, String id, Date hireDate) {
		EmployeeDetail employeeDetail = new EmployeeDetail();
		employeeDetail.setEmployeeId(id);
		employeeDetail.setHireDt(hireDate);
		employeeDetail.setUsername(username);
		employeeDetail = employeeDetailService.saveOrUpdate(employeeDetail);
		return employeeDetail;
	}
}
