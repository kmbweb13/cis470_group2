/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import javax.persistence.*;

import lombok.Data;

import java.util.Date;

/**
 * Instantiates a new abstract domain class. Base class for DB objects Enables CRUD compatibility
 */
@Data
@MappedSuperclass
public class AbstractDomainClass implements DomainObject {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer id;

	/** The version. */
	@Version
	private Integer version;

	/** The date created. */
	private Date dateCreated;

	/** The last updated. */
	private Date lastUpdated;

	/**
	 * Update time stamps.
	 */
	@PreUpdate
	@PrePersist
	public void updateTimeStamps() {
		Date date = new Date();
		lastUpdated = date;
		if (dateCreated == null) {
			dateCreated = date;
		}
	}
}
