/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import java.io.Serializable;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new address.
 */
@Data

/*
 * (non-Javadoc)
 * @see com.group2.wsc.catalog.domain.AbstractDomainClass#hashCode()
 */
@EqualsAndHashCode(callSuper = false)
@Entity
public class Address extends AbstractDomainClass implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1511523058382764468L;

	/** The type. */
	private String type;

	/** The street. */
	private String street;

	/** The street 2. */
	private String street2;

	/** The city. */
	private String city;

	/** The state. */
	private String state;

	/** The postalcode. */
	private String postalcode;

	/** The country. */
	private String country;
}
