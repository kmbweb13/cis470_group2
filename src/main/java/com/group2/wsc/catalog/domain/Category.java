/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

import lombok.Data;

/**
 * Instantiates a new category.
 */
@Data
@Entity
public class Category implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1928657803784911381L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	/** The version. */
	@Version
	private Integer version;

	/** The catagory code. */
	private String catagoryCode;

	/** The label. */
	private String label;

}