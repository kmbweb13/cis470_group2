/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new customer detail.
 */
@Data

/*
 * (non-Javadoc)
 * @see com.group2.wsc.catalog.domain.AbstractDomainClass#hashCode()
 */
@EqualsAndHashCode(callSuper = false)
@Entity
public class CustomerDetail extends AbstractDomainClass {

	/** The id. */
	@Id
	@Column(name = "customerDetailId")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	/** The customer id. */
	private String customerId;

	/** The username. */
	private String username;

	/** The order details. */
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable
	private List<OrderDetail> orderDetails = new ArrayList<>();

	/**
	 * Adds the order detail.
	 *
	 * @param orderDetail the order detail
	 */
	public void addOrderDetail(OrderDetail orderDetail) {
		if (!this.orderDetails.contains(orderDetail)) {
			this.orderDetails.add(orderDetail);
		}
	}

	/**
	 * Removes the product style.
	 *
	 * @param orderDetail the order detail
	 */
	public void removeOrderDetail(OrderDetail orderDetail) {
		this.orderDetails.remove(orderDetail);
	}
}
