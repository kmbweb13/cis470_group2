/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

/**
 * The Interface DomainObject.
 */
public interface DomainObject {

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	Integer getId();

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	void setId(Integer id);
}
