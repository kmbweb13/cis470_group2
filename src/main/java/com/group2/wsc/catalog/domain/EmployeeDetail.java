/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new employee detail.
 */
@Data

/*
 * (non-Javadoc)
 * @see com.group2.wsc.catalog.domain.AbstractDomainClass#hashCode()
 */
@EqualsAndHashCode(callSuper = false)
@Entity
public class EmployeeDetail extends AbstractDomainClass {

	/** The id. */
	@Id
	@Column(name = "employeeDetailId")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	/** The employee id. */
	private String employeeId;

	/** The hire dt. */
	private Date hireDt;

	/** The username. */
	private String username;

}
