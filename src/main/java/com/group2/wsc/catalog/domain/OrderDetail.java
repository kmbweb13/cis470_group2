/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;

import com.group2.wsc.catalog.converters.Utility;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * Instantiates a new order detail.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class OrderDetail extends AbstractDomainClass {

	/** The order dt. */
	private Date orderDt;

	/** The phone number. */
	private String phoneNumber;

	/** The tax rate. */
	private double taxRate;

	/** The employee id. */
	private String employeeId;

	/** The username. */
	private String username;

	/** The order canceled. */
	private boolean orderCanceled;

	/** The order complete. */
	private boolean orderComplete;

	/** The order shipped. */
	private boolean orderShipped;

	/** The order received. */
	private boolean orderReceived;

	/** The order finalized. */
	private boolean orderFinalized;

	/** The order canceled dt. */
	private Date orderCanceledDt;

	/** The order complete dt. */
	private Date orderCompleteDt;

	/** The order shipped dt. */
	private Date orderShippedDt;

	/** The order received dt. */
	private Date orderReceivedDt;

	/** The order finalized dt. */
	private Date orderFinalizedDt;

	/** The order total. */
	private BigDecimal orderTotal;

	/** The order subtotal. */
	private BigDecimal orderSubtotal;

	/** The tax. */
	private BigDecimal tax;

	/** The customer detail. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CustomerDetailId")
	private CustomerDetail customerDetail;

	/** The payments. */
	@Type(type = "serializable")
	@Column(length = 2000)
	private List<Payment> payments = new ArrayList<>();

	/** The billing address. */
	@Type(type = "serializable")
	@Column(length = 1000)
	private Address billingAddress;

	/** The shipping address. */
	@Type(type = "serializable")
	@Column(length = 1000)
	private Address shippingAddress;

	/** The product order details. */
	@Type(type = "serializable")
	@Column(length = 5000)
	private List<ProductOrderDetail> productOrderDetails = new ArrayList<>();

	/**
	 * Adds the payment.
	 *
	 * @param payment the payment
	 */
	public void addPayment(Payment payment) {
		this.payments.add(payment);
	}

	/**
	 * Adds the product order detail.
	 *
	 * @param productOrderDetail the product order detail
	 */
	public void addProductOrderDetail(ProductOrderDetail productOrderDetail) {
		if (!this.productOrderDetails.contains(productOrderDetail)) {
			this.productOrderDetails.add(productOrderDetail);
		}
	}

	/**
	 * Removes the product order detail.
	 *
	 * @param productOrderDetail the product order detail
	 */
	public void removeProductOrderDetail(ProductOrderDetail productOrderDetail) {
		this.productOrderDetails.remove(productOrderDetail);
	}

	/**
	 * Outstanding balance.
	 *
	 * @return the big decimal
	 */
	public BigDecimal outstandingBalance() {
		BigDecimal paymentsTotal = Utility.createCurrency(0);
		for (Payment payment : payments) {
			paymentsTotal = Utility.createCurrency(paymentsTotal.add(payment.getAmount()));
		}
		return Utility.createCurrency(orderTotal.subtract(paymentsTotal));
	}
}