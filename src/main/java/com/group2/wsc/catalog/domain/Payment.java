/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Transient;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new payment.
 */
@Data

/*
 * (non-Javadoc)
 * @see java.lang.Object#hashCode()
 */
@EqualsAndHashCode(exclude = { "orderId" })
public class Payment implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5961923376717054415L;

	/** The date. */
	private Date date;

	/** The amount. */
	private BigDecimal amount;

	/** The type. */
	private String type;

	/** The order id. */
	@Transient
	private Integer orderId;
}
