/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new phone number.
 */
@Data

/*
 * (non-Javadoc)
 * @see com.group2.wsc.catalog.domain.AbstractDomainClass#hashCode()
 */
@EqualsAndHashCode(callSuper = false)
@Entity
public class PhoneNumber extends AbstractDomainClass {

	/** The type. */
	private String type;

	/** The country code. */
	private String countryCode;

	/** The areacode. */
	private String areacode;

	/** The prefix. */
	private String prefix;

	/** The linenumber. */
	private String linenumber;

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.domain.AbstractDomainClass#toString()
	 */
	@Override
	public String toString() {
		return "(" + areacode + ") " + prefix + "-" + linenumber;
	}

	/**
	 * To string with country code.
	 *
	 * @return the string
	 */
	public String toStringWithCountryCode() {
		return "+" + countryCode + " " + this.toString();
	}

	/**
	 * Display value.
	 *
	 * @return the string
	 */
	public String displayValue() {
		return type + " : " + toString();
	}
}
