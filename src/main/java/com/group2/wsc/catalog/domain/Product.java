/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import org.hibernate.annotations.Type;

import lombok.Data;

/**
 * Instantiates a new product.
 */
@Data
@Entity
public class Product implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1553300601690899903L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	/** The version. */
	@Version
	private Integer version;

	/** The media catalog number. */
	private String mediaCatalogNumber;

	/** The image url. */
	private String imageUrl;

	/** The price. */
	private BigDecimal price;

	/** The description. */
	private String description;

	/** The sizes. */
	@Type(type = "serializable")
	private List<String> sizes;

	/** The colors. */
	@Type(type = "serializable")
	private List<String> colors;

	/** The product style. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "productStyle")
	private ProductStyle productStyle;

	@Override
	public boolean equals(Object o) {

		// If the object is compared with itself then return true
		if (o == this) {
			return true;
		}

		/*
		 * Check if o is an instance of Product or not "null instanceof [type]" also returns false
		 */
		if (!(o instanceof Product)) {
			return false;
		}

		Product c = (Product) o;

		return this.getId() == c.getId();
	}

}
