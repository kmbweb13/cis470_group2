/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Transient;

import com.group2.wsc.catalog.converters.Utility;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new product order detail.
 */
@Data

/*
 * (non-Javadoc)
 * @see java.lang.Object#hashCode()
 */
@EqualsAndHashCode(exclude = { "userId", "markStyle", "id", "productId", "removed" })
public class ProductOrderDetail implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2900130584250103858L;

	/** The product. */
	private Product product;

	/** The description. */
	private String description;

	/** The engrave. */
	private boolean engrave;

	/** The print. */
	private boolean print;

	/** The size. */
	private String size;

	/** The color. */
	private String color;

	/** The final price. */
	private BigDecimal finalPrice = new BigDecimal(0);

	/** The quantity. */
	private int quantity;

	/** The user id. */
	@Transient
	private String userId;

	/** The mark style. */
	@Transient
	private String markStyle;

	/** The id. */
	@Transient
	private Integer id;

	/** The product id. */
	@Transient
	private Integer productId;

	/** The removed. */
	@Transient
	private boolean removed;

	/**
	 * Sub total.
	 *
	 * @return the big decimal
	 */
	public BigDecimal subTotal() {
		return Utility.createCurrency(finalPrice.multiply(new BigDecimal(quantity)));
	}

}
