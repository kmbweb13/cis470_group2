/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import lombok.Data;

/**
 * Instantiates a new product style.
 */
@Data
@Entity
public class ProductStyle implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9027235892277419955L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	/** The version. */
	@Version
	private Integer version;

	/** The code. */
	private String code;

	/** The label. */
	private String label;

	/** The engrave. */
	private boolean engrave;

	/** The print. */
	private boolean print;

	/** The category. */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "category")
	private Category category;

	/** The products. */
	@OneToMany(fetch = FetchType.EAGER)
	@JoinTable
	private List<Product> products = new ArrayList<>();

	/**
	 * Adds the product.
	 *
	 * @param product the product
	 */
	public void addProduct(Product product) {
		int index = this.products.indexOf(product);
		if (index == -1) {
			this.products.add(product);
		}
		else {
			this.products.set(index, product);
		}
		product.setProductStyle(this);
	}

	/**
	 * Removes the product.
	 *
	 * @param product the product
	 */
	public void removeProduct(Product product) {
		this.products.remove(product);
		product.setProductStyle(null);
	}

	/**
	 * Checks for products.
	 *
	 * @return true, if successful
	 */
	public boolean hasProducts() {
		return products.size() > 0;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductStyle.class";
	}

}
