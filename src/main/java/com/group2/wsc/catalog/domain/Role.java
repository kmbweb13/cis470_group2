/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new role.
 */
@Data

/*
 * (non-Javadoc)
 * @see com.group2.wsc.catalog.domain.AbstractDomainClass#hashCode()
 */
@EqualsAndHashCode(callSuper = false)
@Entity
public class Role extends AbstractDomainClass {

	/** The role. */
	private String role;

	/** The prioritylevel. */
	private int prioritylevel;

}
