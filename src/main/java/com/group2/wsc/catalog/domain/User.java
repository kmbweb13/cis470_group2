/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * Instantiates a new user.
 */
@Data
/*
 * (non-Javadoc)
 * @see com.group2.wsc.catalog.domain.AbstractDomainClass#hashCode()
 */
@EqualsAndHashCode(callSuper = false, exclude = { "mainRole", "newRoleName", "password", "confirmPassword" })
@Entity
public class User extends AbstractDomainClass {

	/** The password. */
	@Transient
	private String password;

	/** The confirm password. */
	@Transient
	private String confirmPassword;

	/** The username. */
	private String username;

	/** The encrypted password. */
	private String encryptedPassword;

	/** The enabled. */
	private Boolean enabled = true;

	/** The main role name. */
	private String mainRoleName;

	/** The new role name. */
	@Transient
	private String newRoleName = "";

	/** The main role. */
	@Transient
	private Role mainRole;

	/** The user information. */
	@OneToOne(fetch = FetchType.EAGER)
	private UserInformation userInformation;

	/** The roles. */
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable
	private List<Role> roles = new ArrayList<>();

	/** The failed login attempts. */
	private Integer failedLoginAttempts = 0;

	/**
	 * Adds the role.
	 *
	 * @param role the role
	 */
	public void addRole(Role role) {
		if (!this.roles.contains(role)) {
			this.roles.add(role);
		}
	}

	/**
	 * Removes the role.
	 *
	 * @param role the role
	 */
	public void removeRole(Role role) {
		this.roles.remove(role);
	}
}
