/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new user information.
 */
@Data

/*
 * (non-Javadoc)
 * @see com.group2.wsc.catalog.domain.AbstractDomainClass#hashCode()
 */
@EqualsAndHashCode(callSuper = false, exclude = { "tempBirthDt" })
@Entity
public class UserInformation extends AbstractDomainClass {

	/** The firstname. */
	private String firstname;

	/** The middlename. */
	private String middlename;

	/** The lastname. */
	private String lastname;

	/** The preferredname. */
	private String preferredname;

	/** The email address. */
	private String emailAddress;

	/** The birth dt. */
	private Date birthDt;

	/** The temp birth dt. */
	@Transient
	private String tempBirthDt;

	/** The addresses. */
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable
	private List<Address> addresses = new ArrayList<>();

	/** The phonenumbers. */
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable
	private List<PhoneNumber> phonenumbers = new ArrayList<>();

	/** The employee detail. */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employeeDetailId")
	private EmployeeDetail employeeDetail;

	/** The customer detail. */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "customerDetailId")
	private CustomerDetail customerDetail;

	/**
	 * Adds the address.
	 *
	 * @param address the address
	 */
	public void addAddress(Address address) {
		if (!this.addresses.contains(address)) {
			this.addresses.add(address);
		}
	}

	/**
	 * Removes the address.
	 *
	 * @param address the address
	 */
	public void removeAddress(Address address) {
		this.addresses.remove(address);
	}

	/**
	 * Adds the phone number.
	 *
	 * @param phonenumber the phonenumber
	 */
	public void addPhoneNumber(PhoneNumber phonenumber) {
		if (!this.phonenumbers.contains(phonenumber)) {
			this.phonenumbers.add(phonenumber);
		}
	}

	/**
	 * Removes the phone number.
	 *
	 * @param phonenumber the phonenumber
	 */
	public void removePhoneNumber(PhoneNumber phonenumber) {
		this.phonenumbers.remove(phonenumber);
	}

	/**
	 * Gets the address by type.
	 *
	 * @param type the type
	 * @return the address by type
	 */
	public Address getAddressByType(String type) {
		for (int i = 0; i < addresses.size(); i++) {
			Address address = addresses.get(i);
			if (type.equals(address.getType())) {
				return address;
			}
		}
		return null;
	}

	/**
	 * Gets the phone number by type.
	 *
	 * @param type the type
	 * @return the phone number by type
	 */
	public PhoneNumber getPhoneNumberByType(String type) {
		for (int i = 0; i < phonenumbers.size(); i++) {
			PhoneNumber phonenumber = phonenumbers.get(i);
			if (type.equals(phonenumber.getType())) {
				return phonenumber;
			}
		}
		return null;
	}

	/**
	 * Gets the preferredname.
	 *
	 * @return the preferredname
	 */
	public String getPreferredname() {
		if (preferredname != null) {
			return preferredname;
		}
		return firstname;
	}

	/**
	 * Gets the full name.
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return getPreferredname() + " " + lastname;
	}

}
