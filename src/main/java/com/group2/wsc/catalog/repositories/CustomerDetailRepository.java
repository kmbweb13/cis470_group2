/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.group2.wsc.catalog.domain.CustomerDetail;

/**
 * The Interface CustomerDetailRepository.
 */
public interface CustomerDetailRepository extends CrudRepository<CustomerDetail, Integer> {

	/**
	 * Find by username contains ignore case.
	 *
	 * @param username the username
	 * @return the list
	 */
	List<CustomerDetail> findByUsernameContainsIgnoreCase(String username);
}
