/*
 * Devry CIS 470 Group 2
 * Williams Specility Company - Ecommerce project 
 *
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.repositories;

import org.springframework.data.repository.CrudRepository;

import com.group2.wsc.catalog.domain.Product;

/**
 * The Interface ProductRepository.
 */
public interface ProductRepository extends CrudRepository<Product, Integer>{
}
