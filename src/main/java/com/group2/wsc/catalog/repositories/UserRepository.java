/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.group2.wsc.catalog.domain.User;

/**
 * The Interface UserRepository.
 */
public interface UserRepository extends CrudRepository<User, Integer> {

	/**
	 * Find by username.
	 *
	 * @param username the username
	 * @return the user
	 */
	User findByUsername(String username);

	/**
	 * Find by username contains ignore case.
	 *
	 * @param username the username
	 * @return the list
	 */
	List<User> findByUsernameContainsIgnoreCase(String username);

}
