/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.Address;
import com.group2.wsc.catalog.repositories.AddressRepository;

/**
 * The Class AddressServiceImpl.
 */
@Service
@Profile("springdatajpa")
public class AddressServiceImpl implements AddressService {

	/** The address repository. */
	private AddressRepository addressRepository;

	/**
	 * Sets the address repository.
	 *
	 * @param addressRepository the new address repository
	 */
	@Autowired
	public void setAddressRepository(AddressRepository addressRepository) {
		this.addressRepository = addressRepository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#listAll()
	 */
	@Override
	public List<?> listAll() {
		List<Address> addresss = new ArrayList<>();
		addressRepository.findAll().forEach(addresss::add);
		return addresss;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#getById(java.lang.Integer)
	 */
	@Override
	public Address getById(Integer id) {
		return addressRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public Address saveOrUpdate(Address domainObject) {
		return addressRepository.save(domainObject);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#delete(java.lang.Integer)
	 */
	@Override
	public void delete(Integer id) {
		addressRepository.delete(id);
	}
}
