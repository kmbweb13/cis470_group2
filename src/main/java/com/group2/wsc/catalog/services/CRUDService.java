/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.List;

/**
 * The Interface CRUDService.
 *
 * @param <T> the generic type
 */
public interface CRUDService<T> {

	/**
	 * List all.
	 *
	 * @return the list
	 */
	List<?> listAll();

	/**
	 * Gets the by id.
	 *
	 * @param id the id
	 * @return the by id
	 */
	T getById(Integer id);

	/**
	 * Save or update.
	 *
	 * @param domainObject the domain object
	 * @return the t
	 */
	T saveOrUpdate(T domainObject);

	/**
	 * Delete.
	 *
	 * @param id the id
	 */
	void delete(Integer id);
}
