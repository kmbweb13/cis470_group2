/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.Category;
import com.group2.wsc.catalog.repositories.CategoryRepository;

/**
 * The Class CategoryServiceImpl.
 */
@Service
@Profile("springdatajpa")
public class CategoryServiceImpl implements CategoryService {

	/** The category repository. */
	private CategoryRepository categoryRepository;

	/**
	 * Sets the category repository.
	 *
	 * @param categoryRepository the new category repository
	 */
	@Autowired
	public void setCategoryRepository(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#listAll()
	 */
	@Override
	public List<?> listAll() {
		List<Category> categorys = new ArrayList<>();
		categoryRepository.findAll().forEach(categorys::add);
		return categorys;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#getById(java.lang.Integer)
	 */
	@Override
	public Category getById(Integer id) {
		return categoryRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public Category saveOrUpdate(Category domainObject) {
		return categoryRepository.save(domainObject);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#delete(java.lang.Integer)
	 */
	@Override
	public void delete(Integer id) {
		categoryRepository.delete(id);
	}
}
