/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.List;

import com.group2.wsc.catalog.domain.CustomerDetail;

/**
 * The Interface CustomerDetailService.
 */
public interface CustomerDetailService extends CRUDService<CustomerDetail> {

	/**
	 * Find by username contains ignore case.
	 *
	 * @param username the username
	 * @return the list
	 */
	List<CustomerDetail> findByUsernameContainsIgnoreCase(String username);

}
