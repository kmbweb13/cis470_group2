/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.CustomerDetail;
import com.group2.wsc.catalog.repositories.CustomerDetailRepository;

/**
 * The Class CustomerDetailServiceImpl.
 */
@Service
@Profile("springdatajpa")
public class CustomerDetailServiceImpl implements CustomerDetailService {

	/** The customer detail repository. */
	private CustomerDetailRepository customerDetailRepository;

	/**
	 * Sets the customer detail repository.
	 *
	 * @param customerDetailRepository the new customer detail repository
	 */
	@Autowired
	public void setCustomerDetailRepository(CustomerDetailRepository customerDetailRepository) {
		this.customerDetailRepository = customerDetailRepository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#listAll()
	 */
	@Override
	public List<?> listAll() {
		List<CustomerDetail> customerDetails = new ArrayList<>();
		customerDetailRepository.findAll().forEach(customerDetails::add);
		return customerDetails;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#getById(java.lang.Integer)
	 */
	@Override
	public CustomerDetail getById(Integer id) {
		return customerDetailRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public CustomerDetail saveOrUpdate(CustomerDetail domainObject) {
		return customerDetailRepository.save(domainObject);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#delete(java.lang.Integer)
	 */
	@Override
	public void delete(Integer id) {
		customerDetailRepository.delete(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CustomerDetailService#findByUsernameContainsIgnoreCase(java.lang.String)
	 */
	@Override
	public List<CustomerDetail> findByUsernameContainsIgnoreCase(String username) {
		List<CustomerDetail> customerDetails = new ArrayList<>();
		customerDetailRepository.findByUsernameContainsIgnoreCase(username).forEach(customerDetails::add);
		return customerDetails;
	}
}
