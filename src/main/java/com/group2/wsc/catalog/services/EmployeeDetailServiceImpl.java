/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.EmployeeDetail;
import com.group2.wsc.catalog.repositories.EmployeeDetailRepository;

/**
 * The Class EmployeeDetailServiceImpl.
 */
@Service
@Profile("springdatajpa")
public class EmployeeDetailServiceImpl implements EmployeeDetailService {

	/** The employee detail repository. */
	private EmployeeDetailRepository employeeDetailRepository;

	/**
	 * Sets the employee detail repository.
	 *
	 * @param employeeDetailRepository the new employee detail repository
	 */
	@Autowired
	public void setEmployeeDetailRepository(EmployeeDetailRepository employeeDetailRepository) {
		this.employeeDetailRepository = employeeDetailRepository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#listAll()
	 */
	@Override
	public List<?> listAll() {
		List<EmployeeDetail> employeeDetails = new ArrayList<>();
		employeeDetailRepository.findAll().forEach(employeeDetails::add);
		return employeeDetails;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#getById(java.lang.Integer)
	 */
	@Override
	public EmployeeDetail getById(Integer id) {
		return employeeDetailRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public EmployeeDetail saveOrUpdate(EmployeeDetail domainObject) {
		return employeeDetailRepository.save(domainObject);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#delete(java.lang.Integer)
	 */
	@Override
	public void delete(Integer id) {
		employeeDetailRepository.delete(id);
	}
}
