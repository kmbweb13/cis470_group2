/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.OrderDetail;
import com.group2.wsc.catalog.repositories.OrderDetailRepository;

/**
 * The Class OrderDetailServiceImpl.
 */
@Service
@Profile("springdatajpa")
public class OrderDetailServiceImpl implements OrderDetailService {

	/** The order detail repository. */
	private OrderDetailRepository orderDetailRepository;

	/**
	 * Sets the order detail repository.
	 *
	 * @param orderDetailRepository the new order detail repository
	 */
	@Autowired
	public void setOrderDetailRepository(OrderDetailRepository orderDetailRepository) {
		this.orderDetailRepository = orderDetailRepository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#listAll()
	 */
	@Override
	public List<?> listAll() {
		List<OrderDetail> orderDetails = new ArrayList<>();
		orderDetailRepository.findAll().forEach(orderDetails::add);
		return orderDetails;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#getById(java.lang.Integer)
	 */
	@Override
	public OrderDetail getById(Integer id) {
		return orderDetailRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public OrderDetail saveOrUpdate(OrderDetail domainObject) {
		return orderDetailRepository.save(domainObject);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#delete(java.lang.Integer)
	 */
	@Override
	public void delete(Integer id) {
		orderDetailRepository.delete(id);
	}
}
