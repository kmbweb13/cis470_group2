/*
 * Devry CIS 470 Group 2
 * Williams Specility Company - Ecommerce project 
 *
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import com.group2.wsc.catalog.domain.PhoneNumber;

/**
 * The Interface PhoneNumberService.
 */
public interface PhoneNumberService extends CRUDService<PhoneNumber> {
}
