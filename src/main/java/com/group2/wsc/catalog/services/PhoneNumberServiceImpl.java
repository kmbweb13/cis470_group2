/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.PhoneNumber;
import com.group2.wsc.catalog.repositories.PhoneNumberRepository;

/**
 * The Class PhoneNumberServiceImpl.
 */
@Service
@Profile("springdatajpa")
public class PhoneNumberServiceImpl implements PhoneNumberService {

	/** The phone number repository. */
	private PhoneNumberRepository phoneNumberRepository;

	/**
	 * Sets the phone number repository.
	 *
	 * @param phoneNumberRepository the new phone number repository
	 */
	@Autowired
	public void setPhoneNumberRepository(PhoneNumberRepository phoneNumberRepository) {
		this.phoneNumberRepository = phoneNumberRepository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#listAll()
	 */
	@Override
	public List<?> listAll() {
		List<PhoneNumber> phoneNumbers = new ArrayList<>();
		phoneNumberRepository.findAll().forEach(phoneNumbers::add);
		return phoneNumbers;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#getById(java.lang.Integer)
	 */
	@Override
	public PhoneNumber getById(Integer id) {
		return phoneNumberRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public PhoneNumber saveOrUpdate(PhoneNumber domainObject) {
		return phoneNumberRepository.save(domainObject);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#delete(java.lang.Integer)
	 */
	@Override
	public void delete(Integer id) {
		phoneNumberRepository.delete(id);
	}
}
