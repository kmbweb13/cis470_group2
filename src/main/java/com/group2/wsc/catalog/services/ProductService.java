/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import com.group2.wsc.catalog.domain.Product;

/**
 * The Interface ProductService.
 */
public interface ProductService {

	/**
	 * List all products.
	 *
	 * @return the iterable
	 */
	Iterable<Product> listAllProducts();

	/**
	 * Gets the product by id.
	 *
	 * @param id the id
	 * @return the product by id
	 */
	Product getProductById(Integer id);

	/**
	 * Save product.
	 *
	 * @param product the product
	 * @return the product
	 */
	Product saveProduct(Product product);

	/**
	 * Delete product.
	 *
	 * @param id the id
	 */
	void deleteProduct(Integer id);
}
