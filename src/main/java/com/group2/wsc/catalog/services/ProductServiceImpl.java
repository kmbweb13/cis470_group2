/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.Product;
import com.group2.wsc.catalog.repositories.ProductRepository;

/**
 * The Class ProductServiceImpl.
 */
@Service
public class ProductServiceImpl implements ProductService {

	/** The product repository. */
	private ProductRepository productRepository;

	/**
	 * Sets the product repository.
	 *
	 * @param productRepository the new product repository
	 */
	@Autowired
	public void setProductRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.ProductService#listAllProducts()
	 */
	@Override
	public Iterable<Product> listAllProducts() {
		return productRepository.findAll();
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.ProductService#getProductById(java.lang.Integer)
	 */
	@Override
	public Product getProductById(Integer id) {
		return productRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.ProductService#saveProduct(com.group2.wsc.catalog.domain.Product)
	 */
	@Override
	public Product saveProduct(Product product) {
		return productRepository.save(product);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.ProductService#deleteProduct(java.lang.Integer)
	 */
	@Override
	public void deleteProduct(Integer id) {
		productRepository.delete(id);
	}
}
