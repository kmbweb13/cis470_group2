/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.ProductStyle;
import com.group2.wsc.catalog.repositories.ProductStyleRepository;

/**
 * The Class ProductStyleServiceImpl.
 */
@Service
@Profile("springdatajpa")
public class ProductStyleServiceImpl implements ProductStyleService {

	/** The product style repository. */
	private ProductStyleRepository productStyleRepository;

	/**
	 * Sets the product style repository.
	 *
	 * @param productStyleRepository the new product style repository
	 */
	@Autowired
	public void setProductStyleRepository(ProductStyleRepository productStyleRepository) {
		this.productStyleRepository = productStyleRepository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#listAll()
	 */
	@Override
	public List<?> listAll() {
		List<ProductStyle> productStyles = new ArrayList<>();
		productStyleRepository.findAll().forEach(productStyles::add);
		return productStyles;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#getById(java.lang.Integer)
	 */
	@Override
	public ProductStyle getById(Integer id) {
		return productStyleRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public ProductStyle saveOrUpdate(ProductStyle domainObject) {
		return productStyleRepository.save(domainObject);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#delete(java.lang.Integer)
	 */
	@Override
	public void delete(Integer id) {
		productStyleRepository.delete(id);
	}
}
