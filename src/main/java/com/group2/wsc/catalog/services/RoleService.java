/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.List;

import com.group2.wsc.catalog.domain.Role;

/**
 * The Interface RoleService.
 */
public interface RoleService extends CRUDService<Role> {

	/**
	 * Gets the by role name.
	 *
	 * @param name the name
	 * @return the by role name
	 */
	Role getByRoleName(String name);

	/**
	 * Gets the additional roles.
	 *
	 * @param role the role
	 * @return the additional roles
	 */
	List<Role> getAdditionalRoles(Role role);
}
