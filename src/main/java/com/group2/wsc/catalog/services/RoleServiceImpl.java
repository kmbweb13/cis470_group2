/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.Role;
import com.group2.wsc.catalog.repositories.RoleRepository;

/**
 * The Class RoleServiceImpl.
 */
@Service
@Profile("springdatajpa")
public class RoleServiceImpl implements RoleService {

	/** The role repository. */
	private RoleRepository roleRepository;

	/**
	 * Sets the role repository.
	 *
	 * @param roleRepository the new role repository
	 */
	@Autowired
	public void setRoleRepository(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#listAll()
	 */
	@Override
	public List<?> listAll() {
		List<Role> roles = new ArrayList<>();
		roleRepository.findAll().forEach(roles::add);
		return roles;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#getById(java.lang.Integer)
	 */
	@Override
	public Role getById(Integer id) {
		return roleRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.RoleService#getByRoleName(java.lang.String)
	 */
	public Role getByRoleName(String name) {
		return ((List<Role>) roleRepository.findAll()).stream().filter(role -> role.getRole().equals(name)).findFirst()
				.get();
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.RoleService#getAdditionalRoles(com.group2.wsc.catalog.domain.Role)
	 */
	@Override
	public List<Role> getAdditionalRoles(Role mainRole) {
		return ((List<Role>) roleRepository.findAll()).stream()
				.filter(role -> mainRole.getPrioritylevel() >= role.getPrioritylevel()).collect(Collectors.toList());
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public Role saveOrUpdate(Role domainObject) {
		return roleRepository.save(domainObject);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#delete(java.lang.Integer)
	 */
	@Override
	public void delete(Integer id) {
		roleRepository.delete(id);
	}

}
