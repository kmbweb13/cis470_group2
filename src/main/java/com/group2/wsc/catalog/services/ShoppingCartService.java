/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.math.BigDecimal;
import java.util.List;

import com.group2.wsc.catalog.domain.Payment;
import com.group2.wsc.catalog.domain.ProductOrderDetail;
import com.group2.wsc.catalog.domain.User;

/**
 * The Interface ShoppingCartService.
 */
public interface ShoppingCartService {

	/**
	 * Adds the product order detail.
	 *
	 * @param productOrderDetail the product order detail
	 */
	void addProductOrderDetail(ProductOrderDetail productOrderDetail);

	/**
	 * Removes the product order detail.
	 *
	 * @param productOrderDetailId the product order detail id
	 */
	void removeProductOrderDetail(Integer productOrderDetailId);

	/**
	 * Gets the product order details in cart.
	 *
	 * @return the product order details in cart
	 */
	List<ProductOrderDetail> getProductOrderDetailsInCart();

	/**
	 * Load previous open order.
	 *
	 * @param orderId the order id
	 * @return the int
	 */
	int loadPreviousOpenOrder(int orderId);

	/**
	 * Gets the product order detail from cart.
	 *
	 * @param productOrderDetailId the product order detail id
	 * @return the product order detail from cart
	 */
	ProductOrderDetail getProductOrderDetailFromCart(Integer productOrderDetailId);

	/**
	 * Checkout.
	 *
	 * @param payment the payment
	 * @param user the user
	 */
	void checkout(Payment payment, User user);

	/**
	 * Gets the total.
	 *
	 * @return the total
	 */
	BigDecimal getTotal();

	/**
	 * Gets the sub total.
	 *
	 * @return the sub total
	 */
	BigDecimal getSubTotal();

	/**
	 * Gets the tax.
	 *
	 * @return the tax
	 */
	BigDecimal getTax();

	/**
	 * Gets the min payment.
	 *
	 * @return the min payment
	 */
	BigDecimal getMinPayment();

	/**
	 * Clear cart.
	 */
	void clearCart();

	/**
	 * Gets the existing orderid.
	 *
	 * @return the existing orderid
	 */
	Integer getExistingOrderid();

	/**
	 * Gets the total payments.
	 *
	 * @return the total payments
	 */
	BigDecimal getExistingPaymentsTotal();
}
