/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.github.javafaker.Faker;
import com.group2.wsc.catalog.converters.Utility;
import com.group2.wsc.catalog.domain.CustomerDetail;
import com.group2.wsc.catalog.domain.OrderDetail;
import com.group2.wsc.catalog.domain.Payment;
import com.group2.wsc.catalog.domain.PhoneNumber;
import com.group2.wsc.catalog.domain.ProductOrderDetail;
import com.group2.wsc.catalog.domain.Role;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;

import lombok.Data;

/**
 * Instantiates a new shopping cart service impl.
 */
@Data
@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional
public class ShoppingCartServiceImpl implements ShoppingCartService {

	/** The user information service. */
	@Autowired
	private UserInformationService userInformationService;

	/** The customer detail service. */
	@Autowired
	private CustomerDetailService customerDetailService;

	/** The order detail service. */
	@Autowired
	private OrderDetailService orderDetailService;

	/** The user service. */
	@Autowired
	private UserService userService;

	/** The role service. */
	@Autowired
	private RoleService roleService;

	/** The utility. */
	@Autowired
	private Utility utility;

	/** The product order details. */
	private List<ProductOrderDetail> productOrderDetails = new ArrayList<>();

	/** The existing payments. */
	private BigDecimal existingPayments = null;

	/** The existing order id. */
	private Integer existingOrderId;

	// private Payment payment;

	/**
	 * If product is in the map just increment quantity by 1. If product is not in the map with, add it with quantity 1
	 *
	 * @param productOrderDetail the product order detail
	 */
	@Override
	public void addProductOrderDetail(ProductOrderDetail productOrderDetail) {
		Integer id = productOrderDetail.getId();

		if (id == null) {
			int size = productOrderDetails.size();
			productOrderDetail.setId(size);
			productOrderDetails.add(productOrderDetail);
		} else {
			productOrderDetails.set(id, productOrderDetail);
		}
	}

	/**
	 * If product is in the map with quantity > 1, just decrement quantity by 1. If product is in the map with quantity 1, remove it from map
	 *
	 * @param productOrderDetailId the product order detail id
	 */
	@Override
	public void removeProductOrderDetail(Integer productOrderDetailId) {
		ProductOrderDetail productOrderDetail = productOrderDetails.get(productOrderDetailId);
		productOrderDetail.setRemoved(true);
	}

	/**
	 * Gets the product order details in cart.
	 *
	 * @return unmodifiable copy of the map
	 */
	@Override
	public List<ProductOrderDetail> getProductOrderDetailsInCart() {
		return Collections.unmodifiableList(
				productOrderDetails.stream().filter(x -> !x.isRemoved()).collect(Collectors.toList()));
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.ShoppingCartService# getProductOrderDetailFromCart(java.lang.Integer)
	 */
	@Override
	public ProductOrderDetail getProductOrderDetailFromCart(Integer productOrderDetailId) {
		return productOrderDetails.get(productOrderDetailId);
	}

	/**
	 * Checkout will rollback if there is not enough of some product in stock.
	 *
	 * @param payment the payment
	 * @param user the user
	 */
	@Override
	public void checkout(Payment payment, User user) {
		String username = user.getUsername();
		UserInformation userInfomation = user.getUserInformation();
		CustomerDetail customerDetail = userInfomation.getCustomerDetail();

		if (payment.getOrderId() == null) {
			if (createNewOrder(username, customerDetail, user, userInfomation, payment)) {
				updateUserStatus(user);
			}
		} else {
			updateExistingOrder(payment);
		}
		clearCart();
	}

	/**
	 * Creates the new order.
	 *
	 * @param username the username
	 * @param customerDetail the customer detail
	 * @param user the user
	 * @param userInfomation the user infomation
	 * @param payment the payment
	 * @return true, if successful
	 */
	private boolean createNewOrder(String username, CustomerDetail customerDetail, User user,
			UserInformation userInfomation, Payment payment) {
		OrderDetail orderDetail = new OrderDetail();
		orderDetail.setUsername(username);
		boolean updateUser = false;
		if (customerDetail == null) {
			customerDetail = utility.generateCustomerDetails(username, new Faker().numerify("########"));
			if (user.getMainRoleName().equals("GUEST")) {
				updateUser = true;
			}
		}
		orderDetail.setCustomerDetail(customerDetail);
		orderDetail.setOrderDt(new Date());
		orderDetail.setTaxRate(.075);

		orderDetail.setBillingAddress(userInfomation.getAddressByType("billing"));
		orderDetail.setShippingAddress(userInfomation.getAddressByType("shipping"));
		PhoneNumber phone = userInfomation.getPhoneNumberByType("cell");
		orderDetail.setPhoneNumber(phone == null ? "" : phone.toString());

		orderDetail.setProductOrderDetails(getActiveOrders());
		orderDetail.setOrderSubtotal(getSubTotal());
		orderDetail.setTax(getTax());
		orderDetail.setOrderTotal(getTotal());
		orderDetail.addPayment(payment);
		orderDetailService.saveOrUpdate(orderDetail);
		customerDetail.addOrderDetail(orderDetail);
		customerDetailService.saveOrUpdate(customerDetail);
		userInfomation.setCustomerDetail(customerDetail);
		userInformationService.saveOrUpdate(userInfomation);
		return updateUser;
	}

	/**
	 * Update user status.
	 *
	 * @param user the user
	 */
	private void updateUserStatus(User user) {
		String mainRole = "CUSTOMER";
		user.setMainRoleName(mainRole);
		Role role = roleService.getByRoleName(mainRole);
		user.setMainRole(role);
		user.setRoles(roleService.getAdditionalRoles(role));
		userService.saveOrUpdate(user);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		List<GrantedAuthority> updatedAuthorities = new ArrayList<>(auth.getAuthorities());
		updatedAuthorities.add(new SimpleGrantedAuthority(mainRole));
		Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(),
				updatedAuthorities);

		SecurityContextHolder.getContext().setAuthentication(newAuth);
	}

	/**
	 * Update existing order.
	 *
	 * @param payment the payment
	 */
	private void updateExistingOrder(Payment payment) {
		OrderDetail orderDetail = orderDetailService.getById(payment.getOrderId());
		orderDetail.setOrderDt(new Date());
		orderDetail.setProductOrderDetails(getActiveOrders());
		orderDetail.setOrderSubtotal(getSubTotal());
		orderDetail.setTax(getTax());
		orderDetail.setOrderTotal(getTotal());
		if (payment.getAmount() != null) {
			orderDetail.addPayment(payment);
		}
		orderDetail.setOrderFinalized(false);
		orderDetail.setOrderFinalizedDt(null);
		orderDetailService.saveOrUpdate(orderDetail);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.ShoppingCartService#getSubTotal()
	 */
	@Override
	public BigDecimal getSubTotal() {
		BigDecimal subtotal = getActiveOrders().stream().map(ProductOrderDetail::subTotal).reduce(BigDecimal::add)
				.orElse(BigDecimal.ZERO);
		return Utility.createCurrency(subtotal);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.ShoppingCartService#getTax()
	 */
	@Override
	public BigDecimal getTax() {
		return multiplyBy(0.075, false);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.ShoppingCartService#getTotal()
	 */
	@Override
	public BigDecimal getTotal() {
		return multiplyBy(1.075, false);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.ShoppingCartService#getMinPayment()
	 */
	@Override
	public BigDecimal getMinPayment() {
		if (existingPayments == null) {
			return multiplyBy(0.1, true);
		}
		BigDecimal min = multiplyBy(0.1, true).subtract(existingPayments);
		return min.doubleValue() > 0.0 ? Utility.createCurrency(min) : Utility.createCurrency(0);
	}

	/**
	 * Multiply by.
	 *
	 * @param value the value
	 * @param total the total
	 * @return the big decimal
	 */
	private BigDecimal multiplyBy(double value, boolean total) {
		if (total) {
			return Utility.createCurrency(getTotal().multiply(new BigDecimal(value)));
		}
		return Utility.createCurrency(getSubTotal().multiply(new BigDecimal(value)));
	}

	/**
	 * Gets the active orders.
	 *
	 * @return the active orders
	 */
	private List<ProductOrderDetail> getActiveOrders() {
		return productOrderDetails.stream().filter(x -> !x.isRemoved()).collect(Collectors.toList());
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.ShoppingCartService#loadPreviousOpenOrder( int)
	 */
	@Override
	public int loadPreviousOpenOrder(int orderId) {

		OrderDetail orderDetail = orderDetailService.getById(orderId);
		List<ProductOrderDetail> pods = orderDetail.getProductOrderDetails();
		int index = 0;
		for (ProductOrderDetail productOrderDetail : pods) {
			productOrderDetail.setProductId(productOrderDetail.getProduct().getId());
			if (productOrderDetail.getId() == null) {
				productOrderDetail.setId(index++);
			}
		}
		productOrderDetails = pods;
		getTotal().subtract(orderDetail.outstandingBalance());
		existingPayments = Utility.createCurrency(getTotal().subtract(orderDetail.outstandingBalance()));
		this.existingOrderId = orderId;
		return orderDetail.getId();
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.ShoppingCartService#clearCart()
	 */
	@Override
	public void clearCart() {
		productOrderDetails.clear();
		this.existingOrderId = null;
		this.existingPayments = null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.ShoppingCartService#getExistingOrderid()
	 */
	@Override
	public Integer getExistingOrderid() {
		return existingOrderId;
	}

	@Override
	public BigDecimal getExistingPaymentsTotal() {
		
		return this.existingPayments;
	}

}
