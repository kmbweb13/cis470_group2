/*
 * Devry CIS 470 Group 2
 * Williams Specility Company - Ecommerce project 
 *
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import com.group2.wsc.catalog.domain.UserInformation;

/**
 * The Interface UserInformationService.
 */
public interface UserInformationService extends CRUDService<UserInformation> {
}
