/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.repositories.UserInformationRepository;

/**
 * The Class UserInformationServiceImpl.
 */
@Service
@Profile("springdatajpa")
public class UserInformationServiceImpl implements UserInformationService {

	/** The user information repository. */
	private UserInformationRepository userInformationRepository;

	/**
	 * Sets the user information repository.
	 *
	 * @param userInformationRepository the new user information repository
	 */
	@Autowired
	public void setUserInformationRepository(UserInformationRepository userInformationRepository) {
		this.userInformationRepository = userInformationRepository;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#listAll()
	 */
	@Override
	public List<?> listAll() {
		List<UserInformation> userInformations = new ArrayList<>();
		userInformationRepository.findAll().forEach(userInformations::add);
		return userInformations;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#getById(java.lang.Integer)
	 */
	@Override
	public UserInformation getById(Integer id) {
		return userInformationRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public UserInformation saveOrUpdate(UserInformation domainObject) {
		return userInformationRepository.save(domainObject);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#delete(java.lang.Integer)
	 */
	@Override
	public void delete(Integer id) {
		userInformationRepository.delete(id);
	}
}
