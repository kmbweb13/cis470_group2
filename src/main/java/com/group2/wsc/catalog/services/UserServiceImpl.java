/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.repositories.UserRepository;
import com.group2.wsc.catalog.services.security.EncryptionService;

/**
 * The Class UserServiceImpl.
 */
@Service
@Profile("springdatajpa")
public class UserServiceImpl implements UserService {

	/** The user repository. */
	private UserRepository userRepository;

	/**
	 * Sets the user repository.
	 *
	 * @param userRepository the new user repository
	 */
	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	/** The encryption service. */
	private EncryptionService encryptionService;

	/**
	 * Sets the encryption service.
	 *
	 * @param encryptionService the new encryption service
	 */
	@Autowired
	public void setEncryptionService(EncryptionService encryptionService) {
		this.encryptionService = encryptionService;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#listAll()
	 */
	@Override
	public List<?> listAll() {
		List<User> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add); // fun with Java 8
		return users;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#getById(java.lang.Integer)
	 */
	@Override
	public User getById(Integer id) {
		return userRepository.findOne(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public User saveOrUpdate(User domainObject) {
		if (domainObject.getPassword() != null) {
			domainObject.setEncryptedPassword(encryptionService.encryptString(domainObject.getPassword()));
		}
		return userRepository.save(domainObject);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#delete(java.lang.Integer)
	 */
	@Override
	@Transactional
	public void delete(Integer id) {
		userRepository.delete(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.UserService#findByUsername(java.lang.String)
	 */
	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.UserService#findByUsernameContainsIgnoreCase(java.lang.String)
	 */
	@Override
	public List<User> findByUsernameContainsIgnoreCase(String username) {
		List<User> users = new ArrayList<>();
		userRepository.findByUsernameContainsIgnoreCase(username).forEach(users::add);
		return users;
	}
}
