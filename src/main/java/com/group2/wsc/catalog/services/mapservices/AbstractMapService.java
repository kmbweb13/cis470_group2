/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services.mapservices;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.group2.wsc.catalog.domain.DomainObject;

/**
 * The Class AbstractMapService.
 */
public abstract class AbstractMapService {

	/** The domain map. */
	protected Map<Integer, DomainObject> domainMap;

	/**
	 * Instantiates a new abstract map service.
	 */
	public AbstractMapService() {
		domainMap = new HashMap<>();
	}

	/**
	 * List all.
	 *
	 * @return the list
	 */
	public List<DomainObject> listAll() {
		return new ArrayList<>(domainMap.values());
	}

	/**
	 * Gets the by id.
	 *
	 * @param id the id
	 * @return the by id
	 */
	public DomainObject getById(Integer id) {
		return domainMap.get(id);
	}

	/**
	 * Save or update.
	 *
	 * @param domainObject the domain object
	 * @return the domain object
	 */
	public DomainObject saveOrUpdate(DomainObject domainObject) {
		if (domainObject != null) {

			if (domainObject.getId() == null) {
				domainObject.setId(getNextKey());
			}
			domainMap.put(domainObject.getId(), domainObject);

			return domainObject;
		} else {
			throw new RuntimeException("Object Can't be null");
		}
	}

	/**
	 * Delete.
	 *
	 * @param id the id
	 */
	public void delete(Integer id) {
		domainMap.remove(id);
	}

	/**
	 * Gets the next key.
	 *
	 * @return the next key
	 */
	private Integer getNextKey() {
		return Collections.max(domainMap.keySet()) + 1;
	}

}
