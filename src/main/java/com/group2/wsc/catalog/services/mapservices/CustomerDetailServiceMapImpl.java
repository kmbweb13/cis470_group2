/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services.mapservices;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.CustomerDetail;
import com.group2.wsc.catalog.domain.DomainObject;
import com.group2.wsc.catalog.services.CustomerDetailService;

/**
 * The Class CustomerDetailServiceMapImpl.
 */
@Service
@Profile("map")
public class CustomerDetailServiceMapImpl extends AbstractMapService implements CustomerDetailService {

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.mapservices.AbstractMapService#getById(java.lang.Integer)
	 */
	@Override
	public CustomerDetail getById(Integer id) {
		return (CustomerDetail) super.getById(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public CustomerDetail saveOrUpdate(CustomerDetail domainObject) {
		return (CustomerDetail) super.saveOrUpdate(domainObject);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CustomerDetailService#findByUsernameContainsIgnoreCase(java.lang.String)
	 */
	@Override
	public List<CustomerDetail> findByUsernameContainsIgnoreCase(String username) {

		Stream<DomainObject> results = domainMap.values().stream().filter(new Predicate<DomainObject>() {
			public boolean test(DomainObject domainObject) {
				CustomerDetail cd = (CustomerDetail) domainObject;
				return cd.getUsername().toLowerCase().contains(username.toLowerCase());
			}
		});
		return results.map(element -> (CustomerDetail) element).collect(Collectors.toList());
	}
}
