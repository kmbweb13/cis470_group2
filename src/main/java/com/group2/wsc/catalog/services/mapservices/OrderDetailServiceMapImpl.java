/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services.mapservices;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * The Class OrderDetailServiceMapImpl.
 */
@Service
@Profile("map")
public class OrderDetailServiceMapImpl extends AbstractMapService {
}
