/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services.mapservices;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.DomainObject;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.services.UserService;
import com.group2.wsc.catalog.services.security.EncryptionService;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new user service map impl.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Service
@Profile("map")
public class UserServiceMapImpl extends AbstractMapService implements UserService {

	/** The encryption service. */
	@Autowired
	private EncryptionService encryptionService;

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.mapservices.AbstractMapService#listAll()
	 */
	@Override
	public List<DomainObject> listAll() {
		return super.listAll();
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.mapservices.AbstractMapService#getById(java.lang.Integer)
	 */
	@Override
	public User getById(Integer id) {
		return (User) super.getById(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.CRUDService#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public User saveOrUpdate(User domainObject) {
		if (domainObject != null) {
			if (domainObject.getPassword() != null) {
				domainObject.setEncryptedPassword(encryptionService.encryptString(domainObject.getPassword()));
			}
		}
		return (User) super.saveOrUpdate(domainObject);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.mapservices.AbstractMapService#delete(java.lang.Integer)
	 */
	@Override
	public void delete(Integer id) {
		super.delete(id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.UserService#findByUsername(java.lang.String)
	 */
	@Override
	public User findByUsername(String userName) {

		Optional<?> returnUser = domainMap.values().stream().filter(new Predicate<DomainObject>() {
			@Override
			public boolean test(DomainObject domainObject) {
				User user = (User) domainObject;
				return user.getUsername().equalsIgnoreCase(userName);
			}
		}).findFirst();
		try {
			return (User) returnUser.get();
		} catch (NoSuchElementException nsee) {
			// no user do nothing just return null
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.UserService#findByUsernameContainsIgnoreCase(java.lang.String)
	 */
	@Override
	public List<User> findByUsernameContainsIgnoreCase(String username) {

		Stream<DomainObject> results = domainMap.values().stream().filter(new Predicate<DomainObject>() {
			public boolean test(DomainObject domainObject) {
				User user = (User) domainObject;
				return user.getUsername().toLowerCase().contains(username.toLowerCase());
			}
		});

		return results.map(element -> (User) element).collect(Collectors.toList());

	}
}
