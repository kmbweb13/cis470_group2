/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services.security;

/**
 * The Interface EncryptionService.
 */
public interface EncryptionService {

	/**
	 * Encrypt string.
	 *
	 * @param input the input
	 * @return the string
	 */
	String encryptString(String input);

	/**
	 * Check password.
	 *
	 * @param plainPassword the plain password
	 * @param encryptedPassword the encrypted password
	 * @return true, if successful
	 */
	boolean checkPassword(String plainPassword, String encryptedPassword);
}
