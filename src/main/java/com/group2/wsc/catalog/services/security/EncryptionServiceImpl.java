/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services.security;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The Class EncryptionServiceImpl.
 */
@Service
public class EncryptionServiceImpl implements EncryptionService {

	/** The strong encryptor. */
	private StrongPasswordEncryptor strongEncryptor;

	/**
	 * Sets the strong encryptor.
	 *
	 * @param strongEncryptor the new strong encryptor
	 */
	@Autowired
	public void setStrongEncryptor(StrongPasswordEncryptor strongEncryptor) {
		this.strongEncryptor = strongEncryptor;
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.security.EncryptionService#encryptString(java.lang.String)
	 */
	public String encryptString(String input) {
		return strongEncryptor.encryptPassword(input);
	}

	/*
	 * (non-Javadoc)
	 * @see com.group2.wsc.catalog.services.security.EncryptionService#checkPassword(java.lang.String, java.lang.String)
	 */
	public boolean checkPassword(String plainPassword, String encryptedPassword) {
		return strongEncryptor.checkPassword(plainPassword, encryptedPassword);
	}
}
