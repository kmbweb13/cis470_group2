/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services.security;

import java.util.Collection;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.group2.wsc.catalog.domain.UserInformation;

import lombok.Data;

/**
 * Instantiates a new user details impl.
 */
@Data
public class UserDetailsImpl implements UserDetails {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3065647560349301764L;

	/** The authorities. */
	private Collection<SimpleGrantedAuthority> authorities;

	/** The username. */
	private String username;

	/** The user information. */
	private UserInformation userInformation;

	/** The password. */
	private String password;

	/** The enabled. */
	private Boolean enabled = true;

	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#isAccountNonExpired()
	 */
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#isAccountNonLocked()
	 */
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#isCredentialsNonExpired()
	 */
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		return enabled;
	}

}
