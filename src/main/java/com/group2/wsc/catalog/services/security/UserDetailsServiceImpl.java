/*
 * Devry CIS 470 Group 2 Williams Specility Company - Ecommerce project
 * @author Kenneth Burke
 * @version 0.0.1
 */
package com.group2.wsc.catalog.services.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.services.UserService;

/**
 * The Class UserDetailsServiceImpl.
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	/** The user service. */
	private UserService userService;

	/** The user user details converter. */
	private Converter<User, UserDetails> userUserDetailsConverter;

	/**
	 * Sets the user service.
	 *
	 * @param userService the new user service
	 */
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * Sets the user user details converter.
	 *
	 * @param userUserDetailsConverter the user user details converter
	 */
	@Autowired
	@Qualifier(value = "userToUserDetails")
	public void setUserUserDetailsConverter(Converter<User, UserDetails> userUserDetailsConverter) {
		this.userUserDetailsConverter = userUserDetailsConverter;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userUserDetailsConverter.convert(userService.findByUsername(username));
	}
}
