package com.group2.wsc.catalog;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WilliamsSpecialtyCompanyApplication.class)
public class WilliamsSpecialtyCompanyApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void main() {
		WilliamsSpecialtyCompanyApplication.main(new String[] {});
	}
}

