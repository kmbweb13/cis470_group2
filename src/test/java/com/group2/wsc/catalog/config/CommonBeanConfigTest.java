package com.group2.wsc.catalog.config;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class CommonBeanConfigTest {

	@Test
	public void testStrongEncryptor() {
		CommonBeanConfig config = new CommonBeanConfig();
		assertNotNull(config.strongEncryptor());
	}

}
