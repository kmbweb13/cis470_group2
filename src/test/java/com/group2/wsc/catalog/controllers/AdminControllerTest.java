package com.group2.wsc.catalog.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.group2.wsc.catalog.converters.Utility;
import com.group2.wsc.catalog.domain.Address;
import com.group2.wsc.catalog.domain.CustomerDetail;
import com.group2.wsc.catalog.domain.EmployeeDetail;
import com.group2.wsc.catalog.domain.OrderDetail;
import com.group2.wsc.catalog.domain.Product;
import com.group2.wsc.catalog.domain.ProductStyle;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.services.CategoryService;
import com.group2.wsc.catalog.services.EmployeeDetailService;
import com.group2.wsc.catalog.services.ProductService;
import com.group2.wsc.catalog.services.ProductStyleService;
import com.group2.wsc.catalog.services.RoleService;
import com.group2.wsc.catalog.services.UserInformationService;
import com.group2.wsc.catalog.services.UserService;
import com.group2.wsc.catalog.services.security.UserDetailsImpl;

public class AdminControllerTest {
	AdminController adminController;

	UserService userService = mock(UserService.class);
	ProductStyleService productStyleService = mock(ProductStyleService.class);
	ProductService productService = mock(ProductService.class);
	CategoryService categoryService = mock(CategoryService.class);
	EmployeeDetailService employeeDetailService = mock(EmployeeDetailService.class);
	UserInformationService userInformationService = mock(UserInformationService.class);
	RoleService roleService = mock(RoleService.class);
	Utility utility = mock(Utility.class);
	Model model;

	Authentication authentication = mock(Authentication.class);
	SecurityContext securityContext = mock(SecurityContext.class);
	User user;
	UserInformation userInfo;
	@Before
	public void before() {
		List<User> users = new ArrayList<>();
		User userList = new User();
		userList.setUsername("abc");
		users.add(userList);
		adminController = new AdminController(productStyleService, productService, categoryService, employeeDetailService, userService, userInformationService, roleService, utility);
		adminController.setUserService(userService);
		model = new BindingAwareModelMap();
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		
		UserDetailsImpl ud = new UserDetailsImpl();
		ud.setUsername("abc");
		when(authentication.getPrincipal()).thenReturn(ud);
		user = new User();
		user.setId(0);
		userInfo = new UserInformation();
		userInfo.setFirstname("a");
		userInfo.setLastname("B");
		user.setUserInformation(userInfo);
		user.setUsername(ud.getUsername());
		when(userService.getById(anyInt())).thenReturn(user);
		when(userService.findByUsername(anyString())).thenReturn(user);
		when(userService.findByUsernameContainsIgnoreCase(anyString())).thenReturn(users);
		when(utility.generateEmployeeDetails(anyString(), anyString(), anyObject())).thenReturn(new EmployeeDetail());
		when(employeeDetailService.saveOrUpdate(anyObject())).thenReturn(new EmployeeDetail());
		when(productStyleService.getById(anyInt())).thenReturn(new ProductStyle());
		
		

	}

	@Test
	public void testIndex() {
		assertEquals("adminUser", adminController.index(model));
		
	}

	@Test
	public void testSearchCustomers() {
		assertEquals("fragments/userSearchResults :: userSearchResults", adminController.searchCustomers("abc", model));
	}

	@Test
	public void testLoadUserNull() {
		when(userService.findByUsername(anyString())).thenReturn(null);
		assertEquals("fragments/selectedUser :: selectedUser", adminController.loadUser("abc", model));
	}
	@Test
	public void testLoadUserGuest() {
		user.setMainRoleName("GUEST");
		when(userService.findByUsername(anyString())).thenReturn(user);
		assertEquals("fragments/selectedUser :: selectedUser", adminController.loadUser("abc", model));
	}
	@Test
	public void testLoadUserCustomer() {
		user.setMainRoleName("CUSTOMER");
		when(userService.findByUsername(anyString())).thenReturn(user);
		assertEquals("fragments/selectedUser :: selectedUser", adminController.loadUser("abc", model));
	}
	@Test
	public void testLoadUserEmployee() {
		userInfo.setEmployeeDetail(new EmployeeDetail());
		user.setMainRoleName("EMPLOYEE");
		Address address = new Address();
		address.setType("home");
		userInfo.setAddresses(Arrays.asList(address));
		when(userService.findByUsername(anyString())).thenReturn(user);
		assertEquals("fragments/selectedUser :: selectedUser", adminController.loadUser("abc", model));
	}

	@Test
	public void testUpdateEmployeeStatus() {
		user.setNewRoleName("b");
		user.setMainRoleName("a");
		assertEquals("redirect:/adminUserMaint", adminController.updateEmployeeStatus(user, model));
	}
	@Test
	public void testUpdateEmployeeStatusNoUpdate() {
		user.setNewRoleName("a");
		user.setMainRoleName("a");
		assertEquals("redirect:/adminUserMaint", adminController.updateEmployeeStatus(user, model));
	}
	@Test
	public void testUpdateEmployeeStatusExistingEmployee() {
		user.setNewRoleName("b");
		user.setMainRoleName("a");
		userInfo.setEmployeeDetail(new EmployeeDetail());
		assertEquals("redirect:/adminUserMaint", adminController.updateEmployeeStatus(user, model));
	}
	
	@Test
	public void testUpdateEmployeeStatusRemoveEmployee() {
		user.setNewRoleName("");
		user.setMainRoleName("a");
		userInfo.setEmployeeDetail(new EmployeeDetail());
		assertEquals("redirect:/adminUserMaint", adminController.updateEmployeeStatus(user, model));
	}
	
	@Test
	public void testUpdateEmployeeStatusResetCustomer() {
		user.setNewRoleName("");
		user.setMainRoleName("a");
		userInfo.setEmployeeDetail(new EmployeeDetail());
		CustomerDetail cd = new CustomerDetail();
		cd.setOrderDetails(Arrays.asList(new OrderDetail(), new OrderDetail()));
		userInfo.setCustomerDetail(cd);
		userInfo.setEmployeeDetail(new EmployeeDetail());
		
		assertEquals("redirect:/adminUserMaint", adminController.updateEmployeeStatus(user, model));
	}

	@Test
	public void testUpdateEmployeeStatusJustGuest() {
		user.setNewRoleName("");
		user.setMainRoleName("GUEST");
		assertEquals("redirect:/adminUserMaint", adminController.updateEmployeeStatus(user, model));
	}

	@Test
	public void testUpdateEmployeeStatusJustCustomer() {
		user.setNewRoleName("");
		user.setMainRoleName("CUSTOMER");
		assertEquals("redirect:/adminUserMaint", adminController.updateEmployeeStatus(user, model));
	}
	@Test
	public void testUpdateEmployeeStatusNewRoleCustomer() {
		user.setNewRoleName("");
		user.setMainRoleName("b");

		CustomerDetail cd = new CustomerDetail();
		cd.setOrderDetails(new ArrayList<>());
		userInfo.setCustomerDetail(cd);
		assertEquals("redirect:/adminUserMaint", adminController.updateEmployeeStatus(user, model));
	}

	@Test
	public void testAdminproductstyle() {
		assertEquals("adminproductstyle", adminController.adminproductstyle(model));
	}

	@Test
	public void testSaveProductStyle() {
		
		assertEquals("redirect:/adminproductstyle", adminController.saveProductStyle(new ProductStyle(), model));
	}
	@Test
	public void testSaveProductStyleWithId() {
		ProductStyle ps = new ProductStyle();
		ps.setId(0);
		assertEquals("redirect:/adminproductstyle", adminController.saveProductStyle(ps, model));
	}

	@Test
	public void testEditProductStyle() {
		assertEquals("adminproductstyle", adminController.editProductStyle(0, model));
	}

	@Test
	public void testDeleteProductStyle() {
		assertEquals("redirect:/adminproductstyle", adminController.deleteProductStyle(0));
	}

	@Test
	public void testAdminproduct() {
		assertEquals("adminproduct", adminController.adminproduct(model));
	}

	@Test
	public void testSaveProduct() {
		Product product = new Product();
		product.setProductStyle(new ProductStyle());
		assertEquals("redirect:/adminproduct", adminController.saveProduct(product, model));
	}

	@Test
	public void testEditProduct() {
		assertEquals("adminproduct", adminController.editProduct(0, model));
		assertTrue(model.containsAttribute("sizes"));
	}

	@Test
	public void testDeleteProduct() {
		Product product = new Product();
		product.setProductStyle(new ProductStyle());
		when(productService.getProductById(anyInt())).thenReturn(product);
		assertEquals("redirect:/adminproduct", adminController.deleteProduct(0));
	}

}
