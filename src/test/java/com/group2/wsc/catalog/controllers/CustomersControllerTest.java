package com.group2.wsc.catalog.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.group2.wsc.catalog.domain.Address;
import com.group2.wsc.catalog.domain.CustomerDetail;
import com.group2.wsc.catalog.domain.EmployeeDetail;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.services.CustomerDetailService;
import com.group2.wsc.catalog.services.UserService;
import com.group2.wsc.catalog.services.security.UserDetailsImpl;

public class CustomersControllerTest {

	CustomersController customersController;
	CustomerDetailService customerDetailService = mock(CustomerDetailService.class);
	UserService userService = mock(UserService.class);
	Model model;
	Authentication authentication = mock(Authentication.class);
	SecurityContext securityContext = mock(SecurityContext.class);
	
	
	@Before 
	public void before() {
		List<CustomerDetail> customers = new ArrayList<>();
		CustomerDetail customerDetail = new CustomerDetail();
		customerDetail.setUsername("abc");
		customers.add(customerDetail);
		
		
		model = new BindingAwareModelMap();
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		customersController = new CustomersController();
		customersController.setCustomerDetailService(customerDetailService);
		customersController.setUserService(userService);
		UserDetailsImpl ud = new UserDetailsImpl();
		ud.setUsername("abc");
		when(authentication.getPrincipal()).thenReturn(ud);
		User user = new User();
		UserInformation userInfo = new UserInformation();
		userInfo.setFirstname("a");
		userInfo.setLastname("B");
		userInfo.setCustomerDetail(customerDetail);
		user.setUserInformation(userInfo);
		user.setUsername(ud.getUsername());
		when(userService.findByUsername(anyString())).thenReturn(user);
		
		when(customerDetailService.findByUsernameContainsIgnoreCase(anyString())).thenReturn(customers);
	}


	@Test
	public void testIndexNoUser() {
		assertEquals("customers",customersController.index(model));
		assertTrue(model.containsAttribute("currentUser"));
	}

	@Test
	public void testIndex() {
		when(authentication.getPrincipal()).thenReturn(new User());
		assertEquals("customers",customersController.index(model));
		assertFalse(model.containsAttribute("currentUser"));
	}

	@Test
	public void testIndexUserWithAddresses() {
		User user = new User();
		UserInformation userInfo = new UserInformation();
		user.setUserInformation(userInfo);
		user.setUsername("abc");
		Address address = new Address();
		address.setType("billing");
		address.setCity("asdf");
		userInfo.addAddress(address);
		address = new Address();
		address.setType("shipping");
		address.setCity("asdf");
		userInfo.addAddress(address);
		address = new Address();
		address.setType("home");
		address.setCity("asdf");
		userInfo.addAddress(address);
		when(userService.findByUsername(anyString())).thenReturn(user);
				
		assertEquals("customers",customersController.index(model));
		assertTrue(model.containsAttribute("currentUser"));
		assertTrue(model.containsAttribute("billingAddress"));
		assertTrue(model.containsAttribute("shippingAddress"));

	}
	@Test
	public void testIndexUserWithEmployee() {
		User user = new User();
		UserInformation userInfo = new UserInformation();
		user.setUserInformation(userInfo);
		user.setUsername("abc");
		EmployeeDetail ed = new EmployeeDetail();
		userInfo.setEmployeeDetail(ed);	
		
		when(userService.findByUsername(anyString())).thenReturn(user);
				
		assertEquals("customers",customersController.index(model));
		assertTrue(model.containsAttribute("currentUser"));
		assertTrue(model.containsAttribute("homeAddress"));

	}
	@Test
	public void testIndexUserWithEmployeeHomeAddress() {
		User user = new User();
		UserInformation userInfo = new UserInformation();
		user.setUserInformation(userInfo);
		user.setUsername("abc");
		EmployeeDetail ed = new EmployeeDetail();
		userInfo.setEmployeeDetail(ed);	
		Address address = new Address();
		address.setType("home");
		address.setCity("asdf");
		userInfo.addAddress(address);
		when(userService.findByUsername(anyString())).thenReturn(user);
				
		assertEquals("customers",customersController.index(model));
		assertTrue(model.containsAttribute("currentUser"));
		assertTrue(model.containsAttribute("homeAddress"));
	}

	@Test
	public void testSearchCustomers() {
		assertEquals("fragments/customerSearchResults :: customerSearchResults",customersController.searchCustomers("abc", model));
		assertTrue(model.containsAttribute("customers"));
	}

	@Test
	public void testLoadCustomerStringModel() {
		assertEquals("fragments/selectedCustomer :: selectedCustomer",
				customersController.loadCustomer("abc", model));
		assertTrue(model.containsAttribute("customer"));
	}

	@Test
	public void testLoadCustomerNoUser() {
		when(userService.findByUsername(anyString())).thenReturn(null);
		assertEquals("fragments/selectedCustomer :: selectedCustomer",
				customersController.loadCustomer("abc", model));
		assertFalse(model.containsAttribute("customer"));
	}

	@Test
	public void testLoadCustomerWithAddress() {
		
		User user = new User();
		UserInformation userInfo = new UserInformation();
		CustomerDetail customerDetail = new CustomerDetail();
		customerDetail.setUsername("abc");

		userInfo.setCustomerDetail(customerDetail);
		
		user.setUserInformation(userInfo);
		user.setUsername("abc");
		Address address = new Address();
		address.setType("billing");
		address.setCity("asdf");
		userInfo.addAddress(address);
		address = new Address();
		address.setType("shipping");
		address.setCity("asdf");
		userInfo.addAddress(address);
		address = new Address();
		address.setType("home");
		address.setCity("asdf");
		userInfo.addAddress(address);
		when(userService.findByUsername(anyString())).thenReturn(user);
				
		assertEquals("fragments/selectedCustomer :: selectedCustomer",
				customersController.loadCustomer("abc", model));
		assertTrue(model.containsAttribute("customer"));
		assertTrue(model.containsAttribute("billingAddress"));
		assertTrue(model.containsAttribute("shippingAddress"));
		
	}

}
