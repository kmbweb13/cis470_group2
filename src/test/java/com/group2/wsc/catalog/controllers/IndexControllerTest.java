package com.group2.wsc.catalog.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.group2.wsc.catalog.domain.PhoneNumber;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.services.PhoneNumberService;
import com.group2.wsc.catalog.services.RoleService;
import com.group2.wsc.catalog.services.UserInformationService;
import com.group2.wsc.catalog.services.UserService;

public class IndexControllerTest {
	IndexController indexController;
	UserService userService = mock(UserService.class);
	RoleService roleService = mock(RoleService.class);
	UserInformationService userInformationService = mock(UserInformationService.class);
	Model model;
	PhoneNumberService phoneNumberService = mock(PhoneNumberService.class);

	Authentication authentication = mock(Authentication.class);
	SecurityContext securityContext = mock(SecurityContext.class);

	@Before
	public void before() {
		indexController = new IndexController();

		model = new BindingAwareModelMap();
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		indexController.setUserService(userService);
		indexController.setRoleService(roleService);
		indexController.setPhoneNumberService(phoneNumberService);
		indexController.setUserInformationService(userInformationService);
	}

	@Test
	public void testIndex() {
		assertEquals("index", indexController.index(model));
	}

	@Test
	public void testLogin() {
		assertEquals("login", indexController.login(model));
	}

	@Test
	public void testSignup() {
		assertEquals("signup", indexController.signup(model));
		assertTrue(model.containsAttribute("currentUser"));

	}

	@Test
	public void testCreateUser() {
		User user = new User();
		UserInformation userInfo = new UserInformation();
		user.setUserInformation(userInfo);
		userInfo.setPhonenumbers(Arrays.asList(new PhoneNumber()));
		assertEquals("login", indexController.createUser(user, model));
		assertEquals("GUEST", user.getMainRoleName());
	}

	@Test
	public void testCreateUserGoodDate() {
		User user = new User();
		UserInformation userInfo = new UserInformation();
		user.setUserInformation(userInfo);
		userInfo.setPhonenumbers(Arrays.asList(new PhoneNumber()));
		userInfo.setTempBirthDt("2000-01-01");
		assertEquals("login", indexController.createUser(user, model));
		assertEquals("GUEST", user.getMainRoleName());
	}

	@Test
	public void testCreateUserBadDate() {
		User user = new User();
		UserInformation userInfo = new UserInformation();
		user.setUserInformation(userInfo);
		userInfo.setPhonenumbers(Arrays.asList(new PhoneNumber()));
		userInfo.setTempBirthDt("asd2");
		assertEquals("login", indexController.createUser(user, model));
		assertEquals("GUEST", user.getMainRoleName());
	}

	@Test
	public void testCreateUserBadDateLong() {
		User user = new User();
		UserInformation userInfo = new UserInformation();
		user.setUserInformation(userInfo);
		userInfo.setPhonenumbers(Arrays.asList(new PhoneNumber()));
		userInfo.setTempBirthDt("asd234f");
		assertEquals("login", indexController.createUser(user, model));
		assertEquals("GUEST", user.getMainRoleName());
	}

	@Test
	public void testLogin2() {
		indexController.setUseMock(true);
		assertEquals("login2", indexController.login2(model));
		indexController.setUseMock(false);
		assertEquals("login", indexController.login2(model));

	}

}
