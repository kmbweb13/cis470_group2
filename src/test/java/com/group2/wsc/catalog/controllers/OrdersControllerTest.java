package com.group2.wsc.catalog.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.group2.wsc.catalog.domain.OrderDetail;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.services.OrderDetailService;
import com.group2.wsc.catalog.services.UserService;
import com.group2.wsc.catalog.services.security.UserDetailsImpl;

public class OrdersControllerTest {
	OrdersController ordersController;
	OrderDetailService orderDetailService = mock(OrderDetailService.class);
	UserService userService = mock(UserService.class);
	Model model;

	Authentication authentication = mock(Authentication.class);
	SecurityContext securityContext = mock(SecurityContext.class);

	@Before
	public void before() {
		ordersController = new OrdersController();
		ordersController.setUserService(userService);
		ordersController.setOrderDetailService(orderDetailService);

		model = new BindingAwareModelMap();
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		UserDetailsImpl ud = new UserDetailsImpl();
		ud.setUsername("abc");
		when(authentication.getPrincipal()).thenReturn(ud);
		User user = new User();
		UserInformation userInfo = new UserInformation();
		userInfo.setFirstname("a");
		userInfo.setLastname("B");
		user.setUserInformation(userInfo);
		user.setUsername(ud.getUsername());
		when(userService.findByUsername(anyString())).thenReturn(user);
		OrderDetail od = new OrderDetail();
		od.setUsername("abc");
		when(orderDetailService.getById(anyInt())).thenReturn(od);
		when(orderDetailService.saveOrUpdate(anyObject())).thenReturn(od);

	}

	@Test
	public void testLoadOrder() {
		assertEquals("fragments/selectedOrder :: selectedOrder", ordersController.loadOrder("0", "abc", model));
		assertTrue(model.containsAttribute("customerName"));
	}

	@Test
	public void testLoadOrderEmployee() {
		assertEquals("fragments/selectedOrder :: selectedOrder", ordersController.loadOrder("0", "xyz", model));
		assertTrue(model.containsAttribute("customerName"));
	}

	@Test
	public void testAddPayment() {
		assertEquals("fragments/selectedOrderModalDetail :: selectedOrderModalDetail",
				ordersController.addPayment(0, "15.00", "cash", "xyz", model));
	}

	@Test
	public void testAddPaymentBadPayment() {
		assertEquals("fragments/selectedOrderModalDetail :: selectedOrderModalDetail",
				ordersController.addPayment(0, "asdf", "cash", "xyz", model));
	}

	@Test
	public void testAddPaymentSameUser() {
		assertEquals("fragments/selectedOrderModalDetail :: selectedOrderModalDetail",
				ordersController.addPayment(0, "15.00", "cash", "abc", model));
	}

	@Test
	public void testUpdateOrderallFalseNoDate() {
		assertEquals("redirect:/loadCustomer/abc", ordersController.updateOrder(0, false, false, false, model));
	}

	@Test
	public void testUpdateOrderalltrueNoDate() {
		assertEquals("redirect:/loadCustomer/abc", ordersController.updateOrder(0, true, true, true, model));
	}

	@Test
	public void testUpdateOrderallFalseWithDate() {
		OrderDetail od = new OrderDetail();
		Date date = new Date();
		od.setUsername("abc");
		od.setOrderFinalizedDt(date);
		od.setOrderReceivedDt(date);
		od.setOrderShippedDt(date);
		od.setOrderCompleteDt(date);
		when(orderDetailService.getById(anyInt())).thenReturn(od);
		assertEquals("redirect:/loadCustomer/abc", ordersController.updateOrder(0, false, false, false, model));
	}

	@Test
	public void testUpdateOrderalltrueWithDate() {
		OrderDetail od = new OrderDetail();
		Date date = new Date();
		od.setUsername("abc");
		od.setOrderFinalizedDt(date);
		od.setOrderReceivedDt(date);
		od.setOrderShippedDt(date);
		od.setOrderCompleteDt(date);
		when(orderDetailService.getById(anyInt())).thenReturn(od);
		assertEquals("redirect:/loadCustomer/abc", ordersController.updateOrder(0, true, true, true, model));
	}

	@Test
	public void testUpdateOrderFinal() {
		assertEquals("redirect:/loadCustomer/abc", ordersController.updateOrder(0, true, false, false, model));
	}

	@Test
	public void testUpdateOrderShipped() {
		assertEquals("redirect:/loadCustomer/abc", ordersController.updateOrder(0, false, true, false, model));
	}

	@Test
	public void testUpdateOrderReceived() {
		assertEquals("redirect:/loadCustomer/abc", ordersController.updateOrder(0, false, false, true, model));
	}

	@Test
	public void testUpdateOrderFinalShipped() {
		assertEquals("redirect:/loadCustomer/abc", ordersController.updateOrder(0, true, true, false, model));
	}

	@Test
	public void testUpdateOrderFinalReceived() {
		assertEquals("redirect:/loadCustomer/abc", ordersController.updateOrder(0, true, false, true, model));
	}

	@Test
	public void testUpdateOrderShippedReceived() {
		assertEquals("redirect:/loadCustomer/abc", ordersController.updateOrder(0, false, true, true, model));
	}

	@Test
	public void testCancelOrderEmployee() {
		assertEquals("redirect:/loadCustomer/abc", ordersController.cancelOrderEmployee(0, model));
	}
	@Test
	public void testCancelOrderCustomer() {
		assertEquals("fragments/orderHistory :: orderHistory", ordersController.cancelOrderCustomer(0, model));
	}

}
