package com.group2.wsc.catalog.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.group2.wsc.catalog.domain.Product;
import com.group2.wsc.catalog.domain.ProductStyle;
import com.group2.wsc.catalog.services.ProductService;
import com.group2.wsc.catalog.services.ProductStyleService;
import com.group2.wsc.catalog.services.UserInformationService;
import com.group2.wsc.catalog.services.UserService;

public class ProductControllerTest {
	ProductController productController;
	UserService userService = mock(UserService.class);
	ProductService productService = mock(ProductService.class);
	UserInformationService userInformationService = mock(UserInformationService.class);
	ProductStyleService productStyleService = mock(ProductStyleService.class);
	Model model;
	Product product;
	ProductStyle productStyle;

	Authentication authentication = mock(Authentication.class);
	SecurityContext securityContext = mock(SecurityContext.class);

	@Before
	public void before() {
		productController = new ProductController();

		model = new BindingAwareModelMap();
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		productController.setUserService(userService);
		productController.setProductService(productService);
		productController.setProductStyleService(productStyleService);

		product = new Product();
		productStyle = new ProductStyle();
		productStyle.setId(0);
		product.setProductStyle(productStyle);
		product.setId(0);
		when(productService.getProductById(anyInt())).thenReturn(product);
		when(productService.saveProduct(anyObject())).thenReturn(product);
		when(productStyleService.getById(anyObject())).thenReturn(productStyle);
		
	}

	@Test
	public void testList() {
		assertEquals("products", productController.list(model));
		assertTrue(model.containsAttribute("products"));
	}

	@Test
	public void testShowProduct() {
		assertEquals("productshow", productController.showProduct(0, model));
		assertTrue(model.containsAttribute("product"));
	}

	@Test
	public void testShowProductEngrave() {
		productStyle.setEngrave(true);
		assertEquals("productshow", productController.showProduct(0, model));
		assertTrue(model.containsAttribute("product"));
	}

	@Test
	public void testShowProductPrint() {
		productStyle.setPrint(true);
		assertEquals("productshow", productController.showProduct(0, model));
		assertTrue(model.containsAttribute("product"));
	}

	@Test
	public void testShowProductBoth() {
		productStyle.setEngrave(true);
		productStyle.setPrint(true);
		assertEquals("productshow", productController.showProduct(0, model));
		assertTrue(model.containsAttribute("product"));
	}

	@Test
	public void testEdit() {
		assertEquals("productform", productController.edit(0, model));
		assertTrue(model.containsAttribute("product"));
	}

	@Test
	public void testNewProduct() {
		assertEquals("productform", productController.newProduct(model));
		assertTrue(model.containsAttribute("product"));
	}

	@Test
	public void testSaveProduct() {
		assertEquals("redirect:/product/show/0", productController.saveProduct(product, model));
		assertFalse(model.containsAttribute("product"));
	}

	@Test
	public void testDelete() {
		assertEquals("redirect:/products", productController.delete(0, model));
		assertFalse(model.containsAttribute("product"));		
	}

}
