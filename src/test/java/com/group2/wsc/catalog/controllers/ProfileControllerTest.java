package com.group2.wsc.catalog.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.group2.wsc.catalog.domain.Address;
import com.group2.wsc.catalog.domain.PhoneNumber;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.services.AddressService;
import com.group2.wsc.catalog.services.PhoneNumberService;
import com.group2.wsc.catalog.services.UserInformationService;
import com.group2.wsc.catalog.services.UserService;
import com.group2.wsc.catalog.services.security.UserDetailsImpl;

public class ProfileControllerTest {
	ProfileController profileController;
	
	UserService userService = mock(UserService.class);
	AddressService addressService = mock(AddressService.class);
	UserInformationService userInformationService = mock(UserInformationService.class);
	PhoneNumberService phoneNumberService = mock(PhoneNumberService.class);
	
	Model model;

	Authentication authentication = mock(Authentication.class);
	SecurityContext securityContext = mock(SecurityContext.class);

	@Before
	public void before() {
		profileController = new ProfileController(addressService, userService,  userInformationService, phoneNumberService);
		profileController.setUserService(userService);
		model = new BindingAwareModelMap();
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		UserDetailsImpl ud = new UserDetailsImpl();
		ud.setUsername("abc");
		when(authentication.getPrincipal()).thenReturn(ud);
		User user = new User();
		UserInformation userInfo = new UserInformation();
		userInfo.setFirstname("a");
		userInfo.setLastname("B");
		user.setUserInformation(userInfo);
		user.setUsername(ud.getUsername());
		when(userService.findByUsername(anyString())).thenReturn(user);
		when(userService.getById(anyInt())).thenReturn(user);
		when(phoneNumberService.getById(anyInt())).thenReturn(new PhoneNumber());
		Address address = new Address();
		address.setId(0);
		when(addressService.saveOrUpdate(anyObject())).thenReturn(address);
		when(addressService.getById(anyInt())).thenReturn(address);

	}
	
	@Test
	public void testUserprofile() {
		assertEquals("userprofile", profileController.userprofile(model));
		assertTrue(model.containsAttribute("customer"));
	}

	@Test
	public void testUserprofileSuccess() {
		assertEquals("userprofile", profileController.userprofileSuccess(model));
		assertTrue(model.containsAttribute("customer"));
	}

	@Test
	public void testBillingAddressSuccess() {
		assertEquals("userprofile", profileController.billingAddressSuccess(model));
		assertTrue(model.containsAttribute("customer"));
		assertTrue(model.containsAttribute("displayTab"));
	}

	@Test
	public void testShippingAddressSuccess() {
		assertEquals("userprofile", profileController.shippingAddressSuccess(model));
		assertTrue(model.containsAttribute("customer"));
		assertTrue(model.containsAttribute("displayTab"));
	}

	@Test
	public void testShippingHomeSuccess() {
		assertEquals("userprofile", profileController.shippingHomeSuccess(model));
		assertTrue(model.containsAttribute("customer"));
		assertTrue(model.containsAttribute("displayTab"));
	}

	@Test
	public void testEdituserprofile() {
		assertEquals("fragments/userProfileContent :: userProfileContent", profileController.edituserprofile(model));
		assertTrue(model.containsAttribute("editibleProfile"));
	}

	@Test
	public void testCanceledituserprofile() {
		assertEquals("fragments/userProfileContent :: userProfileContent", profileController.canceledituserprofile(model));
		
	}

	@Test
	public void testUpdateUser() {
		User user = new User();
		UserInformation userInfo = new UserInformation();
		user.setUserInformation(userInfo);
		assertEquals("redirect:/userprofileSuccess", profileController.updateUser(user, model));
		
	}
	@Test
	public void testUpdateUserWithPhoneNumbers() {
		User user = new User();
		UserInformation userInfo = new UserInformation();
		user.setUserInformation(userInfo);
		userInfo.setPhonenumbers(Arrays.asList(new PhoneNumber()));
		assertEquals("redirect:/userprofileSuccess", profileController.updateUser(user, model));
		
	}

	@Test
	public void testUpdateUserWithvalidBirthDate() {
		User user = new User();
		UserInformation userInfo = new UserInformation();
		user.setUserInformation(userInfo);
		userInfo.setTempBirthDt("2000-01-01");
		assertEquals("redirect:/userprofileSuccess", profileController.updateUser(user, model));
		
	}
	@Test
	public void testUpdateUserWithinvalidBirthDate() {
		User user = new User();
		UserInformation userInfo = new UserInformation();
		user.setUserInformation(userInfo);
		userInfo.setTempBirthDt("asdfasdf");
		assertEquals("redirect:/userprofileSuccess", profileController.updateUser(user, model));
		
	}
	@Test
	public void testUpdateUserWithvalidshortBirthDate() {
		User user = new User();
		UserInformation userInfo = new UserInformation();
		user.setUserInformation(userInfo);
		userInfo.setTempBirthDt("2000");
		assertEquals("redirect:/userprofileSuccess", profileController.updateUser(user, model));
		
	}

	@Test
	public void testEditShippingAddress() {
		assertEquals("fragments/userProfileAddress :: userProfileAddress", profileController.editShippingAddress(model));
		assertTrue(model.containsAttribute("editibleShippingAddress"));
	}

	@Test
	public void testEditBillingAddress() {
		assertEquals("fragments/userProfileAddress :: userProfileAddress", profileController.editBillingAddress(model));
		assertTrue(model.containsAttribute("editibleBillingAddress"));
	}

	@Test
	public void testEditHomeAddress() {
		assertEquals("fragments/userProfileEmployee :: userProfileEmployee", profileController.editHomeAddress(model));
		assertTrue(model.containsAttribute("editibleHomeAddress"));
	}

	@Test
	public void testCancelEditShippingAddress() {
		assertEquals("fragments/userProfileAddress :: userProfileAddress", profileController.cancelEditShippingAddress(model));
	}

	@Test
	public void testCancelEditHomeAddress() {
		assertEquals("fragments/userProfileEmployee :: userProfileEmployee", profileController.cancelEditHomeAddress(model));
		
	}

	@Test
	public void testUpdateAddressNewBilling() {
		Address address = new Address();
		address.setId(0);
		address.setType("billing");
		when(addressService.saveOrUpdate(anyObject())).thenReturn(address);
		when(addressService.getById(anyInt())).thenReturn(address);
		assertEquals("redirect:/userBillingSuccess", profileController.updateAddress(new Address(), model));
	}

	@Test
	public void testUpdateAddressExstingBilling() {
		Address address = new Address();
		address.setId(0);
		address.setType("billing");
		when(addressService.saveOrUpdate(anyObject())).thenReturn(address);
		when(addressService.getById(anyInt())).thenReturn(address);
		assertEquals("redirect:/userBillingSuccess", profileController.updateAddress(address, model));
	}
	@Test
	public void testUpdateAddressHome() {
		Address address = new Address();
		address.setId(0);
		address.setType("home");
		when(addressService.saveOrUpdate(anyObject())).thenReturn(address);
		when(addressService.getById(anyInt())).thenReturn(address);
		assertEquals("redirect:/userHomeSuccess", profileController.updateAddress(address, model));
	}
	@Test
	public void testUpdateAddressShipping() {
		Address address = new Address();
		address.setId(0);
		address.setType("shipping");
		when(addressService.saveOrUpdate(anyObject())).thenReturn(address);
		when(addressService.getById(anyInt())).thenReturn(address);
		assertEquals("redirect:/userShippingSuccess", profileController.updateAddress(address, model));
	}

}
