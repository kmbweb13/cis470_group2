package com.group2.wsc.catalog.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.group2.wsc.catalog.domain.Payment;
import com.group2.wsc.catalog.domain.Product;
import com.group2.wsc.catalog.domain.ProductOrderDetail;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.services.ProductService;
import com.group2.wsc.catalog.services.ShoppingCartService;
import com.group2.wsc.catalog.services.UserService;
import com.group2.wsc.catalog.services.security.UserDetailsImpl;

public class ShoppingCartControllerTest {
	ShoppingCartController shoppingCartController;

	UserService userService = mock(UserService.class);
	ProductService productService = mock(ProductService.class);
	ShoppingCartService shoppingCartService = mock(ShoppingCartService.class);
	Model model;
	ProductOrderDetail productOrderDetail;
	
	Authentication authentication = mock(Authentication.class);
	SecurityContext securityContext = mock(SecurityContext.class);
	private Product product;

	@Before
	public void before() {
		shoppingCartController = new ShoppingCartController(shoppingCartService, productService);
		shoppingCartController.setUserService(userService);
		model = new BindingAwareModelMap();
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		
		UserDetailsImpl ud = new UserDetailsImpl();
		ud.setUsername("abc");
		when(authentication.getPrincipal()).thenReturn(ud);
		User user = new User();
		UserInformation userInfo = new UserInformation();
		userInfo.setFirstname("a");
		userInfo.setLastname("B");
		user.setUserInformation(userInfo);
		user.setUsername(ud.getUsername());
		when(userService.findByUsername(anyString())).thenReturn(user);
		
		product = new Product();
		product.setId(0);
		when(productService.getProductById(anyInt())).thenReturn(product);
		when(productService.saveProduct(anyObject())).thenReturn(product);
		when(shoppingCartService.getSubTotal()).thenReturn(null);
		when(shoppingCartService.getTax()).thenReturn(null);
		when(shoppingCartService.getTotal()).thenReturn(null);
		when(shoppingCartService.getMinPayment()).thenReturn(null);
		when(shoppingCartService.getExistingOrderid()).thenReturn(0);
		doNothing().when(shoppingCartService).clearCart();
		productOrderDetail = new ProductOrderDetail();
		
		ProductOrderDetail pod = new ProductOrderDetail();
		pod.setId(0);
		when(shoppingCartService.getProductOrderDetailFromCart(anyInt())).thenReturn(pod);
		when(shoppingCartService.loadPreviousOpenOrder(anyInt())).thenReturn(0);
		doNothing().when(shoppingCartService).checkout(anyObject(), anyObject());
		
	}
	@Test
	public void testShoppingCartController() {

		when(shoppingCartService.getExistingOrderid()).thenReturn(null);
		assertEquals("shoppingCart", shoppingCartController.shoppingCart(model));
		assertTrue(model.containsAttribute("subtotal"));
		assertFalse(model.containsAttribute("updateOrder"));
	}
	@Test
	public void testShoppingCartControllerExistingOrder() {
		assertEquals("shoppingCart", shoppingCartController.shoppingCart(model));
		assertTrue(model.containsAttribute("subtotal"));
		assertTrue(model.containsAttribute("updateOrder"));
	}

	@Test
	public void testClearCart() {
		assertEquals("redirect:/products", shoppingCartController.clearCart(model));
	}


	@Test
	public void testAddProductToCart() {
		assertEquals("redirect:/shoppingCart", shoppingCartController.addProductToCart(productOrderDetail, model));
	}

	@Test
	public void testRemoveProductFromCart() {
		assertEquals("shoppingCart", shoppingCartController.removeProductFromCart("0", model));
	}

	@Test
	public void testEditProductInCart() {
		assertEquals("productshow", shoppingCartController.editProductInCart("0", model));
	}

	@Test
	public void testEditExistingProduct() {
		assertEquals("shoppingCart", shoppingCartController.editExistingProduct("0", model));
	}

	@Test
	public void testCheckout() {
		assertEquals("shoppingCart", shoppingCartController.checkout(new Payment(), model));
	}

}
