package com.group2.wsc.catalog.converters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;
import org.springframework.security.core.userdetails.UserDetails;

import com.group2.wsc.catalog.domain.Role;
import com.group2.wsc.catalog.domain.User;

public class UserToUserDetailsTest {

	@Test
	public void testConvertUserIsNull() {
		UserToUserDetails userDetail = new UserToUserDetails();
		UserDetails detailsImp = userDetail.convert(null);

		assertNotNull(detailsImp);
		assertNull(detailsImp.getUsername());
		assertNull(detailsImp.getPassword());
		assertTrue(detailsImp.isEnabled());
		assertNull(detailsImp.getAuthorities());
	}
	@Test
	public void testConvertUserIsNotNull() {
		Role role = new Role();
		role.setRole("user");
		User user = new User();
		user.setUsername("asdf");
		user.setEncryptedPassword("asdf");
		user.setRoles(Arrays.asList(role));
		UserToUserDetails userDetail = new UserToUserDetails();
		UserDetails detailsImp = userDetail.convert(user);
		
		assertNotNull(detailsImp);	
		assertEquals(detailsImp.getUsername(), user.getUsername());
		assertEquals(detailsImp.getPassword(), user.getEncryptedPassword());
		assertEquals(detailsImp.getAuthorities().size(), 1);
	}

}