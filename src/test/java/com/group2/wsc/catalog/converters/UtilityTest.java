package com.group2.wsc.catalog.converters;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.group2.wsc.catalog.domain.CustomerDetail;
import com.group2.wsc.catalog.domain.EmployeeDetail;
import com.group2.wsc.catalog.services.CustomerDetailService;
import com.group2.wsc.catalog.services.EmployeeDetailService;

public class UtilityTest {
	 Utility utility; 
	 CustomerDetailService customerDetailService = mock(CustomerDetailService.class);
	 EmployeeDetailService employeeDetailService = mock(EmployeeDetailService.class);
		

	@Before
	public void before() {
		utility = new Utility();
		when(customerDetailService.saveOrUpdate(any())).thenAnswer(new Answer<CustomerDetail>() {
		    @Override
		    public CustomerDetail answer(InvocationOnMock invocation) throws Throwable {
		      Object[] args = invocation.getArguments();
		      return (CustomerDetail) args[0];
		    }
		  });
		utility.setCustomerDetailService(customerDetailService);
		when(employeeDetailService.saveOrUpdate(any())).thenAnswer(new Answer<EmployeeDetail>() {
		    @Override
		    public EmployeeDetail answer(InvocationOnMock invocation) throws Throwable {
		      Object[] args = invocation.getArguments();
		      return (EmployeeDetail) args[0];
		    }
		  });
		utility.setEmployeeDetailService(employeeDetailService);
	}

	@Test
	public void testCreateCurrencyDouble() {
		assertEquals("-50.00", Utility.createCurrency(-50.0).toString());
		assertEquals("0.00", Utility.createCurrency(0.0).toString());
		assertEquals("1.50", Utility.createCurrency(1.5).toString());
		assertEquals("0.50", Utility.createCurrency(0.5).toString());
		assertEquals("15.25", Utility.createCurrency(15.25352).toString());
		assertEquals("18.24", Utility.createCurrency(18.239).toString());
	}

	@Test
	public void testCreateCurrencyString() {
		assertEquals("-50.00", Utility.createCurrency("-50.0").toString());
		assertEquals("0.00", Utility.createCurrency("0.0").toString());
		assertEquals("1.50", Utility.createCurrency("1.5").toString());
		assertEquals("0.50", Utility.createCurrency("0.5").toString());
		assertEquals("15.25", Utility.createCurrency("15.25352").toString());
		assertEquals("18.24", Utility.createCurrency("18.239").toString());
		assertEquals("0.00", Utility.createCurrency("abc").toString());
		}

	@Test
	public void testCreateCurrencyBigDecimal() {
		assertEquals("50.07", Utility.createCurrency(new BigDecimal("50.0658413")).toString());
		assertEquals("-50.07", Utility.createCurrency(new BigDecimal("-50.0658413")).toString());
	}
	

	@Test
	public void testGenerateCustomerDetails() {
		CustomerDetail customerDetail = utility.generateCustomerDetails("joe", "123");
		assertEquals("joe", customerDetail.getUsername());
		assertEquals("123", customerDetail.getCustomerId());
	}

	@Test
	public void testGenerateEmployeeDetails() {
		Date date = new Date();
		EmployeeDetail employeeDetail = utility.generateEmployeeDetails("joe", "123", date);
		assertEquals("joe", employeeDetail.getUsername());
		assertEquals("123", employeeDetail.getEmployeeId());
		assertEquals(date.toString(), employeeDetail.getHireDt().toString());
	}


}
