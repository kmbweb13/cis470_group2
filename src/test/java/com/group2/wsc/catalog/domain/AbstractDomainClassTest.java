package com.group2.wsc.catalog.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

import java.util.Date;

import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class AbstractDomainClassTest {

	@Test
	public void testTimeStamps() {
		AbstractDomainClass adc = new AbstractDomainClass();
		assertNull(adc.getDateCreated());
		assertNull(adc.getLastUpdated());
		adc.updateTimeStamps();
		assertEquals(adc.getDateCreated().getTime(), adc.getLastUpdated().getTime());
		try {
			// pause thread one second to allow new date to be created for update
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			//do nothing
		}
		adc.updateTimeStamps();
		assertNotEquals(adc.getDateCreated().getTime(), adc.getLastUpdated().getTime());
	}

	@Test
	public void testLomBok() {
		AbstractDomainClass adc = new AbstractDomainClass();
		adc.toString();
		adc.setLastUpdated(new Date());
		adc.setVersion(1);
		EqualsVerifier.forClass(AbstractDomainClass.class)
			.suppress(Warning.STRICT_INHERITANCE)
			.suppress(Warning.NONFINAL_FIELDS)
			.suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)
			.withRedefinedSuperclass()
			.verify();
	}

}
