package com.group2.wsc.catalog.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class CustomerDetailTest {
	CustomerDetail customerDetail;
	OrderDetail orderDetail;
	@Before 
	public void before() {
		customerDetail = new CustomerDetail();
		orderDetail = new OrderDetail();
		orderDetail.setEmployeeId("z123123");
		
		customerDetail.addOrderDetail(orderDetail);
		
	}

	@Test
	public void testAddOrderDetail() {
		assertEquals(1, customerDetail.getOrderDetails().size());
		customerDetail.addOrderDetail(orderDetail);
		assertEquals(1, customerDetail.getOrderDetails().size());
	}

	@Test
	public void testAddOrderDetailNew() {
		assertEquals(1, customerDetail.getOrderDetails().size());
		OrderDetail orderDetail = new OrderDetail();
		orderDetail.setEmployeeId("r345345");
		customerDetail.addOrderDetail(orderDetail);
		assertEquals(2, customerDetail.getOrderDetails().size());
	}

	@Test
	public void testRemoveOrderDetail() {
		assertEquals(1, customerDetail.getOrderDetails().size());
		customerDetail.removeOrderDetail(orderDetail);
		assertEquals(0, customerDetail.getOrderDetails().size());
	}

	@Test
	public void testLomBok() {
		customerDetail.toString();
		EqualsVerifier.forClass( CustomerDetail.class )
	        .suppress( Warning.STRICT_INHERITANCE )
	        .suppress(Warning.NONFINAL_FIELDS)
	        .suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)
	        .withPrefabValues(OrderDetail.class, orderDetail, new OrderDetail())
	        .withRedefinedSuperclass()
	        .verify();
	}
}
