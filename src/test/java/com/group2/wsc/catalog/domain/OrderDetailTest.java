package com.group2.wsc.catalog.domain;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.group2.wsc.catalog.converters.Utility;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class OrderDetailTest {

	OrderDetail orderDetail;
	List<ProductOrderDetail> list;
	ProductOrderDetail pod;

	@Before
	public void before() {
		orderDetail = new OrderDetail();
		pod = new ProductOrderDetail();
		list = new ArrayList<>();
		list.add(pod);
		orderDetail.setProductOrderDetails(list);
	}

	@Test
	public void testAddProductOrderDetail() {
		assertEquals(1, orderDetail.getProductOrderDetails().size());
		orderDetail.addProductOrderDetail(pod);
		assertEquals(1, orderDetail.getProductOrderDetails().size());
	}

	@Test
	public void testAddProductOrderDetailNew() {
		pod = new ProductOrderDetail();
		pod.setColor("red");
		assertEquals(1, orderDetail.getProductOrderDetails().size());
		orderDetail.addProductOrderDetail(pod);
		assertEquals(2, orderDetail.getProductOrderDetails().size());
	}

	@Test
	public void testRemoveProductOrderDetail() {

		assertEquals(1, orderDetail.getProductOrderDetails().size());
		orderDetail.removeProductOrderDetail(pod);
		assertEquals(0, orderDetail.getProductOrderDetails().size());
	}

	@Test
	public void testOutstandingBalance() {
		BigDecimal total = Utility.createCurrency(5);
		orderDetail.setOrderTotal(total);
		assertEquals(total, orderDetail.outstandingBalance());
	}

	@Test
	public void testOutstandingBalanceWithPayment() {
		BigDecimal total = Utility.createCurrency(5);
		orderDetail.setOrderTotal(total);
		Payment payment = new Payment();
		payment.setAmount(Utility.createCurrency(3));
		orderDetail.addPayment(payment);
		assertEquals(Utility.createCurrency(2), orderDetail.outstandingBalance());
	}
	

	@Test
	public void testLomBok() {
		orderDetail.setOrderCanceledDt(new Date());
		orderDetail.setPayments(new ArrayList<>());
		orderDetail.toString();
		
		ProductOrderDetail productOrderDetail = new ProductOrderDetail();
		productOrderDetail.setColor("red");
		OrderDetail orderDetail2 = new OrderDetail();
		orderDetail2.setId(2);
		orderDetail2.setDateCreated(new Date());
		orderDetail2.setEmployeeId("x123123");
		EqualsVerifier.forClass( OrderDetail.class )
	        .suppress( Warning.STRICT_INHERITANCE )
	        .suppress(Warning.NONFINAL_FIELDS)
	        .suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)
	        .withPrefabValues(ProductOrderDetail.class, productOrderDetail , new ProductOrderDetail())
	        .withPrefabValues(OrderDetail.class, orderDetail2 , new OrderDetail())
	        .withRedefinedSuperclass()
	        .verify();
	}
}
