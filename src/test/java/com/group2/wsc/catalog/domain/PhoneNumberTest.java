package com.group2.wsc.catalog.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class PhoneNumberTest {
	PhoneNumber phoneNumber;

	@Before
	public void before() {
		phoneNumber = new PhoneNumber();

		phoneNumber.setType("a");
		phoneNumber.setCountryCode("country");
		phoneNumber.setAreacode("area");
		phoneNumber.setPrefix("prefix");
		phoneNumber.setLinenumber("line");
	}

	@Test
	public void testToString() {
		assertEquals("(area) prefix-line", phoneNumber.toString());
	}

	@Test
	public void testToStringWithCountry() {
		assertEquals("+country (area) prefix-line", phoneNumber.toStringWithCountryCode());
	}

	@Test
	public void testDisplayValue() {
		assertEquals("a : (area) prefix-line", phoneNumber.displayValue());
	}

	@Test
	public void testLomBok() {

		(new PhoneNumber()).toString();
		EqualsVerifier.forClass(PhoneNumber.class).suppress(Warning.STRICT_INHERITANCE)
				.suppress(Warning.NONFINAL_FIELDS).suppress(Warning.ALL_FIELDS_SHOULD_BE_USED).withRedefinedSuperclass()
				.verify();
	}

}
