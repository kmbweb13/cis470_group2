package com.group2.wsc.catalog.domain;

import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class ProductOrderDetailTest {


	@Test
	public void testLomBok() {
		ProductOrderDetail productOrderDetail = new ProductOrderDetail();
		productOrderDetail.setDescription("a");
		productOrderDetail.setSize("");
		productOrderDetail.setEngrave(true);
		productOrderDetail.setPrint(true);
		productOrderDetail = new ProductOrderDetail();
		productOrderDetail.setEngrave(false);
		productOrderDetail.setPrint(false);
		ProductStyle productStyle = new ProductStyle();
		productStyle.setVersion(1);
		productStyle.setCategory(new Category());
		productStyle.setCode("abc");
		EqualsVerifier.forClass( ProductOrderDetail.class )
	        .suppress( Warning.STRICT_INHERITANCE )
	        .suppress(Warning.NONFINAL_FIELDS)
	        .suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)
	        .withPrefabValues(ProductStyle.class, productStyle, new ProductStyle())
	        .verify();
	}

}
