package com.group2.wsc.catalog.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class ProductStyleTest {
	ProductStyle productStyle;
	Product product;
	@Before 
	public void before() {
		productStyle = new ProductStyle();
		product = new Product();
		product.setImageUrl("aer");
		product.setId(1);
		
		productStyle.addProduct(product);
		
	}

	@Test
	public void testAddProduct() {
		assertEquals(1, productStyle.getProducts().size());
		productStyle.addProduct(product);
		assertEquals(1, productStyle.getProducts().size());
	}

	@Test
	public void testAddProductWithUpdate() {
		assertEquals(1, productStyle.getProducts().size());
		Product product = new Product();
		product.setImageUrl("aasdfasdfawer");
		product.setId(1);
		assertEquals(1, productStyle.getProducts().size());
	}

	@Test
	public void testAddProductNew() {
		assertEquals(1, productStyle.getProducts().size());
		Product product = new Product();
		product.setImageUrl("aasdfasdfawer");
		product.setId(2);
		productStyle.addProduct(product);
		assertEquals(2, productStyle.getProducts().size());
	}

	@Test
	public void testRemoveProduct() {
		assertEquals(1, productStyle.getProducts().size());
		productStyle.removeProduct(product);
		assertEquals(0, productStyle.getProducts().size());
	}
	
	@Test
	public void testHasProducts() {
		assertTrue(productStyle.hasProducts());
	}
	
	@Test
	public void testHasProductsNone() {
		ProductStyle productStyle = new ProductStyle();
		assertFalse(productStyle.hasProducts());
	}

	@Test
	public void testLomBok() {
		ProductStyle productStyle = new ProductStyle();
		productStyle.setVersion(1);
		productStyle.setCategory(new Category());
		productStyle.setCode("abc");
		productStyle.toString();
		EqualsVerifier.forClass( ProductStyle.class )
	        .suppress( Warning.STRICT_INHERITANCE )
	        .suppress(Warning.NONFINAL_FIELDS)
	        .suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)
	        .withPrefabValues(ProductStyle.class, productStyle, new ProductStyle())
	        .verify();
	}

}
