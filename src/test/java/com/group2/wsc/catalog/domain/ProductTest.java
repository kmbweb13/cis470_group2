package com.group2.wsc.catalog.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

public class ProductTest {

	@Test
	public void testEquals() {
		Product product = new Product();
		product.setImageUrl("as");
		product.setPrice(new BigDecimal(0));
		product.setVersion(1);
		product.toString();
		product.setId(1);
		product.setDescription("asdfasdfasd");
		Product product2 = new Product();
		product2.setImageUrl("aafasdfs");
		product2.setPrice(new BigDecimal(34));
		product2.setVersion(0);
		product2.setId(5);
		product2.setDescription("asdfasdfasd");
		
		assertFalse(product.equals(product2));
	}
	@Test
	public void testEqualsSameId() {
		Product product = new Product();
		product.setImageUrl("as");
		product.setPrice(new BigDecimal(0));
		product.setVersion(1);
		product.toString();
		product.setId(1);
		product.setDescription("asdfasdfasd");
		Product product2 = new Product();
		product2.setImageUrl("aafasdfs");
		product2.setPrice(new BigDecimal(34));
		product2.setVersion(0);
		product2.setId(1);
		product2.setDescription("asdfasdfasd");
		
		assertTrue(product.equals(product2));
	}

}
