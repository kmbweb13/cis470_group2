package com.group2.wsc.catalog.domain;

import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class RoleTest {


	@Test
	public void testLomBok() {

		(new Role()).toString();
		EqualsVerifier.forClass(Role.class)
			.suppress(Warning.STRICT_INHERITANCE)
			.suppress(Warning.NONFINAL_FIELDS)
			.suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)
			.withRedefinedSuperclass()
			.verify();
	}


}
