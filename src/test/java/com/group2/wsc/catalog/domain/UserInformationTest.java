package com.group2.wsc.catalog.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class UserInformationTest {

	UserInformation userInfo;
	Address address;
	PhoneNumber phoneNumber;
	@Before
	public void before() {
		address = new Address();
		address.setState("123");
		address.setType("a");
		phoneNumber = new PhoneNumber();
		phoneNumber.setAreacode("123");
		phoneNumber.setType("a");
		userInfo = new UserInformation();
		userInfo.addAddress(address);
		userInfo.addPhoneNumber(phoneNumber);

	}

	@Test
	public void testAddAddress() {
		assertEquals(1, userInfo.getAddresses().size());
		userInfo.addAddress(address);
		assertEquals(1, userInfo.getAddresses().size());
	}

	@Test
	public void testAddAddressNew() {
		Address address = new Address();
		address.setState("1234");
		assertEquals(1, userInfo.getAddresses().size());
		userInfo.addAddress(address);
		assertEquals(2, userInfo.getAddresses().size());
	}

	@Test
	public void testGetAddressByType() {
		Address address = new Address();
		address.setType("b");
		userInfo.addAddress(address);
		assertEquals("b", userInfo.getAddressByType("b").getType());
		assertEquals("a", userInfo.getAddressByType("a").getType());
		assertEquals(null, userInfo.getAddressByType("c"));
		
	}

	@Test
	public void testRemoveAddress() {
		assertEquals(1, userInfo.getAddresses().size());
		userInfo.removeAddress(address);
		assertEquals(0, userInfo.getAddresses().size());
	}
	@Test
	public void testAddPhoneNumber() {
		assertEquals(1, userInfo.getPhonenumbers().size());
		userInfo.addPhoneNumber(phoneNumber);
		assertEquals(1, userInfo.getPhonenumbers().size());
	}

	@Test
	public void testAddPhoneNumberNew() {
		PhoneNumber phoneNumber = new PhoneNumber();
		phoneNumber.setAreacode("456");
		assertEquals(1, userInfo.getPhonenumbers().size());
		userInfo.addPhoneNumber(phoneNumber);
		assertEquals(2, userInfo.getPhonenumbers().size());
	}

	@Test
	public void testGetPhoneNumberByType() {
		PhoneNumber phoneNumber = new PhoneNumber();
		phoneNumber.setType("b");
		userInfo.addPhoneNumber(phoneNumber);
		assertEquals("b", userInfo.getPhoneNumberByType("b").getType());
		assertEquals("a", userInfo.getPhoneNumberByType("a").getType());
		assertEquals(null, userInfo.getPhoneNumberByType("c"));
		
	}

	@Test 
	public void testFullName() { 
		userInfo.setFirstname("a");
		userInfo.setLastname("b");
		assertEquals("a b", userInfo.getFullName());
	}

	@Test
	public void testRemovePhoneNumber() {
		assertEquals(1, userInfo.getPhonenumbers().size());
		userInfo.removePhoneNumber(phoneNumber);
		assertEquals(0, userInfo.getPhonenumbers().size());
	}
	

	@Test
	public void testLomBok() {
		OrderDetail orderDetail = new OrderDetail();
		orderDetail.setEmployeeId("S");
		(new UserInformation()).toString();
		EqualsVerifier.forClass(UserInformation.class)
			.suppress(Warning.STRICT_INHERITANCE)
			.suppress(Warning.NONFINAL_FIELDS)
			.suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)
			.withRedefinedSuperclass()
			.withPrefabValues(OrderDetail.class, orderDetail, new OrderDetail())
			.verify();
	}


}
