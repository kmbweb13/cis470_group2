package com.group2.wsc.catalog.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class UserTest {
	User user;
	Role role;
	
	@Before 
	public void before() {
		user = new User();
		role = new Role();
		role.setRole("a");
		user.addRole(role);
	}

	@Test 
	public void testAddRole() {
		assertEquals(1, user.getRoles().size());
		user.addRole(role);
		assertEquals(1, user.getRoles().size());
	}
	@Test 
	public void testAddRoleNew() {
		Role role = new Role();
		role.setRole("b");
		assertEquals(1, user.getRoles().size());
		user.addRole(role);
		assertEquals(2, user.getRoles().size());
	}

	@Test 
	public void testRemoveRole() {
		assertEquals(1, user.getRoles().size());
		user.removeRole(role);
		assertEquals(0, user.getRoles().size());
	}
	
	@Test
	public void testLomBok() {
		user.setConfirmPassword("g");
		user.setFailedLoginAttempts(1);
		CustomerDetail detail = new CustomerDetail();
		detail.setUsername("abc");
		(new User()).toString();
		EqualsVerifier.forClass(User.class)
			.suppress(Warning.STRICT_INHERITANCE)
			.suppress(Warning.NONFINAL_FIELDS)
			.suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)
			.withRedefinedSuperclass()
			.withPrefabValues(CustomerDetail.class, detail, new CustomerDetail())
			.verify();
	}

}
