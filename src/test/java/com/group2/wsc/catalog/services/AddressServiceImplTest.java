package com.group2.wsc.catalog.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.group2.wsc.catalog.domain.Address;
import com.group2.wsc.catalog.repositories.AddressRepository;

public class AddressServiceImplTest {
	AddressServiceImpl addressServiceImpl;
	AddressRepository addressRepository = mock(AddressRepository.class);

	@Before
	public void before() {
		List<Address> addresss = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			Address address = new Address();
			address.setId(i);
			address.setCity(i + "");
			addresss.add(address);
		}
		addressServiceImpl = new AddressServiceImpl();
		addressServiceImpl.setAddressRepository(addressRepository);
		when(addressRepository.findAll()).thenReturn(addresss);
		when(addressRepository.findOne(anyInt())).thenReturn(addresss.get(1));
		when(addressRepository.save(addresss.get(1))).thenReturn(addresss.get(1));
		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();
				int index = (Integer) args[0];
				addresss.remove(index);
				return null;
			}
		}).when(addressRepository).delete(anyInt());

	}

	@Test
	public void testListAllAddresss() {
		assertEquals(3, addressServiceImpl.listAll().spliterator().getExactSizeIfKnown());
	}

	@Test
	public void testGetAddressById() {
		Address address = addressServiceImpl.getById(1);
		assertEquals("1", address.getCity());
	}

	@Test
	public void testSaveAddress() {
		Address address = addressServiceImpl.getById(1);
		addressServiceImpl.saveOrUpdate(address);
		assertEquals("1", address.getCity());
	}

	@Test
	public void testDeleteAddress() {
		assertEquals(3, addressServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		addressServiceImpl.delete(1);
		assertEquals(2, addressServiceImpl.listAll().spliterator().getExactSizeIfKnown());

	}
}