package com.group2.wsc.catalog.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.group2.wsc.catalog.domain.Category;
import com.group2.wsc.catalog.repositories.CategoryRepository;

public class CategoryServiceImplTest {
	CategoryServiceImpl categoryServiceImpl;
	CategoryRepository categoryRepository = mock(CategoryRepository.class);

	@Before
	public void before() {
		List<Category> categorys = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			Category category = new Category();
			category.setId(i);
			category.setLabel(i + "");
			categorys.add(category);
		}
		categoryServiceImpl = new CategoryServiceImpl();
		categoryServiceImpl.setCategoryRepository(categoryRepository);
		when(categoryRepository.findAll()).thenReturn(categorys);
		when(categoryRepository.findOne(anyInt())).thenReturn(categorys.get(1));
		when(categoryRepository.save(categorys.get(1))).thenReturn(categorys.get(1));
		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();
				int index = (Integer) args[0];
				categorys.remove(index);
				return null;
			}
		}).when(categoryRepository).delete(anyInt());

	}

	@Test
	public void testListAllCategorys() {
		assertEquals(3, categoryServiceImpl.listAll().spliterator().getExactSizeIfKnown());
	}

	@Test
	public void testGetCategoryById() {
		Category category = categoryServiceImpl.getById(1);
		assertEquals("1", category.getLabel());
	}

	@Test
	public void testSaveCategory() {
		Category category = categoryServiceImpl.getById(1);
		categoryServiceImpl.saveOrUpdate(category);
		assertEquals("1", category.getLabel());
	}

	@Test
	public void testDeleteCategory() {
		assertEquals(3, categoryServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		categoryServiceImpl.delete(1);
		assertEquals(2, categoryServiceImpl.listAll().spliterator().getExactSizeIfKnown());

	}
}