package com.group2.wsc.catalog.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.group2.wsc.catalog.domain.CustomerDetail;
import com.group2.wsc.catalog.repositories.CustomerDetailRepository;

public class CustomerDetailServiceImplTest {
	CustomerDetailServiceImpl customerDetailServiceImpl;
	CustomerDetailRepository customerDetailRepository = mock(CustomerDetailRepository.class);

	@Before
	public void before() {
		List<CustomerDetail> customerDetails = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			CustomerDetail customerDetail = new CustomerDetail();
			customerDetail.setId(i);
			customerDetail.setCustomerId(i + "");
			customerDetail.setUsername("abc" + 1);
			customerDetails.add(customerDetail);
		}
		customerDetailServiceImpl = new CustomerDetailServiceImpl();
		customerDetailServiceImpl.setCustomerDetailRepository(customerDetailRepository);
		when(customerDetailRepository.findAll()).thenReturn(customerDetails);
		when(customerDetailRepository.findOne(anyInt())).thenReturn(customerDetails.get(1));
		when(customerDetailRepository.save(customerDetails.get(1))).thenReturn(customerDetails.get(1));

		when(customerDetailRepository.findByUsernameContainsIgnoreCase(anyString()))
				.then(new Answer<List<CustomerDetail>>() {
					public List<CustomerDetail> answer(InvocationOnMock invocation) {
						Object[] args = invocation.getArguments();
						String key = (String) args[0];
						final String searchKey = key.toLowerCase();
						return customerDetails.stream().filter(cd -> cd.getUsername().toLowerCase().contains(searchKey))
								.collect(Collectors.toList());
					}
				});

		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();
				int index = (Integer) args[0];
				customerDetails.remove(index);
				return null;
			}
		}).when(customerDetailRepository).delete(anyInt());

	}

	@Test
	public void testListAllCustomerDetails() {
		assertEquals(3, customerDetailServiceImpl.listAll().spliterator().getExactSizeIfKnown());
	}

	@Test
	public void testGetCustomerDetailById() {
		CustomerDetail customerDetail = customerDetailServiceImpl.getById(1);
		assertEquals("1", customerDetail.getCustomerId());
	}

	@Test
	public void testSaveCustomerDetail() {
		CustomerDetail customerDetail = customerDetailServiceImpl.getById(1);
		customerDetailServiceImpl.saveOrUpdate(customerDetail);
		assertEquals("1", customerDetail.getCustomerId());
	}

	@Test
	public void testDeleteCustomerDetail() {
		assertEquals(3, customerDetailServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		customerDetailServiceImpl.delete(1);
		assertEquals(2, customerDetailServiceImpl.listAll().spliterator().getExactSizeIfKnown());

	}

	@Test
	public void testFindByUsernameContainsIgnoreCasesameCase() {
		List<CustomerDetail> customerDetails = customerDetailServiceImpl.findByUsernameContainsIgnoreCase("abc");
		assertTrue(customerDetails.size() == 3);
	}

	@Test
	public void testFindByUsernameContainsIgnoreCaseDifferentCase() {
		List<CustomerDetail> customerDetails = customerDetailServiceImpl.findByUsernameContainsIgnoreCase("aBc");
		assertTrue(customerDetails.size() == 3);
	}

	@Test
	public void testFindByUsernameContainsIgnoreCaseNotFound() {
		List<CustomerDetail> customerDetails = customerDetailServiceImpl.findByUsernameContainsIgnoreCase("asdfa");
		assertTrue(customerDetails.size() == 0);
	}
}