package com.group2.wsc.catalog.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.group2.wsc.catalog.domain.EmployeeDetail;
import com.group2.wsc.catalog.repositories.EmployeeDetailRepository;

public class EmployeeDetailServiceImplTest {
	EmployeeDetailServiceImpl employeeDetailServiceImpl;
	EmployeeDetailRepository employeeDetailRepository = mock(EmployeeDetailRepository.class);

	@Before
	public void before() {
		List<EmployeeDetail> employeeDetails = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			EmployeeDetail employeeDetail = new EmployeeDetail();
			employeeDetail.setId(i);
			employeeDetail.setUsername(i + "");
			employeeDetails.add(employeeDetail);
		}
		employeeDetailServiceImpl = new EmployeeDetailServiceImpl();
		employeeDetailServiceImpl.setEmployeeDetailRepository(employeeDetailRepository);
		when(employeeDetailRepository.findAll()).thenReturn(employeeDetails);
		when(employeeDetailRepository.findOne(anyInt())).thenReturn(employeeDetails.get(1));
		when(employeeDetailRepository.save(employeeDetails.get(1))).thenReturn(employeeDetails.get(1));
		doAnswer(new Answer<Void>() {
		    public Void answer(InvocationOnMock invocation) {
		      Object[] args = invocation.getArguments();
		      int index = (Integer)args[0];
		      employeeDetails.remove(index);
		      return null;
		    }
		}).when(employeeDetailRepository).delete(anyInt());
		
	}
	@Test
	public void testListAllEmployeeDetails() {
		assertEquals(3, employeeDetailServiceImpl.listAll().spliterator().getExactSizeIfKnown());
	}

	@Test
	public void testGetEmployeeDetailById() {
		EmployeeDetail employeeDetail = employeeDetailServiceImpl.getById(1);
		assertEquals("1", employeeDetail.getUsername());
	}

	@Test
	public void testSaveEmployeeDetail() {
		EmployeeDetail employeeDetail = employeeDetailServiceImpl.getById(1);
		employeeDetailServiceImpl.saveOrUpdate(employeeDetail);
		assertEquals("1", employeeDetail.getUsername());
	}

	@Test
	public void testDeleteEmployeeDetail() {
		assertEquals(3, employeeDetailServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		employeeDetailServiceImpl.delete(1);
		assertEquals(2, employeeDetailServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		
	}
}