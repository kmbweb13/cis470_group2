package com.group2.wsc.catalog.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.group2.wsc.catalog.domain.OrderDetail;
import com.group2.wsc.catalog.repositories.OrderDetailRepository;

public class OrderDetailServiceImplTest {
	OrderDetailServiceImpl orderDetailServiceImpl;
	OrderDetailRepository orderDetailRepository = mock(OrderDetailRepository.class);

	@Before
	public void before() {
		List<OrderDetail> orderDetails = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			OrderDetail orderDetail = new OrderDetail();
			orderDetail.setId(i);
			orderDetail.setEmployeeId(i + "");
			orderDetails.add(orderDetail);
		}
		orderDetailServiceImpl = new OrderDetailServiceImpl();
		orderDetailServiceImpl.setOrderDetailRepository(orderDetailRepository);
		when(orderDetailRepository.findAll()).thenReturn(orderDetails);
		when(orderDetailRepository.findOne(anyInt())).thenReturn(orderDetails.get(1));
		when(orderDetailRepository.save(orderDetails.get(1))).thenReturn(orderDetails.get(1));
		doAnswer(new Answer<Void>() {
		    public Void answer(InvocationOnMock invocation) {
		      Object[] args = invocation.getArguments();
		      int index = (Integer)args[0];
		      orderDetails.remove(index);
		      return null;
		    }
		}).when(orderDetailRepository).delete(anyInt());
		
	}
	@Test
	public void testListAllOrderDetails() {
		assertEquals(3, orderDetailServiceImpl.listAll().spliterator().getExactSizeIfKnown());
	}

	@Test
	public void testGetOrderDetailById() {
		OrderDetail orderDetail = orderDetailServiceImpl.getById(1);
		assertEquals("1", orderDetail.getEmployeeId());
	}

	@Test
	public void testSaveOrderDetail() {
		OrderDetail orderDetail = orderDetailServiceImpl.getById(1);
		orderDetailServiceImpl.saveOrUpdate(orderDetail);
		assertEquals("1", orderDetail.getEmployeeId());
	}

	@Test
	public void testDeleteOrderDetail() {
		assertEquals(3, orderDetailServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		orderDetailServiceImpl.delete(1);
		assertEquals(2, orderDetailServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		
	}
}