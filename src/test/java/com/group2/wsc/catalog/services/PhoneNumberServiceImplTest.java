package com.group2.wsc.catalog.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.group2.wsc.catalog.domain.PhoneNumber;
import com.group2.wsc.catalog.repositories.PhoneNumberRepository;

public class PhoneNumberServiceImplTest {
	PhoneNumberServiceImpl phoneNumberServiceImpl;
	PhoneNumberRepository phoneNumberRepository = mock(PhoneNumberRepository.class);

	@Before
	public void before() {
		List<PhoneNumber> phoneNumbers = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			PhoneNumber phoneNumber = new PhoneNumber();
			phoneNumber.setId(i);
			phoneNumber.setType(i + "");
			phoneNumbers.add(phoneNumber);
		}
		phoneNumberServiceImpl = new PhoneNumberServiceImpl();
		phoneNumberServiceImpl.setPhoneNumberRepository(phoneNumberRepository);
		when(phoneNumberRepository.findAll()).thenReturn(phoneNumbers);
		when(phoneNumberRepository.findOne(anyInt())).thenReturn(phoneNumbers.get(1));
		when(phoneNumberRepository.save(phoneNumbers.get(1))).thenReturn(phoneNumbers.get(1));
		//doNothing().when(productRepository).delete(anyInt());
		doAnswer(new Answer<Void>() {
		    public Void answer(InvocationOnMock invocation) {
		      Object[] args = invocation.getArguments();
		      int index = (Integer)args[0];
		      phoneNumbers.remove(index);
		      return null;
		    }
		}).when(phoneNumberRepository).delete(anyInt());
		
	}
	@Test
	public void testListAllPhoneNumbers() {
		assertEquals(3, phoneNumberServiceImpl.listAll().spliterator().getExactSizeIfKnown());
	}

	@Test
	public void testGetPhoneNumberById() {
		PhoneNumber phoneNumber = phoneNumberServiceImpl.getById(1);
		assertEquals("1", phoneNumber.getType());
	}

	@Test
	public void testSavePhoneNumber() {
		PhoneNumber phoneNumber = phoneNumberServiceImpl.getById(1);
		phoneNumberServiceImpl.saveOrUpdate(phoneNumber);
		assertEquals("1", phoneNumber.getType());
	}

	@Test
	public void testDeletePhoneNumber() {
		assertEquals(3, phoneNumberServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		phoneNumberServiceImpl.delete(1);
		assertEquals(2, phoneNumberServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		
	}
}