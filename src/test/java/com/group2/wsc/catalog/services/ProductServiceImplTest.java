package com.group2.wsc.catalog.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.group2.wsc.catalog.domain.Product;
import com.group2.wsc.catalog.repositories.ProductRepository;

public class ProductServiceImplTest {
	ProductServiceImpl productServiceImpl;
	ProductRepository productRepository = mock(ProductRepository.class);

	@Before
	public void before() {
		List<Product> products = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			Product product = new Product();
			product.setId(i);
			product.setMediaCatalogNumber(i + "");
			products.add(product);
		}
		productServiceImpl = new ProductServiceImpl();
		productServiceImpl.setProductRepository(productRepository);
		when(productRepository.findAll()).thenReturn(products);
		when(productRepository.findOne(anyInt())).thenReturn(products.get(1));
		when(productRepository.save(products.get(1))).thenReturn(products.get(1));
		//doNothing().when(productRepository).delete(anyInt());
		doAnswer(new Answer<Void>() {
		    public Void answer(InvocationOnMock invocation) {
		      Object[] args = invocation.getArguments();
		      int index = (Integer)args[0];
		      products.remove(index);
		      return null;
		    }
		}).when(productRepository).delete(anyInt());
		
	}
	
	
	@Test
	public void testListAllProducts() {
		assertEquals(3, productServiceImpl.listAllProducts().spliterator().getExactSizeIfKnown());
	}

	@Test
	public void testGetProductById() {
		Product product = productServiceImpl.getProductById(1);
		assertEquals("1", product.getMediaCatalogNumber());
	}

	@Test
	public void testSaveProduct() {
		Product product = productServiceImpl.getProductById(1);
		productServiceImpl.saveProduct(product);
		assertEquals("1", product.getMediaCatalogNumber());
	}

	@Test
	public void testDeleteProduct() {
		assertEquals(3, productServiceImpl.listAllProducts().spliterator().getExactSizeIfKnown());
		productServiceImpl.deleteProduct(1);
		assertEquals(2, productServiceImpl.listAllProducts().spliterator().getExactSizeIfKnown());
		
	}

}
