package com.group2.wsc.catalog.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.group2.wsc.catalog.domain.ProductStyle;
import com.group2.wsc.catalog.repositories.ProductStyleRepository;

public class ProductStyleServiceImplTest {
	ProductStyleServiceImpl productStyleServiceImpl;
	ProductStyleRepository productStyleRepository = mock(ProductStyleRepository.class);

	@Before
	public void before() {
		List<ProductStyle> productStyles = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			ProductStyle productStyle = new ProductStyle();
			productStyle.setId(i);
			productStyle.setLabel(i + "");
			productStyles.add(productStyle);
		}
		productStyleServiceImpl = new ProductStyleServiceImpl();
		productStyleServiceImpl.setProductStyleRepository(productStyleRepository);
		when(productStyleRepository.findAll()).thenReturn(productStyles);
		when(productStyleRepository.findOne(anyInt())).thenReturn(productStyles.get(1));
		when(productStyleRepository.save(productStyles.get(1))).thenReturn(productStyles.get(1));
		//doNothing().when(productRepository).delete(anyInt());
		doAnswer(new Answer<Void>() {
		    public Void answer(InvocationOnMock invocation) {
		      Object[] args = invocation.getArguments();
		      int index = (Integer)args[0];
		      productStyles.remove(index);
		      return null;
		    }
		}).when(productStyleRepository).delete(anyInt());
		
	}
	@Test
	public void testListAllProductStyles() {
		assertEquals(3, productStyleServiceImpl.listAll().spliterator().getExactSizeIfKnown());
	}

	@Test
	public void testGetProductStyleById() {
		ProductStyle productStyle = productStyleServiceImpl.getById(1);
		assertEquals("1", productStyle.getLabel());
	}

	@Test
	public void testSaveProductStyle() {
		ProductStyle productStyle = productStyleServiceImpl.getById(1);
		productStyleServiceImpl.saveOrUpdate(productStyle);
		assertEquals("1", productStyle.getLabel());
	}

	@Test
	public void testDeleteProductStyle() {
		assertEquals(3, productStyleServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		productStyleServiceImpl.delete(1);
		assertEquals(2, productStyleServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		
	}
}