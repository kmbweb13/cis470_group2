package com.group2.wsc.catalog.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.group2.wsc.catalog.domain.Role;
import com.group2.wsc.catalog.repositories.RoleRepository;

public class RoleServiceImplTest {
	RoleServiceImpl roleServiceImpl;
	RoleRepository roleRepository = mock(RoleRepository.class);

	@Before
	public void before() {
		List<Role> roles = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			Role role = new Role();
			role.setId(i);
			role.setRole(i + "");
			role.setPrioritylevel(i);
			roles.add(role);
		}
		roleServiceImpl = new RoleServiceImpl();
		roleServiceImpl.setRoleRepository(roleRepository);
		when(roleRepository.findAll()).thenReturn(roles);
		when(roleRepository.findOne(anyInt())).thenReturn(roles.get(1));
		when(roleRepository.save(roles.get(1))).thenReturn(roles.get(1));
		doAnswer(new Answer<Void>() {
		    public Void answer(InvocationOnMock invocation) {
		      Object[] args = invocation.getArguments();
		      int index = (Integer)args[0];
		      roles.remove(index);
		      return null;
		    }
		}).when(roleRepository).delete(anyInt());
		
	}
	@Test
	public void testListAllRoles() {
		assertEquals(3, roleServiceImpl.listAll().spliterator().getExactSizeIfKnown());
	}

	@Test
	public void testGetRoleById() {
		Role role = roleServiceImpl.getById(1);
		assertEquals("1", role.getRole());
	}

	@Test
	public void testSaveRole() {
		Role role = roleServiceImpl.getById(1);
		roleServiceImpl.saveOrUpdate(role);
		assertEquals("1", role.getRole());
	}

	@Test
	public void testDeleteRole() {
		assertEquals(3, roleServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		roleServiceImpl.delete(1);
		assertEquals(2, roleServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		
	}

	@Test
	public void testGetByRoleName() {
		assertEquals("0", roleServiceImpl.getByRoleName("0").getRole());
	}
	@Test
	public void getAdditionalRoles() {
		Role role = new Role();
		role.setId(1);
		role.setRole("1");
		role.setPrioritylevel(1);
		assertEquals(2, roleServiceImpl.getAdditionalRoles(role).size());
	}
}