package com.group2.wsc.catalog.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.group2.wsc.catalog.converters.Utility;
import com.group2.wsc.catalog.domain.CustomerDetail;
import com.group2.wsc.catalog.domain.OrderDetail;
import com.group2.wsc.catalog.domain.Payment;
import com.group2.wsc.catalog.domain.PhoneNumber;
import com.group2.wsc.catalog.domain.Product;
import com.group2.wsc.catalog.domain.ProductOrderDetail;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class ShoppingCartServiceImplTest {
	ShoppingCartServiceImpl shoppingCartServiceImpl;
	Payment payment;
	User user;
	UserInformation userInfo;
	Utility utility = mock(Utility.class);
	UserInformationService userInformationService = mock(UserInformationService.class);
	CustomerDetailService customerDetailService = mock(CustomerDetailService.class);
	OrderDetailService orderDetailService = mock(OrderDetailService.class);
	UserService userService = mock(UserService.class);
	RoleService roleService = mock(RoleService.class);
	Authentication authentication = mock(Authentication.class);
	SecurityContext securityContext = mock(SecurityContext.class);

	@Before
	public void before() {
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);

		shoppingCartServiceImpl = new ShoppingCartServiceImpl();
		shoppingCartServiceImpl.setUtility(utility);
		shoppingCartServiceImpl.setUserInformationService(userInformationService);
		shoppingCartServiceImpl.setCustomerDetailService(customerDetailService);
		shoppingCartServiceImpl.setOrderDetailService(orderDetailService);
		shoppingCartServiceImpl.setUserService(userService);
		shoppingCartServiceImpl.setRoleService(roleService);
		List<ProductOrderDetail> productOrderDetails = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			ProductOrderDetail pod = new ProductOrderDetail();
			pod.setId(i);
			pod.setUserId("abc");
			pod.setFinalPrice(Utility.createCurrency(10.00));
			pod.setQuantity(1);
			Product product = new Product();
			product.setId(1);
			pod.setProduct(product);
			pod.setProductId(1);
			productOrderDetails.add(pod);
		}
		shoppingCartServiceImpl.setProductOrderDetails(productOrderDetails);

		payment = new Payment();
		user = new User();
		userInfo = new UserInformation();
		user.setUsername("abc");
		user.setUserInformation(userInfo);

		when(utility.generateCustomerDetails(anyString(), anyString())).then(new Answer<CustomerDetail>() {
			public CustomerDetail answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();
				CustomerDetail cd = new CustomerDetail();
				cd.setUsername((String) args[0]);
				cd.setCustomerId((String) args[1]);
				return cd;
			}
		});

		when(orderDetailService.saveOrUpdate(anyObject())).thenReturn(null);
		when(customerDetailService.saveOrUpdate(anyObject())).thenReturn(null);
		when(userInformationService.saveOrUpdate(anyObject())).thenReturn(null);
		when(orderDetailService.getById(0)).then(new Answer<OrderDetail>() {
			public OrderDetail answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();
				OrderDetail orderDetail = new OrderDetail();
				orderDetail.setId((int) args[0]);
				orderDetail.setProductOrderDetails(productOrderDetails);
				orderDetail.setOrderTotal(Utility.createCurrency(30));
				return orderDetail;
			}
		});
		when(orderDetailService.getById(1)).then(new Answer<OrderDetail>() {
			public OrderDetail answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();
				OrderDetail orderDetail = new OrderDetail();
				orderDetail.setId((int) args[0]);
				orderDetail.setProductOrderDetails(productOrderDetails);
				ProductOrderDetail pod = new ProductOrderDetail();
				pod.setProductId(1);
				pod.setUserId("abc");
				pod.setFinalPrice(Utility.createCurrency(10.00));
				pod.setQuantity(1);
				Product product = new Product();
				product.setId(1);
				pod.setProduct(product);
				pod.setProductId(1);
				orderDetail.addProductOrderDetail(pod);
				orderDetail.setOrderTotal(Utility.createCurrency(30));
				return orderDetail;
			}
		});
	}

	@Test
	public void testAddProductOrderDetail() {
		assertEquals(3, shoppingCartServiceImpl.getProductOrderDetails().size());

		shoppingCartServiceImpl.addProductOrderDetail(new ProductOrderDetail());
		assertEquals(4, shoppingCartServiceImpl.getProductOrderDetails().size());

	}

	@Test
	public void testAddProductOrderDetailExisting() {
		assertEquals(3, shoppingCartServiceImpl.getProductOrderDetails().size());
		ProductOrderDetail pod = shoppingCartServiceImpl.getProductOrderDetailFromCart(1);
		pod.setColor("blue");
		shoppingCartServiceImpl.addProductOrderDetail(pod);
		assertEquals(3, shoppingCartServiceImpl.getProductOrderDetails().size());

	}

	@Test
	public void testRemoveProductOrderDetail() {
		assertEquals(3, shoppingCartServiceImpl.getProductOrderDetails().size());

		shoppingCartServiceImpl.removeProductOrderDetail(1);
		assertEquals(3, shoppingCartServiceImpl.getProductOrderDetails().size());
		ProductOrderDetail pod = shoppingCartServiceImpl.getProductOrderDetailFromCart(1);
		assertTrue(pod.isRemoved());
	}

	@Test
	public void testGetProductOrderDetailsInCart() {
		assertEquals(3, shoppingCartServiceImpl.getProductOrderDetailsInCart().size());

		shoppingCartServiceImpl.removeProductOrderDetail(1);
		assertEquals(2, shoppingCartServiceImpl.getProductOrderDetailsInCart().size());

	}

	@Test
	public void testGetProductOrderDetailFromCart() {
		assertEquals(3, shoppingCartServiceImpl.getProductOrderDetails().size());
		ProductOrderDetail pod = shoppingCartServiceImpl.getProductOrderDetailFromCart(1);
		assertFalse(pod.isRemoved());
	}

	@Test
	public void testCheckout() {
		user.setMainRoleName("GUEST");
		shoppingCartServiceImpl.checkout(payment, user);
		assertEquals(1, user.getUserInformation().getCustomerDetail().getOrderDetails().size());
	}

	@Test
	public void testCheckoutCustomer() {
		user.setMainRoleName("CUSTOMER");
		shoppingCartServiceImpl.checkout(payment, user);
		assertEquals(1, user.getUserInformation().getCustomerDetail().getOrderDetails().size());
	}

	@Test
	public void testCheckoutWithPhoneNumber() {
		user.setMainRoleName("GUEST");
		PhoneNumber pn = new PhoneNumber();
		pn.setAreacode("555");
		pn.setType("cell");
		pn.setPrefix("444");
		pn.setLinenumber("5555");
		user.getUserInformation().addPhoneNumber(pn);
		shoppingCartServiceImpl.checkout(payment, user);
		List<OrderDetail> orderDetails = user.getUserInformation().getCustomerDetail().getOrderDetails();
		assertEquals(1, orderDetails.size());
		assertEquals("(555) 444-5555", orderDetails.get(0).getPhoneNumber());
	}

	@Test
	public void testCheckoutCustomerDetails() {
		user.setMainRoleName("CUSTOMER");
		CustomerDetail cd = new CustomerDetail();
		cd.setUsername("abc");
		cd.setCustomerId("123");
		cd.addOrderDetail(new OrderDetail());
		user.getUserInformation().setCustomerDetail(cd);
		assertEquals(1, user.getUserInformation().getCustomerDetail().getOrderDetails().size());
		shoppingCartServiceImpl.checkout(payment, user);
		assertEquals(2, user.getUserInformation().getCustomerDetail().getOrderDetails().size());
	}

	@Test
	public void testCheckoutExistingOrder() {
		CustomerDetail cd = new CustomerDetail();
		cd.setUsername("abc");
		cd.setCustomerId("123");
		OrderDetail od = new OrderDetail();
		od.setId(0);
		cd.addOrderDetail(od);
		user.getUserInformation().setCustomerDetail(cd);
		payment.setOrderId(0);
		assertEquals(1, user.getUserInformation().getCustomerDetail().getOrderDetails().size());
		shoppingCartServiceImpl.checkout(payment, user);
		assertEquals(1, user.getUserInformation().getCustomerDetail().getOrderDetails().size());
	}

	@Test
	public void testCheckoutExistingOrderWithPayment() {
		CustomerDetail cd = new CustomerDetail();
		cd.setUsername("abc");
		cd.setCustomerId("123");
		OrderDetail od = new OrderDetail();
		od.setId(0);
		cd.addOrderDetail(od);
		user.getUserInformation().setCustomerDetail(cd);
		payment.setOrderId(0);
		payment.setAmount(Utility.createCurrency(10.00));
		assertEquals(1, user.getUserInformation().getCustomerDetail().getOrderDetails().size());
		shoppingCartServiceImpl.checkout(payment, user);
		assertEquals(1, user.getUserInformation().getCustomerDetail().getOrderDetails().size());
	}

	@Test
	public void testGetSubTotal() {
		assertEquals(Utility.createCurrency(30), shoppingCartServiceImpl.getSubTotal());
		ProductOrderDetail pod = new ProductOrderDetail();
		pod.setFinalPrice(Utility.createCurrency(10.00));
		pod.setQuantity(1);
		shoppingCartServiceImpl.addProductOrderDetail(pod);
		assertEquals(Utility.createCurrency(40), shoppingCartServiceImpl.getSubTotal());

	}

	@Test
	public void testGetSubTotalRemovedOrders() {
		assertEquals(Utility.createCurrency(30), shoppingCartServiceImpl.getSubTotal());
		ProductOrderDetail pod = new ProductOrderDetail();
		pod.setFinalPrice(Utility.createCurrency(10.00));
		pod.setQuantity(1);
		shoppingCartServiceImpl.addProductOrderDetail(pod);
		assertEquals(Utility.createCurrency(40), shoppingCartServiceImpl.getSubTotal());
		shoppingCartServiceImpl.removeProductOrderDetail(0);
		assertEquals(Utility.createCurrency(30), shoppingCartServiceImpl.getSubTotal());
	}

	@Test
	public void testGetTax() {
		assertEquals(Utility.createCurrency(2.25), shoppingCartServiceImpl.getTax());
		ProductOrderDetail pod = new ProductOrderDetail();
		pod.setFinalPrice(Utility.createCurrency(10.00));
		pod.setQuantity(1);
		shoppingCartServiceImpl.addProductOrderDetail(pod);
		assertEquals(Utility.createCurrency(3), shoppingCartServiceImpl.getTax());
	}

	@Test
	public void testGetTotal() {
		assertEquals(Utility.createCurrency(32.25), shoppingCartServiceImpl.getTotal());
		ProductOrderDetail pod = new ProductOrderDetail();
		pod.setFinalPrice(Utility.createCurrency(10.00));
		pod.setQuantity(1);
		shoppingCartServiceImpl.addProductOrderDetail(pod);
		assertEquals(Utility.createCurrency(43), shoppingCartServiceImpl.getTotal());
	}

	@Test
	public void testGetMinPayment() {
		assertEquals(Utility.createCurrency(3.23), shoppingCartServiceImpl.getMinPayment());
		ProductOrderDetail pod = new ProductOrderDetail();
		pod.setFinalPrice(Utility.createCurrency(10.00));
		pod.setQuantity(1);
		shoppingCartServiceImpl.addProductOrderDetail(pod);
		assertEquals(Utility.createCurrency(4.3), shoppingCartServiceImpl.getMinPayment());
	}

	@Test
	public void testGetMinPaymentExisting() {
		shoppingCartServiceImpl.setExistingPayments(Utility.createCurrency(10));
		assertEquals(Utility.createCurrency(0), shoppingCartServiceImpl.getMinPayment());
		ProductOrderDetail pod = new ProductOrderDetail();
		pod.setFinalPrice(Utility.createCurrency(10.00));
		pod.setQuantity(1);
		shoppingCartServiceImpl.addProductOrderDetail(pod);
		assertEquals(Utility.createCurrency(0), shoppingCartServiceImpl.getMinPayment());
	}

	@Test
	public void testGetMinPaymentExistingLessThanMin() {
		shoppingCartServiceImpl.setExistingPayments(Utility.createCurrency(2));
		assertEquals(Utility.createCurrency(1.23), shoppingCartServiceImpl.getMinPayment());
		ProductOrderDetail pod = new ProductOrderDetail();
		pod.setFinalPrice(Utility.createCurrency(10.00));
		pod.setQuantity(1);
		shoppingCartServiceImpl.addProductOrderDetail(pod);
		assertEquals(Utility.createCurrency(2.3), shoppingCartServiceImpl.getMinPayment());
	}

	@Test
	public void testLoadPreviousOpenOrder() {
		assertEquals(0, shoppingCartServiceImpl.loadPreviousOpenOrder(0));
	}

	@Test
	public void testLoadPreviousOpenOrderAddedOrder() {

		assertEquals(1, shoppingCartServiceImpl.loadPreviousOpenOrder(1));
	}

	@Test
	public void testLomBok() {
		shoppingCartServiceImpl.setExistingOrderId(1);
		shoppingCartServiceImpl.toString();
		Product product = new Product();
		product.setDescription("asdf");
		product.setId(1);
		EqualsVerifier.forClass(ShoppingCartServiceImpl.class)
		.suppress(Warning.STRICT_INHERITANCE)
		.suppress(Warning.NONFINAL_FIELDS)
		.withPrefabValues(Product.class, product, new Product()).verify();
	}

}
