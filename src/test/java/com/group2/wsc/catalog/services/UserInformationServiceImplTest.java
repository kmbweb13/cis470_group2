package com.group2.wsc.catalog.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.repositories.UserInformationRepository;

public class UserInformationServiceImplTest {
	UserInformationServiceImpl userInformationServiceImpl;
	UserInformationRepository userInformationRepository = mock(UserInformationRepository.class);

	@Before
	public void before() {
		List<UserInformation> userInformations = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			UserInformation userInformation = new UserInformation();
			userInformation.setId(i);
			userInformation.setFirstname(i + "");
			userInformations.add(userInformation);
		}
		userInformationServiceImpl = new UserInformationServiceImpl();
		userInformationServiceImpl.setUserInformationRepository(userInformationRepository);
		when(userInformationRepository.findAll()).thenReturn(userInformations);
		when(userInformationRepository.findOne(anyInt())).thenReturn(userInformations.get(1));
		when(userInformationRepository.save(userInformations.get(1))).thenReturn(userInformations.get(1));
		doAnswer(new Answer<Void>() {
		    public Void answer(InvocationOnMock invocation) {
		      Object[] args = invocation.getArguments();
		      int index = (Integer)args[0];
		      userInformations.remove(index);
		      return null;
		    }
		}).when(userInformationRepository).delete(anyInt());
		
	}
	@Test
	public void testListAllUserInformations() {
		assertEquals(3, userInformationServiceImpl.listAll().spliterator().getExactSizeIfKnown());
	}

	@Test
	public void testGetUserInformationById() {
		UserInformation userInformation = userInformationServiceImpl.getById(1);
		assertEquals("1", userInformation.getFirstname());
	}

	@Test
	public void testSaveUserInformation() {
		UserInformation userInformation = userInformationServiceImpl.getById(1);
		userInformationServiceImpl.saveOrUpdate(userInformation);
		assertEquals("1", userInformation.getFirstname());
	}

	@Test
	public void testDeleteUserInformation() {
		assertEquals(3, userInformationServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		userInformationServiceImpl.delete(1);
		assertEquals(2, userInformationServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		
	}
}