package com.group2.wsc.catalog.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.repositories.UserRepository;
import com.group2.wsc.catalog.services.security.EncryptionService;

public class UserServiceImplTest {
	UserServiceImpl userServiceImpl;
	UserRepository userRepository = mock(UserRepository.class);
	EncryptionService encryptionService = mock(EncryptionService.class);

	@Before
	public void before() {
		List<User> users = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			User user = new User();
			user.setId(i);
			user.setUsername("abc" + i);
			users.add(user);
		}
		userServiceImpl = new UserServiceImpl();
		userServiceImpl.setUserRepository(userRepository);
		userServiceImpl.setEncryptionService(encryptionService);
		when(userRepository.findAll()).thenReturn(users);
		when(userRepository.findOne(anyInt())).thenReturn(users.get(1));
		when(userRepository.save(users.get(1))).thenReturn(users.get(1));
		when(userRepository.findByUsernameContainsIgnoreCase(anyString())).then(new Answer<List<User>>() {
			public List<User> answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();
				String key = (String) args[0];
				final String searchKey = key.toLowerCase();
				return users.stream().filter(cd -> cd.getUsername().toLowerCase().contains(searchKey))
						.collect(Collectors.toList());
			}
		});
		when(userRepository.findByUsername(anyString())).then(new Answer<User>() {
			public User answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();
				String key = (String) args[0];
				final String searchKey = key.toLowerCase();
				try {
					return users.stream().filter(cd -> cd.getUsername().toLowerCase().equals(searchKey)).findFirst()
							.get();
				} catch (Exception e) {
					return null;
				}
			}
		});
		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();
				int index = (Integer) args[0];
				users.remove(index);
				return null;
			}
		}).when(userRepository).delete(anyInt());
		when(encryptionService.encryptString(anyString())).thenReturn("123");
	}

	@Test
	public void testListAllUsers() {
		assertEquals(3, userServiceImpl.listAll().spliterator().getExactSizeIfKnown());
	}

	@Test
	public void testGetUserById() {
		User user = userServiceImpl.getById(1);
		assertEquals("abc1", user.getUsername());
	}

	@Test
	public void testSaveUser() {
		User user = userServiceImpl.getById(1);
		userServiceImpl.saveOrUpdate(user);
		assertEquals("abc1", user.getUsername());
	}

	@Test
	public void testSaveUserNew() {
		

		User user = new User();
		user.setId(5);
		user.setPassword("asdf");
		user.setUsername("abc5");
		userServiceImpl.saveOrUpdate(user);
		assertEquals("abc5", user.getUsername());
	}

	@Test
	public void testDeleteUser() {
		assertEquals(3, userServiceImpl.listAll().spliterator().getExactSizeIfKnown());
		userServiceImpl.delete(1);
		assertEquals(2, userServiceImpl.listAll().spliterator().getExactSizeIfKnown());

	}

	@Test
	public void testFindByUsername() {
		User user = userServiceImpl.findByUsername("abc1");
		assertNotNull(user);
	}

	@Test
	public void testFindByUsernameInvalid() {
		User user = userServiceImpl.findByUsername("asdf");
		assertNull(user);
	}

	@Test
	public void testFindByUsernameContainsIgnoreCasesameCase() {
		List<User> users = userServiceImpl.findByUsernameContainsIgnoreCase("abc");
		assertTrue(users.size() == 3);
	}

	@Test
	public void testFindByUsernameContainsIgnoreCaseDifferentCase() {
		List<User> users = userServiceImpl.findByUsernameContainsIgnoreCase("aBc");
		assertTrue(users.size() == 3);
	}

	@Test
	public void testFindByUsernameContainsIgnoreCaseNotFound() {
		List<User> users = userServiceImpl.findByUsernameContainsIgnoreCase("asdfa");
		assertTrue(users.size() == 0);
	}
}