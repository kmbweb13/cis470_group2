package com.group2.wsc.catalog.services.mapservices;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.group2.wsc.catalog.domain.CustomerDetail;
import com.group2.wsc.catalog.domain.DomainObject;

public class CustomerDetailServiceMapImplTest {

	CustomerDetailServiceMapImpl customerDetailServiceMapImpl;

	@Before
	public void before() {

		customerDetailServiceMapImpl = new CustomerDetailServiceMapImpl();
		customerDetailServiceMapImpl.domainMap = new HashMap<Integer, DomainObject>();
		for (int i = 0; i < 3; i++) {
			CustomerDetail customerDetail = new CustomerDetail();
			customerDetail.setUsername("abc" + i);
			customerDetail.setId(i);
			customerDetailServiceMapImpl.domainMap.put(i, customerDetail);
		}
	}

	@Test
	public void testGetByIdInteger() {
		assertEquals(3, customerDetailServiceMapImpl.domainMap.size());
		CustomerDetail customerDetail = customerDetailServiceMapImpl.getById(1);
		assertEquals("abc1", customerDetail.getUsername());
	}

	@Test
	public void testSaveOrUpdateCustomerDetail() {

		assertEquals(3, customerDetailServiceMapImpl.domainMap.size());
		CustomerDetail customerDetail = customerDetailServiceMapImpl.getById(1);
		customerDetail.setCustomerId("123");
		customerDetailServiceMapImpl.saveOrUpdate(customerDetail);
		assertEquals("123", customerDetail.getCustomerId());
	}

	@Test
	public void testFindByUsernameContainsIgnoreCasesameCase() {
		List<CustomerDetail> customerDetails = customerDetailServiceMapImpl.findByUsernameContainsIgnoreCase("abc");
		assertTrue(customerDetails.size() == 3);
	}

	@Test
	public void testFindByUsernameContainsIgnoreCaseDifferentCase() {
		List<CustomerDetail> customerDetails = customerDetailServiceMapImpl.findByUsernameContainsIgnoreCase("aBc");
		assertTrue(customerDetails.size() == 3);
	}

	@Test
	public void testFindByUsernameContainsIgnoreCaseNotFound() {
		List<CustomerDetail> customerDetails = customerDetailServiceMapImpl.findByUsernameContainsIgnoreCase("asdfa");
		assertTrue(customerDetails.size() == 0);
	}
}
