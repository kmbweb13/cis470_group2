package com.group2.wsc.catalog.services.mapservices;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.group2.wsc.catalog.domain.DomainObject;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.services.security.EncryptionService;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceMapImplTest {
	UserServiceMapImpl userServiceMapImpl;
	EncryptionService encryptionService = mock(EncryptionService.class);

	@Before
	public void before() {

		userServiceMapImpl = new UserServiceMapImpl();

		when(encryptionService.encryptString(anyString())).thenReturn("newValue");
		userServiceMapImpl.setEncryptionService(encryptionService);

		userServiceMapImpl.domainMap = new HashMap<Integer, DomainObject>();
		for (int i = 0; i < 3; i++) {
			User user = new User();
			user.setUsername("abc" + i);
			user.setId(i);
			userServiceMapImpl.domainMap.put(i, user);
		}
	}

	@Test
	public void testListAll() {
		assertEquals(3, userServiceMapImpl.listAll().size());
	}

	@Test
	public void testDelete() {
		assertEquals(3, userServiceMapImpl.domainMap.size());
		userServiceMapImpl.delete(1);
		assertEquals(2, userServiceMapImpl.domainMap.size());
	}

	@Test
	public void testGetByIdInteger() {
		assertEquals(3, userServiceMapImpl.domainMap.size());
		User user = userServiceMapImpl.getById(1);
		assertEquals("abc1", user.getUsername());
	}

	@Test
	public void testSaveOrUpdateUser() {
		assertNotNull(userServiceMapImpl.getEncryptionService());
		User user = userServiceMapImpl.getById(1);
		assertEquals("abc1", user.getUsername());
		user.setPassword("123");
		user = userServiceMapImpl.saveOrUpdate(user);
		assertEquals("newValue", user.getEncryptedPassword());
	}

	@Test
	public void testSaveOrUpdateUserNull() {

		try {
			userServiceMapImpl.saveOrUpdate(null);
			fail("RuntimeException expected.");
		} catch (RuntimeException expected) {
			assertEquals("Object Can't be null", expected.getMessage());
		}
	}

	@Test
	public void testSaveOrUpdateUserNew() {
		assertEquals(3, userServiceMapImpl.domainMap.size());
		User user = new User();
		user.setUsername("abc4");
		user = userServiceMapImpl.saveOrUpdate(user);
		assertEquals("abc4", user.getUsername());
		assertEquals(4, userServiceMapImpl.domainMap.size());

	}

	@Test
	public void testSaveOrUpdateUserPasswordNull() {
		User user = userServiceMapImpl.getById(1);
		assertEquals("abc1", user.getUsername());
		user = userServiceMapImpl.saveOrUpdate(user);
		assertEquals(null, user.getEncryptedPassword());
	}

	@Test
	public void testFindByUsername() {
		User user = userServiceMapImpl.findByUsername("abc1");
		assertEquals("abc1", user.getUsername());
	}

	@Test
	public void testFindByUsernameUnknown() {
		User user = userServiceMapImpl.findByUsername("asdfa");
		assertNull(user);
	}

	@Test
	public void testFindByUsernameContainsIgnoreCasesameCase() {
		List<User> users = userServiceMapImpl.findByUsernameContainsIgnoreCase("abc");
		assertTrue(users.size() == 3);
	}

	@Test
	public void testFindByUsernameContainsIgnoreCaseDifferentCase() {
		List<User> users = userServiceMapImpl.findByUsernameContainsIgnoreCase("aBc");
		assertTrue(users.size() == 3);
	}

	@Test
	public void testFindByUsernameContainsIgnoreCaseNotFound() {
		List<User> users = userServiceMapImpl.findByUsernameContainsIgnoreCase("asdfa");
		assertTrue(users.size() == 0);
	}
	
	@Test
	public void testLomBok() {

		 assertEquals("UserServiceMapImpl(encryptionService=encryptionService)", userServiceMapImpl.toString());
		 EqualsVerifier.forClass( UserServiceMapImpl.class )
	        .suppress( Warning.STRICT_INHERITANCE )
	        .suppress(Warning.NONFINAL_FIELDS)
	        .suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)
	        .verify();
	}

}
