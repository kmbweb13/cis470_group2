package com.group2.wsc.catalog.services.security;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.junit.Before;
import org.junit.Test;

public class EncryptionServiceImplTest {
	EncryptionServiceImpl encryptionServiceImpl;

	@Before
	public void before() {
		encryptionServiceImpl = new EncryptionServiceImpl();
		encryptionServiceImpl.setStrongEncryptor(new StrongPasswordEncryptor());
	}

	@Test
	public void testEncryptStringAndCheckPassword() {
		String result =  encryptionServiceImpl.encryptString("abc");
		assertTrue(encryptionServiceImpl.checkPassword("abc", result));
		result =  encryptionServiceImpl.encryptString("abc");
		assertFalse(encryptionServiceImpl.checkPassword("abc1", result));
	}

}
