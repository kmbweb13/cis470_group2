package com.group2.wsc.catalog.services.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.group2.wsc.catalog.domain.OrderDetail;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class UserDetailsImplTest {
	UserDetailsImpl userDetails;

	@Before
	public void before() {
		userDetails = new UserDetailsImpl();
	}

	@Test
	public void testIsAccountNonExpired() {
		assertTrue(userDetails.isAccountNonExpired());
	}

	@Test
	public void testIsAccountNonLocked() {
		assertTrue(userDetails.isAccountNonLocked());
	}

	@Test
	public void testIsCredentialsNonExpired() {
		assertTrue(userDetails.isCredentialsNonExpired());
	}

	@Test
	public void testLomBok() {
		assertEquals(
				"UserDetailsImpl(authorities=null, username=null, userInformation=null, password=null, enabled=true)",
				userDetails.toString());
		OrderDetail od1 = new OrderDetail();
		od1.setUsername("asdf");
		EqualsVerifier.forClass(UserDetailsImpl.class).suppress(Warning.STRICT_INHERITANCE)
				.suppress(Warning.NONFINAL_FIELDS).suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)

				.withPrefabValues(OrderDetail.class, od1, new OrderDetail()).verify();
	}

}
