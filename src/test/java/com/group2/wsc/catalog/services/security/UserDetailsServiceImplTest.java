package com.group2.wsc.catalog.services.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.group2.wsc.catalog.converters.UserToUserDetails;
import com.group2.wsc.catalog.domain.Role;
import com.group2.wsc.catalog.domain.User;
import com.group2.wsc.catalog.domain.UserInformation;
import com.group2.wsc.catalog.services.UserService;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsServiceImplTest {
	private UserDetailsServiceImpl userDetailsServiceImpl;
	UserService userService = mock(UserService.class);

	@Before
	public void before() { 
		userDetailsServiceImpl = new UserDetailsServiceImpl();
		userDetailsServiceImpl.setUserUserDetailsConverter(new UserToUserDetails());
		User user = new User();
		user.setUsername("abc1");
		user.setEncryptedPassword("testPassword");
		user.setEnabled(true);
		user.setUserInformation(new UserInformation());
		Role role = new Role();
		role.setRole("TEST");
		user.addRole(role);
		
		
		when(userService.findByUsername(anyString())).thenReturn(user);
		userDetailsServiceImpl.setUserService(userService);
	
	}
	@Test
	public void testLoadUserByUsername() {
		UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername("abc1");
		assertEquals("abc1", userDetails.getUsername());
		assertEquals("testPassword", userDetails.getPassword());
		assertTrue(userDetails.getAuthorities().size() ==1);
		assertEquals("TEST", ((SimpleGrantedAuthority)userDetails.getAuthorities().toArray()[0]).getAuthority());
	}

}
